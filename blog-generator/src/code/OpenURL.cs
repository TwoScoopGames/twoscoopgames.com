using UnityEngine;

public class OpenURL : MonoBehaviour {

  public void GoToURL(string url) {
    Application.OpenURL(url);
  }
}
