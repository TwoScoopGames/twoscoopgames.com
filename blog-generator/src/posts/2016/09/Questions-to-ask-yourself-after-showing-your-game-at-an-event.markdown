---
dateTime: 2016-09-12T18:02:52.000Z
year: 2016
month: 09
day: 12
time: 14.02.52
title: Questions to ask yourself after showing your game at an event
tags:
  - clusterjunkgame
  - game design
  - game development
  - gamedev
  - gamedevky
  - indie game
  - indie games
  - louisville
  - louisvillemakesgames
  - makerlou
featured: false
coverPhoto: /images/2016/09/Screen-Shot-2016-09-14-at-10.03.07-AM-300x59.png
---

<p><img src="/images/2016/09/Screen-Shot-2016-09-14-at-10.03.07-AM-300x59.png" alt="Two Scoop Games - Indie game marketing ideas" width="300" height="59" class="aligncenter size-medium wp-image-731" srcset="/images/2016/09/Screen-Shot-2016-09-14-at-10.03.07-AM-300x59.png 300w, /images/2016/09/Screen-Shot-2016-09-14-at-10.03.07-AM-768x152.png 768w, /images/2016/09/Screen-Shot-2016-09-14-at-10.03.07-AM-1024x203.png 1024w, /images/2016/09/Screen-Shot-2016-09-14-at-10.03.07-AM-1200x237.png 1200w" sizes="(max-width: 300px) 85vw, 300px"/></p><p><b>By Alex Bezuska</b><br/>(Also relevant: <a href="http://twoscoopgames.tumblr.com/post/99052760401/run-a-fun-festival-booth">How to run a festival booth!</a>)</p>
<p>Having just shown our new game <a href="http://clusterjunkgame.com">Cluster Junk: The Secret of Trash Island</a> for the first time at a public show with lots of kids I am pondering a lot of things. The show went really well from what I observed, but there is always room for improvement, and focusing of efforts for next time. I started to put together a list for my team to help everyone take some notes on what they observed. This led me to think this may be useful for others in our game dev community, and other developers in general. I have shown our games 6 times now at public events, and each time I come away with new issues to solve and things to improve.  So here are the things me and my team will be pondering in the time leading up to the next event.</p>
<p><b>1. Who played your game? </b><br/>Ages, etc.</p>
<p><b>2. What were the first things players tried to do? </b><br/>Was this what you had intended them to do?</p>
<p><b>3. What reactions during play did the players have? </b><br/>If they were in a group, what did friends/onlookers comment about while watching the player play.</p>
<p><b>4. What different types of players and play styles did you see? </b><br/>Think back to Bartle’s Taxonomy (and if you have no idea what I am talking about watch this great video: <a href="https://www.youtube.com/watch?v=yxpW2ltDNow">Bartle’s taxonomy</a>), and what features can be added or changed to serve these play styles.</p>
<p><b>5. What was difficult for players to understand? </b><br/>Specific questions players asked/ Observations of struggle in play</p>
<p><b>6. What was the most fun for players?  </b><br/>Comments/ assumptions based on reactions during play</p>
<p><b>7. How long did players play on average? </b><br/>Did they complete the experience you had set out to show off?</p>
<p><b>8. When players stopped playing, why did they stop and how did they stop?</b><br/>Did they know what to do before leaving the booth? Did they have any trouble exiting the booth area?</p>
<p><b>9. Did the players  give any feedback? </b></p>
<p><b>10. Why didn’t the players who left early stay longer? </b><br/>This might be tough if there was no feedback given. Not super important if most players had the full experience you intended.</p>
<p><b>11. Did players make any suggestions, or say you should add any features? </b><br/>Remember if a player suggests something, don’t just add the feature they suggest, but look for a root cause behind the suggestion and address it in a way that fits your design goals.</p>
<p><b>12. What was social interaction like between you/your team and the players coming by your booth? </b><br/>Was this what you expected? How can you improve?</p>
<p><b>13. What stood out to you the most about the experience?</b></p>
<p><b>14. What can you improve on? </b><br/>What would you have changed if you had to do the show over? This will help you figure out what you should try changing for next time you show your game</p>

<p><b>Who is Alex?</b><br/>Alex Bezuska is the cofounder/ artist for <a href="http://t.umblr.com/redirect?z=http%3A%2F%2Ftwoscoopgames.com&amp;t=YmVmMjViMmEwMWZjZDAzNjhhMGQ5YjkxNTNlMzJmYTgzODQyOTM4Nix2TDIwcE5BNw%3D%3D">Two Scoop Games</a> and Director at <a href="http://louisvillemakesgames.org">Louisville Makes Games!</a>  Alex is obsessively passionate about making games and building the local game development community. He is also an avid lover of ice cream.</p>
