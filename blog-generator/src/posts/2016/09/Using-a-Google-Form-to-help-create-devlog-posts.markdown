---
dateTime: 2016-09-14T20:57:43.000Z
year: 2016
month: 09
day: 14
time: 16.57.43
title: Using a Google Form to help create devlog posts
tags:
  - gamedev
  - devlog
  - indie games
  - game design
  - game development
  - video games
  - indie game
  - indiegames
  - marketing
featured: false
coverPhoto: /images/2016/09/devlog-form.png
---


Last night while working on our current project #ClusterJunkGame we were talking about what to do next. We are just coming down from showing our game at a big local event and there is a lot to reflect on, organize, and prepare for the next event in about a month. As a team I tend to be the one that focuses the most on marketing and social media, which can be tough to handle when I am already working on art for our games. In an attempt to offset my load I have asked the rest of the team to help with the devlogs and we discussed it as a group and determined that it it is too difficult to sit in front of screen and think, “What did I do today?”. 

![](/images/2016/09/devlog-form.png)

After a bit of discussion we came up with a great idea; a form with questions  in order from least to most in-depth. This way each team member can ease themselves into as Eric put it “get yourself into the contemplative mindset”. As with all of my blog posts in this series, the topic stems from something that we as a small indie studio needed, or learned from experience.

So here we go, the devlog generator. This survey is to be taken by each member of the team after each day they work on the game.  There is a google form version of this as well which stores all the content in a google sheets spreadsheet for later copy-and-paste into your devlog of choice (See my other post [Why you need a devlog](http://twoscoopgames.tumblr.com/post/92630486631/why-you-need-a-devlog) for blog platform suggestions)

DevLog Submission

Name

What game are you working on?

Date of work session.

Start time  to end time

What did you work on? (example: Code, Art, Story, Marketing, Music, etc.)

What feature or tasks did you accomplish?

Please provide a link to screenshot, gif, or video of this feature in action. (if applicable)

Did you encounter any problems? How did you solve them?

What did you learn during this session?

Other comments:

