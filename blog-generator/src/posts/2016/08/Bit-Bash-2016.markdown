---
dateTime: 2016-08-16T16:58:49.000Z
year: 2016
month: 08
day: 16
time: 12.58.49
title: Bit Bash 2016
featured: false
coverPhoto: /images/2016/08/tumblr_oc0hu1ftPW1td7tmqo1_1280.jpg
coverPhotoAlt: bitbash was so much fun It was great seeing friends from near and far, and inspiring games and art of all types BitBashChicago chicago

---

<p><a href="https://twitter.com/hashtag/bitbash?src=hash">#<b>bitbash</b></a> was so much fun! It was great seeing friends from near and far, and inspiring games and art of all types! <a href="https://twitter.com/BitBashChicago">@<b>BitBashChicago</b></a> <a href="https://twitter.com/hashtag/chicago?src=hash">#<b>chicago</b></a><br/></p>
