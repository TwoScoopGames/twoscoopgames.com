---
dateTime: 2016-02-08T17:34:10.000Z
year: 2016
month: 02
day: 08
time: 12.34.10
title: Morning Ritual - our Global Game Jam 2016 entry
tags:
  - ggj16
  - global game jam
  - globalgamejam
  - gamedev
  - gamedevky
  - game development
  - louisville
  - kentucky
  - morning ritual
  - morningritual
  - ipad
  - iphone
  - indiedev
  - ios
  - touch
  - warioware
featured: false
coverPhoto: http://twoscoopgames.com/img/morningritual.gif
coverPhotoAlt: New promo video for the new game we are working on called Morning Ritual. Check it out here httpswww.youtube.comwatchvEfONONJ7GLUfeatureyoutu.be
youtube:
    - EfONONJ7GLU
---

[youtube-0]
Video for the new game jam game we created called Morning Ritual. [Check out the game here](http://twoscoopgames.com/morningritual/)