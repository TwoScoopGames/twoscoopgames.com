---
dateTime: 2016-02-08T17:40:38.000Z
year: 2016
month: 02
day: 08
time: 12.40.38
title: Two Scoop Games is coming to Louisville Arcade Expo 2016
tags:
  - gamedevlou
  - louisvillemakesgames
  - warpzonelouisville
  - louisville
  - morning ritual
  - gamedev
  - game development
  - game
  - game design
  - indie games
  - indie game
  - indiegames
  - indie developer
  - warioware
  - microgames
  - ipad
  - ios
  - iphone
  - louisvillearcadeexpo
featured: false
coverPhoto: /images/2016/02/tumblr_o28p3qTRFU1td7tmqo1_1280.png
coverPhotoAlt: We added MorningRitual to our website, check out the playable ggj16 game jam version, and look out for a full game soon for iOS  httptwoscoopgames.commorningritualIf you are in the Louisville Kentucky area - We will be showing our game at the Louisville Arcade Expo httpwww.arcaderx.com March 4-6 2016 as part of an indie showcase calledIndie Alley

---

<p>We added #MorningRitual to our website, check out the playable #ggj16 game jam version, and look out for a full game soon for iOS!   <br/><a href="http://twoscoopgames.com/morningritual/">http://twoscoopgames.com/morningritual/</a><br/>If you are in the Louisville Kentucky area - We will be showing our game at the Louisville Arcade Expo (<a href="http://www.arcaderx.com/">http://www.arcaderx.com/</a>) March 4-6 2016 as part of an indie showcase called “Indie Alley”!</p>
