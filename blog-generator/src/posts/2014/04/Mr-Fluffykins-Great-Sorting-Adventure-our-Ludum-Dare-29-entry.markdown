---
dateTime: 2014-04-28T17:29:19.000Z
year: 2014
month: 04
day: 28
time: 13.29.19
title: Mr. Fluffykins' Great Sorting Adventure - our Ludum Dare 29 entry
tags:

  - YouTube
featured: false
coverPhoto: http://twoscoopgames.com/img/fluffykins-hover.gif
youtube:
    - ZsB_soaHNpI
---


[youtube-0]

​Take a peek inside your home internet router to see how all those crazy cat pictures actually get to your computer. Play as Mr. Fluffykins, the cat who sorts your email and deletes your spam. Try to keep the data flowing as long as you can! This game is a Two Scoop collaboration with team Pancake, our Ludum Dare 29 team which consisted of: Aaron Smith, Alex Bezuska, Anthony Quisenberry, Eric Lathrop, Rex Soriano, and Nathan Hutchens.​ 
