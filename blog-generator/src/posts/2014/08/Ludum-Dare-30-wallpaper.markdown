---
dateTime: 2014-08-21T18:39:42.000Z
year: 2014
month: 08
day: 21
time: 14.39.42
title: Ludum Dare 30 wallpaper
tags:
  - ld48
  - game dev
  - game development
  - game jam
  - gamedev
  - gamejam
  - indie games
  - indiedev
  - ld30
  - ludum dare
  - ludum dare 30
  - wallpaper
featured: false
coverPhoto: /images/2014/08/ld30-wallpaper-1920x1200.png
---

Hey everyone I created a wallpaper for Ludum Dare 30, feel free to use it however you like!
<br/><img class="alignnone size-medium wp-image-370696" src="/images/2014/08/ld30-wallpaper-1920x1200.png" alt="Ludum Dare 30 Wallpaper ld30 ld48 by Two Scoop Games" width="640" style="margin: 0 auto"/><br/><br/>
Download the size you need here:
<br/><a href="/images/2014/08/ld30-wallpaper-1920x1200.png">1920×1200 (landscape)</a>
<br/><a href="/images/2014/08/ld30-wallpaper-1080x1920.png">1080×1920 (portrait)</a>
<br/><a href="/images/2014/08/ld30-wallpaper-1536x2048.png">1536×2048 (portrait)</a>
<br/><a href="/images/2014/08/ld30-wallpaper-1680x1050.png">1680×1050 (landscape)</a>
<br/><a href="/images/2014/08/ld30-wallpaper-1920x1080.png">1920×1080 (landscape)</a>
<br/><a href="/images/2014/08/ld30-wallpaper-2048x1536.png">2048×1536 (landscape)</a>
<br/><a href="/images/2014/08/ld30-wallpaper-2560x1600.png">2560×1600 (landscape)</a>
<br/><br/><br/><img class="alignnone size-medium wp-image-370696" src="/images/2014/08/loudumdare30-wallpaper-1920x1080.png" alt="Ludum Dare 30 Wallpaper ld30 ld48 by Two Scoop Games" width="640" style="margin: 0 auto"/>
Also for my fellow Louisvillians who will be jamming with us at <a title="Game Dev Lou - Louisville Kentucky's Inide game development community" href="http://gamedevlou.org">GameDevLou</a>, here are some Louisville-specific versions just for fun:
<br/><br/><a href="/images/2014/08/loudumdare30-wallpaper-1080x1920.png">1080×1920 (portrait)</a>
<br/><a href="/images/2014/08/loudumdare30-wallpaper-1536x2048.png">1536×2048 (portrait)</a>
<br/><a href="/images/2014/08/loudumdare30-wallpaper-1920x1080.png">1920×1080 (landscape)</a>
<br/><a href="/images/2014/08/loudumdare30-wallpaper-2048x1536.png">2048×1536 (landscape)</a>
<br/>
Let me know if there are any sizes you need and I might be able to whip another one up for you.
<br/><br/>
Peace, and happy jamming!
