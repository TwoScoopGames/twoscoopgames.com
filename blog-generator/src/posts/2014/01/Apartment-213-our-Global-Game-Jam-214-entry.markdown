---
dateTime: 2014-01-24T17:29:20.000Z
year: 2014
month: 01
day: 24
time: 13.29.20
title: Apartment 213 - our Global Game Jam 2014 entry
tags:

  - YouTube
featured: false
coverPhoto: http://twoscoopgames.com/img/apartment213-hover.gif
youtube:
    - FffkvbF-AQQ
---
[youtube-0]

Play the game free here: <a href="http://ift.tt/2wN7ZIL">http://ift.tt/2wN7ZIL</a> This is a game about the ways several characters see the small apartment that their lives cross in. You start from the perspective of a small mouse and slowly switch perspectives with the antagonist of each character&rsquo;s own personal stories. These stories are colored by personal bias and each character&rsquo;s own concept of the apartment they interact in reveals more about that character than it does the apartment itself. Game Jam Created for Global Game Jam 2014 Janurary 24-26 2014 Theme: we don&rsquo;t see things as they are, we see them as we are View the Jam Page: http://localhost:8000/apartment213/ by Two Scoop Games</p>
