---
dateTime: 2014-12-08T15:17:08.000Z
year: 2014
month: 12
day: 08
time: 10.17.08
title: Desserts Killed Your Daddy our Ludum Dare 31 entry
tags:
  - browser game
  - browser games
  - game design
  - game dev
  - game development
  - gamedev
  - html5 canvas
  - html5 game
  - indie game
  - indie games
  - ld31
  - ldjam
  - ludum dare
featured: false
coverPhoto: /images/2014/12/desserts.png
---

So another Ludum Dare game jam is in the history books, and Two Scoop has created another ridiculous game! This time it’s ‘Desserts Killed Your Daddy’.

<br/><br/><img class="aligncenter size-full wp-image-627" alt="Desserts Killed Your Daddy by Two Scoop Games" src="/images/2014/12/desserts.png" width="800" height="340"/><br/><br/>We wanted to use the sword slashing mechanic from the Legend of Zelda: A Link To the Past and get it to feel right. With the mechanic being our main goal this time, the game is pretty simple. You play as a little chef’s hat with a rolling pin and you must defeat as many desserts as possible and beat your high score. We threw in lots of variety in enemy looks and behaviours to make things more interesting, and there is even a GINGERBOSS ever 50 points you accumulate!
<br/><br/>
Here is the link to the Ludum Dare Page for DKYD where you can try the game yourself: <a href="http://ludumdare.com/compo/ludum-dare-31/?action=preview&amp;uid=37347">http://ludumdare.com/compo/ludum-dare-31/?action=preview&amp;uid=37347</a>
<br/><br/>
Enjoy, and check out our other games as well at <a href="http://twoscoopgames.com">TwoScoopGames.com!</a>

<br/><br/><br/>
 

-Alex,
Artist for Two Scoop Games
