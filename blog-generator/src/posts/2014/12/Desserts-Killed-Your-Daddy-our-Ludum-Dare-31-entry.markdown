---
dateTime: 2014-12-08T17:29:21.000Z
year: 2014
month: 12
day: 08
time: 13.29.21
title: Desserts Killed Your Daddy - our Ludum Dare 31 entry
tags:

  - YouTube
featured: false
coverPhoto: http://twoscoopgames.com/img/desserts-ss-1.png
youtube:
    - 3R9OXxwA97A
---


<p>Play free here: <a href="http://ift.tt/2wNFKtD">http://ift.tt/2wNFKtD</a> About the game Defeat as many desserts as possible while keeping your health up with hearts. Enemies you will face are: 3 types of cake turtles, 4 types of walking cookies, and the terrifying GINGERBOSS! Can you get to 50 points and defeat the GINGERBOSS? Cans have a chance of containing a heart which you will need to refill your health. Game created for the Ludum Dare 31 Game Jam December 2014. Music by Ill Storms Game Jam Created for Ludum Dare 31 December 5th-8th, 2014 Theme: Entire Game on One Screen View the Jam Page: http://localhost:8000/desserts/ by Two Scoop Games</p>
