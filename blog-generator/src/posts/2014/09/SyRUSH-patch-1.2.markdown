---
dateTime: 2014-09-22T17:15:42.000Z
year: 2014
month: 09
day: 22
time: 13.15.42
title: SyRUSH patch 1.2
tags:
  - app store
  - apple
  - breakfast
  - game
  - game dev
  - indie game
  - indie game dev
  - iOs
  - ios game
  - ipad game
  - iphone game
  - syrup
  - SyRUSH
  - waffle
featured: false
coverPhoto: /images/2014/09/syrush1.2.png
---

<img src="/images/2014/09/syrush1.2.png"/><br/><br/>
This version will include one bug fix, and one new feature.
<br/>
The bug fix has to do with the mute button, now it works!
<br/>
The new feature is something that has been requested a few times, and will go into future games we create as well. It is pausing when the game is not in focus or when you change apps.
Now when you get a notification and click it on accident, the game will pause let you do what you need to do, and be waiting for you when you get back, with no lost progress!
<br/><br/><img style="display: block; margin: 0 auto;" src="/images/2014/09/taptocontinue.png"/><br/><br/>
Look for version 1.2 coming to iOS this week!

<br/><br/>
Check it out here, and download it for iOS: <a href="http://twoscoopgames.com/syrush">http://twoscoopgames.com/syrush</a>

