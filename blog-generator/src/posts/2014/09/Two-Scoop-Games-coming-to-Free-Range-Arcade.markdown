---
dateTime: 2014-09-22T19:13:22.000Z
year: 2014
month: 09
day: 22
time: 15.13.22
title: Two Scoop Games coming to Free Range Arcade
tags:
  - arcade
  - bloomington
  - events
  - Free Range Arcade
  - indiana
  - indie games
  - meet two scoop games
  - midwest
  - play games
featured: false
coverPhoto: /images/2014/09/twoscoopgames-at-freerangearcade.png
---

We have been invited to come show off our games at an event called Free Range Arcade in Bloomington, IN, on October 3rd, 2014 5pm. Come visit us and play our new games <a href="http://twoscoopgames.com/syrush">SyRUSH</a> and <a href="http://twoscoopgames.com/http://twoscoopgames.com/stanleysqueaks/">Stanley Squeaks and the Emerald Burrito</a>!
<br/><br/><img src="/images/2014/09/twoscoopgames-at-freerangearcade.png"/><br/><br/><strong>When:</strong> Oct 3, 2014 at 5:00 pm
<br/><strong>Where:</strong> City Hall Atrium, 401 N Morton, Bloomington, IN
<br/><strong>More info :</strong> <a href="http://freerangearcade.org">freerangearcade.org</a>
<br/><strong>Admission:</strong> Free!
<br/>
Free Range Arcade is a gallery of video games made right here in the Midwest! Game makers from all over the region will gather here in Bloomington to show off their work. Free Range Arcade will take place during Bloomington’s Gallery Walk, so you can see both local games and local art!
<br/><br/>
Indie Studios who will be there:
<br/><a href="http://StudioCypher.com">Studio Cypher</a>, <a href="http://MommysBestGames.com">Mommy’s Best Games</a>,  <a href="http://www.facebook.com/VisualVillains">Visual Villains</a>, <a href="http://SuperSoul.co">Super Soul</a>,  <a href="http://ContestedSpace.net">Zon Haralovich</a>, <a href="http://MadGoblinGames.net">Mad Goblin Games</a>, DevRen Games, <a href="http://SS64Games.com">S.S. 64 Games</a>, and <a href="http://TwoScoopGames.com">Two Scoop Games</a>!

