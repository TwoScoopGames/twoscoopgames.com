---
dateTime: 2014-09-17T02:23:39.000Z
year: 2014
month: 09
day: 16
time: 22.23.39
title: SyRUSH is out now for iPhone, iPad, and iPod Touch
tags:
  - app store
  - apple
  - game dev
  - indie game
  - indie game dev
  - iOs
  - ios game
  - iPad
  - ipad game
  - iPhone
  - iphone game
  - screenshot
  - syrup
  - SyRUSH
  - waffle
featured: false
coverPhoto: /images/2014/09/syrush-release-twitter5.png
---

![](/images/2014/09/syrush-release-twitter5.png) 

Our latest game SyRUSH, which we have deemed “the official waffle simulator of breakfast,” is out now and available in the iOS App Store. Feel the rush in this fast-paced arcade style game where you tap waffle squares to fill your waffle with the perfect distribution of mouth-watering toppings. Full version of the game includes 7 unique toppings. 
 
![](/images/2014/09/syrush-leaderboard.png)

![](/images/2014/09/syrush-iphone-sm.gif)

![](/images/2014/09/syrush-achievements.png)  

This is our first leap into Apple’s GameCenter, which means SyRUSH has online leaderbords to compete with waffle afficianados across the globe, and fun achievements to unlock.  

The game is free to try, and 99 cents to purchase the full game(via in-app-purchase).  

Check it out here, and download it for iOS: [http://twoscoopgames.com/syrush](http://twoscoopgames.com/syrush)

