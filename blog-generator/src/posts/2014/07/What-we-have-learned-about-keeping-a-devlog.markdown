---
dateTime: 2014-07-22T18:46:53.000Z
year: 2014
month: 07
day: 22
time: 14.46.53
title: What we have learned about keeping a devlog
tags:
  - blog
  - blogging
  - devlog
  - game dev
  - game development
  - independant game studio
  - indie game
  - tigsource
  - tumblr
  - twitter
coverPhoto: /images/2014/07/ugh-marketing-partone.png
---

![image](/images/2014/07/ugh-marketing-partone.png)

### “I’m an indie game developer just starting out, how will anyone know that we exist?”

## Thats a good question; They won’t!

Being independent means you have to be your own marketing and PR, in addition to finding time around work, school, and or family to work on programming or creating art for your game.



### A devlog one way way to show others your progress in making your game

This makes things tough – you might feel like you are having to switch between many hats, and do things that are not your greatest skill.

## But what do I show? I’ve never kept up a regular blog before…

What helps and what we have been trying our best to stick to is keeping lists of what features/bugs/additions you are making to your game each session, and then each time you have a chunk of tasks checked off the list, write a bit about them, take some screenshots, or better yet - *gifs* - and post it on your devlog.

Additionally you can share current builds if you like, but at the bare minimum, some screenshots and (if you can) animated gifs are a really good way to show off your fun animations!

For most types of games we can think of, and in almost all cases **keeping a devlog can't harm you.** It you don’t have a website, don’t worry, you don’t need to setup a full site (though it is also really helpful) there are tons of free blogging services out there but we think **[Tumblr](http://tumblr.com)** is a good choice simply because of the community. It’s easy to get eyes on your blog with [tumblr](http://tumblr.com) and easy to use, update, and maintain. In conjunction with your own blog, also get on [tigsource](http://tigsource.com) (The Indie Game Source) forums where they have a whole devlogs section. You should keep both your devlog blog and [tigsource](http://tigsource.com) devlogs in sync and talk to anyone who comments.

## It’s about engaging your audience

We don’t want to sound like a ‘marketing guy,’ heck, they might make me turn in my ‘indie’ card if we talk about SEO… but **you want to engage your audience**. The people who play your games are your best friends, treat them that way. Give your [twitter](http://twitter.com) & [tumblr](http://tumblr.com) followers sneak peaks and updates, keep them informed of each cool new feature you add and they will feel like they are part of the process, and keep checking in with you. Just from my experience with doing this stuff for 6 months or so, you start to love engaging with people and when you’re starting from nothing like we all are, eyeballs are going to help a ton. **Those eyes might carry over to your next game, and the one after that, and so on.**

### Other great resources to learn about marketing your Independent games:

*   [Pixel Prospector](http://www.http://www.pixelprospector.com/the-big-list-of-indie-game-marketing/.com)
*   [Indie Game Girl](http://www.indiegamegirl.com)
*   [Zero Budget Marketing](http://www.gamasutra.com/blogs/VaheKocharyan/20140628/219850/Zero_Budget_Marketing.php) – Gamasutra
*   [Zero Budget Marketing](http://unity3d.com/learn/tutorials/modules/beginner/marketing-and-pr/zero-budget-marketing) – Unity3D
