---
dateTime: 2014-07-29T03:26:40.000Z
year: 2014
month: 07
day: 28
time: 23.26.40
title: Some Cupacabra fan art for our pal Larry
tags:
  - art
  - chupacabra
  - drawing
  - larry the chupacabra
featured: false
coverPhoto: /images/2014/07/chupa.png
---

![](/images/2014/07/chupa.png)  
![](/images/2014/07/chupacabra.png)  
Just practicing drawing more and we love the cartoon Larry the Chupacabra uses for his avatar, check out [his Youtube channel](https://www.youtube.com/channel/UCclRgnIvNTuptDoPI76YzFw)!

Thanks Larry for trying out game, and making videos of them.