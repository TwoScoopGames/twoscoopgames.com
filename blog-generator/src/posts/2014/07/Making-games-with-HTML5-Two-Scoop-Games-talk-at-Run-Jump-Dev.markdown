---
dateTime: 2014-07-30T16:04:03.000Z
year: 2014
month: 07
day: 30
time: 12.04.03
title: Making games with HTML5 - Two Scoop Games talk at Run Jump Dev
featured: false
coverPhoto: /images/2014/07/twoscooptalk.jpg
---

We recently gave a talk at Run Jump Dev, Lexington Kentucky’s game development group on creating games with HTML5 JavaScript. The location was a local co-working space called Awesome Inc. The talk went really well there was a pretty good attendance. We had the pleasure of meeting a lot of local game developers from Lexington and Louisville areas. I’m very excited about the things that Run Jump Dev is doing and hope to be a part of some of the events taking place in the future. Check out Run Jump Dev’s website here at [http://RunJumpDev.org](http://RunJumpDev.org).  

![](/images/2014/07/twoscooptalk.jpg)  
![](/images/2014/07/twoscooptalk2.jpg)  

I have included an MP3 recording so you can listen to it if you like. We had a great time and hope to visit again soon!  

[Download the MP3 here](/files/2014/07/Making-Games-with-HTML5-Two-Scoop-Games-talk-at-Run-Jump-Dev.mp3)

