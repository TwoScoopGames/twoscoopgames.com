---
dateTime: 2020-01-10T00:00:00.000Z
year: 2020
month: 01
day: 10
time: 13.09.18
title: We have a Discord! Join to play the free Kick Bot Demo!
featured: false
coverPhoto: /images/2020/discord-kick-bot-demo.png
---

Join our Two Scoop Games Discord where you can be one of the first people to play the new Kick Bot demo!
http://discord.gg/twoscoopgames

![Join the two Scoop Games Discord and play the free Kick Bot demo!](/images/2020/discord-kick-bot-demo.png)  
