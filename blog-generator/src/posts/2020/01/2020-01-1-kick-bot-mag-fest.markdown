---
dateTime: 2020-01-01T00:00:00.000Z
year: 2020
month: 01
day: 01
time: 13.09.18
title: Come play Kick Bot at MAGFest jan 2-5, 2020!
featured: false
coverPhoto: /images/2020/kick-bot-magfest-ad.jpg
---
![Come play Kick Bot at MAGFest jan 2-5, 2020!](/images/2020/kick-bot-magfest-ad.jpg)
MAGFest here we come! Come try our game Kick Bot, playable 24/7 at
MAGFest later this week in DC!

https://super.magfest.org/mivs2020/kick_bot

![Come play Kick Bot at MAGFest jan 2-5, 2020!](/images/2020/magfest-2020-prizes.png)

We are hosting a Kick Bot speedrun tournament. Best demo completion time between wins this beautiful Figma Samus and can name their own level in the upcoming Kick Bot game!  Second place will nab this super cool Metroid toy!

Stop by our booth today and follow
http://twitter.com/twoscoopgames on twitter for details!
