---
dateTime: 2020-09-10T00:00:00.000Z
year: 2020
month: 09
day: 10
time: 00.00.00
title: Game Dev Quick Tip - Open Steam Client App Directly from your game
blurb: In this tutorial we will show you how to create a link in your game that will open in the player's Steam client app
featured: false
coverPhoto: /images/2020/gamedev-steamlink.png

youtube:
    - uvKVp3-pzsw
---

[youtube-0]

We recently found a cool thing when working on our upcoming game Kick Bot. We wanted an easy way to get people to go from our game demo to wishlist Kick Bot on Steam.

### Direct link to Steam Client

Most game engines have a way to open a link in the player's browser but this tip goes one step further and opens the Steam page for your game in the player's Steam client app on their computer directly.

To do this you need a special URL: "steam://store/" + your game's AppID

There are two methods to get your game's AppID on Steam.

Through the steam client you can go to your game's page and click this link using the middle mouse button (usually clicking down the scroll wheel) This will open the game's page in a new window with a URL bar at the top. Your game's AppID is will be in the URL bar at the top.

The other method is from your web browser, navigate to your game and the AppID will be in the URL bar.

Depending on your game engine you will open URLs in different ways, I will go over how we did it in Unity.



### Opening a URL in Unity

The main bit of code you need is Application.OpenURL()

<pre><code class="language-csharp line-numbers">Application.OpenURL("steam://store/1136290/");
</code></pre>

This is completely optional but to make this script more versatile we created a function called "GoToURL" where you can specify any URL you want as a string. This makes it easy to use in Unity Events and more.

<pre data-src="/code/OpenURL.cs" data-download-link><code class="language-csharp line-numbers">using UnityEngine;

public class OpenURL : MonoBehaviour {

  public void GoToURL(string url) {
    Application.OpenURL(url);
  }
}
</code></pre>


I hope this is helpful, thanks for watching and be sure to wishlist Kick Bot on Steam and join our Discord to try the free demo today!

You can try the free Kick Bot demo for yourself by joining our Discord: http://discord.gg/twoscoopgames

Thanks again, and be sure to tell your friends about the our game Kick Bot!

If you want to help us out:

Wishlist Kick Bot on Steam here: http://wishlist.kickbotgame.com

Become a patron on Patreon and get all the latest development updates first: https://www.patreon.com/twoscoopgames

More info about the game: http://kickbotgame.com
