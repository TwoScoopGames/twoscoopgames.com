---
dateTime: 2020-09-08T00:00:00.000Z
year: 2020
month: 09
day: 08
time: 15.18.30
title: Watch our playtest / interview with Wonderville NYC about Kick Bot
blurb: Eric and I had a great time last night talking with Matt and Mark from Wonderville NYC, an indie arcade bar in Brooklyn, New York as part of their Remote-CTRL series of twitch streams!
featured: false
coverPhoto: /images/2020/wonderville-stream-screenshot.png

---
Eric and I had a great time last night talking with Matt and Mark from [Wonderville NYC](https://www.wonderville.nyc/), an indie arcade bar in Brooklyn, New York as part of their Remote-CTRL series of twitch streams!

Mark played Kick Bot while Matt asked us questions about the game concept, development and more!
Check out the full interview here:
[youtube-0]


(direct link: https://www.youtube.com/watch?v=84OGFYxwC30&ab_channel=wonderville)

The Wonderville folks were great to hang out with and Mark was able to get through all 32 levels in our recent beta build of Kick Bot!

You can try the free Kick Bot demo for yourself by joining our Discord: http://discord.gg/twoscoopgames

Thanks again, and be sure to tell your friends about the our game Kick Bot!

If you want to help us out:

Wishlist Kick Bot on Steam here: (http://wishlist.kickbotgame.com)[http://wishlist.kickbotgame.com]

Become a patron on Patreon and get all the latest development updates first: (https://www.patreon.com/twoscoopgames)[https://www.patreon.com/twoscoopgames]

More info about the game: (http://kickbotgame.com)[http://kickbotgame.com]
