---
dateTime: 2020-09-06T00:00:00.000Z
year: 2020
month: 09
day: 06
time: 15.18.30
title: We're streaming w/ Wonderville NYC Sept 7
featured: false
coverPhoto: /images/2020/kick-bot-wonderville-promo.png
---


![](/images/2020/kick-bot-wonderville-promo.png)

 We're streaming with Wonderville NYC Monday Sept 7 at 7pm EST to talk about our upcoming game Kick Bot!

 The event is part of Wonderville's new Remote CTRL series where they playtest games by indies live and chat with the developers.

 Join us at http://twitch.tv/wondervillenyc!
