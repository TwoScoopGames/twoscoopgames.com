---
dateTime: 2020-03-25T00:00:00.000Z
year: 2020
month: 03
day: 25
time: 13.09.18
title: 6 years of Two Scoop Games!
featured: false
coverPhoto: /images/2020/6th-anniversary.png
---
![6 year anniversary of Two Scoop Games!](/images/2020/6th-anniversary.png)

It's our studio's 6th anniversary! Two Scoop Games was founded on March 25th, 2014.

We didn't celebrate in person this year due to COVID-19, but check out this 5-year retrospective I wrote last year:

http://blog.twoscoopgames.com/posts/2019/03/5-year-scoop-iversary.html
