---
dateTime: 2020-10-12T00:00:00.000Z
year: 2020
month: 10
day: 12
time: 15.18.30
title: Kick Bot Soundtrack Q+A with Composer Jake Mercer
blurb:
featured: false
coverPhoto: /images/2020/jake-mercer-ost-qa.png
youtube:
  - 0rEkJslOdsY
---

[youtube-0]


(direct link: https://www.youtube.com/watch?v=0rEkJslOdsY)

On Friday Oct 9, 2020 Alex from Two Scoop Games sat on a call with Kick Bot composer Jake Mercer to answer questions form the community about his work on the music for the game.


If you want to help us out:

Wishlist Kick Bot on Steam here: (http://wishlist.kickbotgame.com)[http://wishlist.kickbotgame.com]

Become a patron on Patreon and get all the latest development updates first: (https://www.patreon.com/twoscoopgames)[https://www.patreon.com/twoscoopgames]

More info about the game: (http://kickbotgame.com)[http://kickbotgame.com]
