---
dateTime: 2020-11-3T00:00:00.000Z
year: 2020
month: 11
day: 3
time: 00.00.00
title: Happy Halloween! – We made tinder for ghosts!
blurb:
author: Alex Bezuska
featured: false
coverPhoto: /images/2020/spooky-singles-share-graphic-2.png
youtube:
  - EB1bTl3EzeY
  - zcfDRCfGkwE
  - LqGsFq6I7-E
  - s6jMOCY0SSQ
---

[youtube-0]
(direct link: https://www.youtube.com/watch?v=EB1bTl3EzeY)

## Spooky.singles - Tinder for ghosts
Just to start off Spooky.singles is a completely free app Eric and I made just for fun, we have no intent to make money on this project, no ads or anything – just a free little fun thing. I like to say we made it to make people laugh in the year 2020, which has been a tough time for so many people.

### 6HourSpookyJam
It all started with the annual 6 Hour Spooky jam, which is a jam our local gamedev community [Louisville Makes Games](http://louisvillemakesgames.org) hosts each year. This year Eric and I came into it with a bit of a plan – we at least knew we wanted to pursue an idea Eric had mentioned in passing one day "Tinder for ghosts".

We started the jam off with a short planning session then got to work. Since we are web developers in our day jobs we decided that using web technologies to create a parody website made sense. If we had created the app with Unity it would have been WAY more involved to get folks playing this on their phones since we would have to get it into the app stores on both platforms, whereas with a true web app people can just open a link and start playing (yes I know Unity web player exists but it works really poorly on mobile and is way slower than a simple web page).

Eric created a skeleton (pun intended) web app using web frameworks he is familiar with to save time. The frontend of the site is JavaScript with React, while the backend is Elixir using the Phoenix library. My job was to handle the UX and UI of the app, so I used my CSS skills to make it look as good as I could in the given time!

#### The basic concept was:
- Users can create their own spooky dating profiles
- You swipe left or right to reject or accept other ghosts
- Matches that both accept each-other can chat using a keyboard of limited responses and emojis predetermined by us.

[youtube-1]

### Scope
We really overestimated what we could get done in 6 hours! By the end of the 6 hour jam we had the profile creation done, and a lot of the bones were there (pun intended) for the matches and chat components - but that was it.

### Chat
The chat feature was something we both agreed needed to be locked down to responses we came up with ahead of time. The goal for the chat was to keep it pretty wholesome – allow users to be a bit flirty but mostly keep it PG with a little innuendo possible with emojis and such. There is no way to actually type your own responses in the chat, you can only pick from a few buttons on our custom keyboard such as "Questions", "Answers", "Small talk", and "Spicy", we also have an emoji mode where you can freely type from a select group of emojis.

The chat experience end up looking like this:
[youtube-2]

We were pretty excited with the project and after the jam presentations via video call with the other participants we worked on the game for about 6 additional hours that day after the 6 hour jam, and then even more on evenings the following week to get the game ready for a Halloween release.

All and all I think we worked on this in all our free time for about one calendar week, tweaking, updating, adding notification features, more art for the characters, and tons more text for the name, bio, and conversation generators.

![Spooky.singles characters](/images/2020/spooky-singles-characters.png)

We released the game on October 30th and told our friends in the local gamedev community [Louisville Makes Games](http://louisvillemakesgames.org) first to get some of the user generated content flowing, then promoted a bit on social media the next day on Halloween.

Our posts on reddit didn't seem to get much response, I think folks might have assumed we were trying to sell them something... but the site is just meant to be a joke and something fun to enjoy for free. (you don't even have to make an account with any personal info or anything!)

All in all I am happy with how the app turned out! I think it would be worth our time next year to add some updates like more artwork, text, a couple new features etc and try to promote it right at the beginning of October. I think giving people a chance to play with it for the whole spooky month would be the way to go.


[youtube-3]

### Stats
So how many people created ghosts? According to Google Analytics we had 448 users create ghosts between Oct 28 and Nov 2, with 237 of those people on Halloween!

Thats a lot of ghouls!

### Follow [@Spooky_Singles](https://twitter.com/Spooky_Singles) on twitter

I set up a new twitter account for spooky.singles to show off some of the ghoulish creations our users come up with. Follow [@Spooky_Singles](https://twitter.com/Spooky_Singles) on Twitter to see posts like the "Eligible Ghoul of the day". Tweet us your own screenshots if you create a great profile or have a funny conversation and we will share it as well!

![Follow @spooky_singles on Twitter to see posts like the "Eligible Ghoul of the day"](/images/2020/spooky-singles-twitter.png)


Go to [spooky.singles](https://spooky.singles) to create your own spooky monster dating profile and start chatting with hot ghouls today!
Completely free Halloween fun from Two Scoop Games!
