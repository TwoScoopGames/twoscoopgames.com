/*
Custom arcade controller by Two Scoop Games 
twoscoopgames.com
*/

#include <Keyboard.h>

unsigned long debounceDelay = 16;

// Button A
int aPin = 8;
char aKey = KEY_A;
int aButtonState = LOW;
int aLastButtonState = LOW;
unsigned long leftLastDebounceTime = 0;

// Button B
int bPin = 9;
char bKey = KEY_B;
int bButtonState = LOW;
int bLastButtonState = LOW;
unsigned long leftLastDebounceTime = 0;

// Joystick Up
int upPin = 10;
char upKey = KEY_UP_ARROW;
int upButtonState = LOW;
int upLastButtonState = LOW;
unsigned long leftLastDebounceTime = 0;

// Joystick Down
int downPin = 16;
char downKey = KEY_DOWN_ARROW;
int downButtonState = LOW;
int downLastButtonState = LOW;
unsigned long leftLastDebounceTime = 0;

// Joystick Left
int leftPin = 15;
char leftKey = KEY_LEFT_ARROW;
int leftButtonState = LOW;
int leftLastButtonState = LOW;
unsigned long leftLastDebounceTime = 0;

// Joystick Right
int rightPin = 14;
char rightKey = KEY_RIGHT_ARROW;
int rightButtonState = LOW;
int rightLastButtonState = LOW;
unsigned long leftLastDebounceTime = 0;

void setup() {
    // Button A
    pinMode(aPin, INPUT_PULLUP);
    
    // Button B
    pinMode(bPin, INPUT_PULLUP);
    
    // Joystick Up
    pinMode(upPin, INPUT_PULLUP);
    
    // Joystick Down
    pinMode(downPin, INPUT_PULLUP);
    
    // Joystick Left
    pinMode(leftPin, INPUT_PULLUP);
    
    // Joystick Right
    pinMode(rightPin, INPUT_PULLUP);
    
    Keyboard.begin();
}

void loop() {
    // Button A
    checkButton(aPin, &aButtonState, &aLastButtonState, &aLastDebounceTime, aKey);
    
    // Button B
    checkButton(bPin, &bButtonState, &bLastButtonState, &bLastDebounceTime, bKey);
    
    // Joystick Up
    checkButton(upPin, &upButtonState, &upLastButtonState, &upLastDebounceTime, upKey);
    
    // Joystick Down
    checkButton(downPin, &downButtonState, &downLastButtonState, &downLastDebounceTime, downKey);
    
    // Joystick Left
    checkButton(leftPin, &leftButtonState, &leftLastButtonState, &leftLastDebounceTime, leftKey);
    
    // Joystick Right
    checkButton(rightPin, &rightButtonState, &rightLastButtonState, &rightLastDebounceTime, rightKey);
    
    Keyboard.begin();
    
}

void checkButton(int buttonPin, int * buttonState, int * lastButtonState, unsigned long * lastDebounceTime, char keyToSend) {
    int reading = digitalRead(buttonPin);
    if (reading != *lastButtonState) {
        *lastDebounceTime = millis();
    }
    
    if ((millis() - *lastDebounceTime) > debounceDelay) {
        if (reading != *buttonState) {
            *buttonState = reading;
            if (*buttonState == HIGH) {
                Keyboard.release(keyToSend);
            } else {
                Keyboard.press(keyToSend);
            }
        }
    }
    *lastButtonState = reading;
}



