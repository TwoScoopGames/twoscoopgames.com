---
dateTime: 2019-08-26T00:00:00.000Z
year: 2019
month: 08
day: 26
time: 00.00.00
title: Kick Bot at Bit Bash 2019 - Highlights
featured: false
coverPhoto: /images/2019/08/two-scoop-update-bit-bash-hightlights.png
blurb: Great response to our upcoming game Kick Bot at Bit Bash in Chicago recently! 
youtube:
    - xh4-TEi6nUI
---


[youtube-0]


### What is Bit Bash?

Bit Bash is Chicago’s alternative arcade - a pop up event showcasing a selection of unique and creative indie games and interactive art experiences from around the world!

### Kick Bot at Bit Bash

We had a great time showing our upcoming game Kick Bot at Bit Bash in Chicago recently. The response to Kick Bot was very positive! 

We recorded stats on how the players did and we will be doing another post soon just about the stats we gathered.

Thanks Bit Bash Chicago for having us, fantastic event with great people running it, we hope to come again next year!

Find out more about the upcoming game Kick Bot here:
[Kick Bot teaser trailer](http://kickbotgame.com)  
  
[Wishlist Kick Bot on our Steam page](http://wishlist.kickbotgame.com)


[![](/images/2019/08/kick-bot-wishlit-email-image-v3.png)](http://wishlist.kickbotgame.com)
If you haven't heard already [Kick Bot is coming to Steam](http://wishlist.kickbotgame.com) - but we need your help! We need you and everyone you know to add Kick Bot to their wishlists. In addition to letting you know when the game comes out getting a huge number of wishlists will help us let Steam know that people want Kick Bot, which makes them more likely to feature Kick Bot and that's the kind of promotion we need to be able to continue making games. With so many games coming out - your wishlist is your vote. Help us show that Kick Bot is a game you want to play - go to [wishlist.kickbotgame.com](http://wishlist.kickbotgame.com) and click "add to your wishlist." 


