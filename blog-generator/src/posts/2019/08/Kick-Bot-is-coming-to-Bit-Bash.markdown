---
dateTime: 2019-08-08T00:00:00.000Z
year: 2019
month: 08
day: 08
time: 10.02.57
title: Kick Bot is coming to Bit Bash
featured: false
coverPhoto: /images/2019/08/kick-bot-bit-bash.png
youtube:
    - xFNZHaLYlkA
---


We have an exciting announcement about Kick Bot today!
#### Kick Bot was chosen to be shown at Bit Bash in Chicago, Illinois this August 17-18!

[youtube-0]


We have an awesome demo prepared that will give players a taste of what the game is all about and are very excited to see what players think and hear feedback.
The awesome thing about Bit Bash is that it is a curated show, with games chosen from around the world, Kick Bot will be in some very good company with many indie games you might have heard of! A big hope of mine is to meet people and make new friends in the industry while showing off all the hard work we have been putting into our game!

### What is Bit Bash?
Bit Bash is Chicago’s alternative arcade - a pop up event showcasing a selection of unique and creative indie games and interactive art experiences from around the world!

### Where and when is Bit Bash?
This year is the first time Bit Bash takes place at MSI, the Museum of Science and Industry (5700 S Lake Shore Drive, Chicago) and included with your ticket you get full access to the museum!
We will be showing Kick Bot on Saturday August 17 from 9:30am - midnight on Saturday, August 17 and from 9:30am - 4pm on Sunday August 18!

### Who is Bit Bash for?
Bit Bash is for anyone who loves games, art, creativity, and strangeness. Many of the experiences at Bit Bash are exclusively shown at events or even exclusively created for this event. 
For families with small children there is a special family preview on Saturday only from 9:30am to 11am. (Kick Bot will be playable for the entire event, including the Family Preview hours)
We are honored to be a part of this great event which we have attended several times. This is the first out of state showing of one of our games and we are very excited.
We have a special Bit Bash demo of Kick Bot planned which will include new levels never seen before this event.

### Another special announcement

In a few days, Friday August 16 we have an even bigger announcement about Kick Bot! 
You'll just have to wait and see what that is!

Want to get updates about Kick Bot or join the upcoming beta test? [Sign up here](http://bit.ly/kickbot)