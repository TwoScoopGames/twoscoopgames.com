---
dateTime: 2019-08-16T00:00:00.000Z
year: 2019
month: 08
day: 16
time: 00.00.00
title: Kick Bot is coming to Steam!
featured: false
coverPhoto: /images/2019/08/kick-bot-is-coming-to-steam.png
---


[![](/images/2019/08/kick-bot-wishlit-email-image-v3.png)](http://wishlist.kickbotgame.com)
If you haven't heard already [Kick Bot is coming to Steam](http://wishlist.kickbotgame.com) – but we need your help! 

We need you and everyone you know to add Kick Bot to their wishlists. 

### Why wishlists matter

In addition to letting you know when the game comes out, getting a huge number of wishlists will help us let Steam know that people want Kick Bot, which makes them more likely to feature Kick Bot. That's the kind of promotion we need to be able to continue making games.

 With so many games coming out – your wishlist is your vote. Help us show that Kick Bot is a game you want to play – go to [wishlist.kickbotgame.com](http://wishlist.kickbotgame.com) and click "add to your wishlist."   
### Thanks

[![](/images/2019/08/kick-bot-is-coming-to-steam.gif)](http://wishlist.kickbotgame.com)




