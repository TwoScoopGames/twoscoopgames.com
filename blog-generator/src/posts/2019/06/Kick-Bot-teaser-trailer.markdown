---
dateTime: 2019-06-15T00:00:00.000Z
year: 2019
month: 06
day: 15
time: 00.00.00
title: Kick Bot teaser trailer
blurb: Check out the new trailer and sign up to be a beta tester
featured: false
tags:
  - events
  - kickbot
coverPhoto: /images/2019/06/kickbotgame-kick-earth.gif
coverPhotoAlt: New trailer for Kick Bot
youtube:
  - xc6wudU5_CI
---

[youtube-0]

This last week we put a lot of effort into the new trailer for our game Kick Bot! We hope to show it at some events across the midwest to get playtesting and build the community around the game.


Early pre-alpha footage, Gameplay subject to change.  

*An evil AI banished to the moon has built a giant robot leg and plans to kick the Earth! You are Kick Bot, the only hope for humanity, sent into space to wall-jump through treacherous fast-paced pixel-art levels using only two buttons! Destroy the GigaLeg from the inside and defeat the AI at all costs! Kick Bot DX coming soon to console and PC from Two Scoop Games!*

also "Kick Bot DX" is now "Kick Bot"

Music by [Jake Mercer](https://soundcloud.com/jake-mercer-5)

Sign up for the beta and email updates here:

[Join the Beta](http://bit.ly/kickbot)

[Get Email Updates](http://bit.ly/kickbot)
