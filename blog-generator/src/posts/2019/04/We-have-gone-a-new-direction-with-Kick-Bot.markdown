---
dateTime: 2019-04-16T01:33:31.000Z
year: 2019
month: 04
day: 15
time: 21.33.31
title: We have gone a new direction with Kick Bot!
blurb: We have switched to developing Kick Bot for consoles and PC
tags:
  - kickbot
  - announcements
featured: false
coverPhoto: /images/2019/04/tumblr_inline_pq15n402TI1sh27ku_540.gif
coverPhotoAlt: We have switched to developing Kick Bot for consoles and PC
---


**Warning: the game described on this post is a prototype - features and visuals have changed so drastically that we would appreciate if you don't use them if writing about the game. This post is only for historical purposes. Please look at the [official Kick Bot Presskit for videos, gifs, and screenshots of the current game](http://kickbotgame.com)**

![](/images/2019/04/tumblr_inline_pq15n402TI1sh27ku_540.gif)

New art style, new gameplay, more variety, and more content.  

![](/images/2019/04/tumblr_inline_pq15niETkZ1sh27ku_540.gif)  

While continuing to work on [Kick Bot](http://twoscoopgames.com/kickbot/) this year we decided that we liked a lot of things about the game, except for the core gameplay - which is a pretty big problem. We also were hearing more and more horror stories about the mobile game landscape and feeling like there was less of a chance to "make it" (be a self-sustaining game studio) if we were targeting such a saturated platform. We decided to explore something new and began re-writing the game from scratch, this time with more variety, hand-crafted levels, and an entirely different set of target platforms - Console and PC. After I created some concept art based on Game Boy Advance era games, we had a boost in energy and have been happily working on the new version of [Kick Bot](http://twoscoopgames.com/kickbot/) ever since.

![](http://twoscoopgames.com/img/kickbotgame-gameplay-sm2.gif)

We want to bring you the best Kick Bot possible, that means opening ourselves up to releasing on different platforms.

![](/images/2019/04/tumblr_inline_pq15n5D1Bq1sh27ku_540.gif)

Sign up for the beta and let us know how you play games!  
[http://bit.ly/kickbot](http://bit.ly/kickbot)
