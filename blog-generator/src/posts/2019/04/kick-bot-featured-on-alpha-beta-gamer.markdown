---
dateTime: 2019-04-16T01:31:18.000Z
year: 2019
month: 04
day: 15
time: 21.31.18
title: Kick Bot featured on Alpha Beta Gamer
blurb: Our new video caught the eye of a popular game beta blog
tags:
  - Kick bot
  - Press
featured: false
coverPhoto: /images/2019/04/tumblr_inline_pq15k5eUNj1sh27ku_540.png
---

**Warning: the game described on this post is a prototype - features and visuals have changed so drastically that we would appreciate if you don't use them if writing about the game. This post is only for historical purposes. Please look at the [official Kick Bot Presskit for videos, gifs, and screenshots of the current game](http://kickbotgame.com)**

![](/images/2019/04/tumblr_inline_pq15k5eUNj1sh27ku_540.png)
 
Big thanks to Alpha Beta Gamer for doing a write up about Kick Bot DX. We can’t wait to show off more of the cool stuff we have planned for Kick Bot!  

You can check out the article here:  
[https://www.alphabetagamer.com/kick-bot-dx-beta-sign-up/](https://www.alphabetagamer.com/kick-bot-dx-beta-sign-up/)
