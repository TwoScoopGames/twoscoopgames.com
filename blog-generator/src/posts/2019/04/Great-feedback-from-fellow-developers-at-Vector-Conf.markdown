---
dateTime: 2019-04-28T00:00:00.000Z
year: 2019
month: 04
day: 28
time: 00.00.00
title: Great feedback from fellow developers at Vector Conf
featured: false
tags:
  - events
  - kickbot
coverPhoto: /images/2019/04/rami-playing-kickbot.jpg
coverPhotoAlt: Rami Ismail playing Kick Bot by Two Scoop Games at Vector Conf
---
![](/images/2019/04/rami-playing-kickbot.jpg)
We had a fantastic time at [Vector Conf](http://vectorconf.com/) this year, great people, great talks!  

Thanks so much to everyone who took the time to try [Kick Bot](http://twoscoopgames.com/kickbot) and give us valuable feedback including [Lisa Brown](http://twitter.com/Wertle) and [Rami Ismail](http://twitter.com/tha_rami)!
