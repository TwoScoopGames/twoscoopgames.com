---
dateTime: 2019-05-11T00:00:00.000Z
year: 2019
month: 05
day: 11
time: 00.00.00
title: Kick Bot progress - Louisville Makes Games May play-testing night
blurb: Folks trying out the latest build of Kick Bot at Warp Zone
tags:
  - events
  - kickbot
featured: false
coverPhoto: /images/2019/05/playtesting-kickbot-1.jpg
---


**Warning: the game described on this post is a prototype - features and visuals have changed so drastically that we would appreciate if you don't use them if writing about the game. This post is only for historical purposes. Please look at the [official Kick Bot Presskit for videos, gifs, and screenshots of the current game](http://kickbotgame.com)**

![](/images/2019/05/playtesting-kickbot-1.jpg)


At Warp Zone Louisville, our local gamedev space, we hold semi-regular playtesting nights where the public is invited to come and try games still in development by members of the community.

We always try to have something to show because feedback from a random sampling of people is vital to game development.

![](/images/2019/05/playtesting-kickbot-2.jpg)

This was the first version with distinct seperate levels in the same order each time, and with checkpoints.
Previously randomized the levels each time you die, and had the player start over each time, much like the older arcade-style [Kick Bot on the Chrome Web Store](http://twoscoopgames.com/kickbotclassic)

We had a great turnout and people seemed to enjoy the latest version of Kick Bot.

![](/images/2019/05/playtesting-kickbot-3.jpg)
