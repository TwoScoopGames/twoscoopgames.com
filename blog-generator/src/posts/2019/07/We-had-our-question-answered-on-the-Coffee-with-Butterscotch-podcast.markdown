---
dateTime: 2019-07-03T00:00:00.000Z
year: 2019
month: 07
day: 03
time: 00.00.00
title: We had our question answered on the Coffee with Butterscotch podcast
featured: false
coverPhoto: /images/2019/07/coffee-with-butterscotch.png
---


![](/images/2019/07/coffee-with-butterscotch.png)

[Butterscotch Shenanigans](https://www.bscotch.net/) is an indie games studio based out of Saint Louis, Missouri who are best known for the game [Crashlands](https://www.bscotch.net/games/crashlands).

We have been listening to their podcast [Coffee with Butterscotch](https://www.bscotch.net/podcast) since 2017, but have gone back to the beginning and listed to every episode at this point (currently 211 episodes)

[Coffee with Butterscotch](https://www.bscotch.net/podcast) is a fantastic podcast where the Coster brothers, Sam, Seth, and Adam who make up the core Butterscotch team talk about life , business, and the games industry. Their dedication and fearless trying of new things is encouraging and inspiring and we definitely recommend checking their podcast out (and buying their games!)


Our question for the podcast had to do with asking for an ice cream recommendation in Saint Louis for when we visit their home turf this September to attend [Pixel Pop Festival](http://pixelpopfestival.com/).

They unanimously agreed that [Clementine’s](https://www.clementinescreamery.com/) was the place to go, and so we will have to check it out and report back!

Here is the snippet of the Coffee with Butterscotch podcast episode 211 where our question is answered:

[Two Scoop Games' question on Coffee with Butterscotch](/files/2019/07/coffee-with-butterscotch-ep-211-two-scoop-games-question.mp3)

Here is a link to the full episode: [Coffee with Butterscotch podcast episode 211: The Mongers You Know](https://www.bscotch.net/podcast/211)

Be sure to subscribe to their show!

Thanks guys!




