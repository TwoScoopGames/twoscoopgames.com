---
dateTime: 2019-11-15T00:00:00.000Z
year: 2019
month: 11
day: 15
time: 10.49.05
title: Intro to Animation with Aseprite
featured: false
coverPhoto: /images/2019/intro-to-aseprite.png
youtube:
    - 2O4tHOSqcgc
---
[youtube-0]

We recently held a lightning talks night at my local game dev space Warp Zone Louisville (http://louisvillegamesgames.org), lighting talks are short 5-15min talks to give someone an intro to a topic, get them excited to learn more, and provide a few resources. In my talk, I gave a quick

Thanks again, and be sure to tell your friends about the our game Kick Bot!

If you want to help us out:
Wishlist Kick Bot on Steam here: (http://wishlist.kickbotgame.com)[http://wishlist.kickbotgame.com]
Become a patron on Patreon and get all the latest development updates first: (https://www.patreon.com/twoscoopgames)[https://www.patreon.com/twoscoopgames]
More info about the game: (http://kickbotgame.com)[http://kickbotgame.com]
