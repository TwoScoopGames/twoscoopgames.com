---
dateTime: 2019-11-16T00:00:00.000Z
year: 2019
month: 11
day: 16
time: 10.49.05
title: Kick Bot - New Teleporters Mechanic!
featured: false
coverPhoto: /images/2019/teleporters.png
youtube:
    - EOCHOMQdQns
    - y7YJ2_uAczg
---
[youtube-0]

Recently at our local game dev space Warp Zone Louisville (http://louisvillegamesgames.org), we had a show and tell session where Eric talked about how he smoothed out the camera in Kick Bot when using the new teleporters.

[youtube-1]
The new teleporters we added to Kick Bot during the #6hourspookyjam, let us know what you think!

Thanks again, and be sure to tell your friends about the game!

If you want to help us out:
Wishlist Kick Bot on Steam here: (http://wishlist.kickbotgame.com)[http://wishlist.kickbotgame.com]
Become a patron on Patreon and get all the latest development updates first: (https://www.patreon.com/twoscoopgames)[https://www.patreon.com/twoscoopgames]
More info about the game: (http://kickbotgame.com)[http://kickbotgame.com]
