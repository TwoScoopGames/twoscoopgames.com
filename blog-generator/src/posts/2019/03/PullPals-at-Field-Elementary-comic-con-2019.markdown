---
dateTime: 2019-03-21T00:00:00.000Z
year: 2019
month: 03
day: 21
time: 00.00.00
title: PullPals at Field Elementary School Comic Con 2019
blurb: Bringing the strechy baby monster puzzle games directly to this nation's youth
tags:
  - events
  - pullpals
featured: false
coverPhoto: /images/2019/05/pullpals-at-field-2019.png
coverPhotoAlt: PullPals at Field Elementary School Comic Con
---

![PullPals at Field Elementary School Comic Con](/images/2019/05/pullpals-at-field-2019.png)


We showed our PullPals demo at Field Elementary again this year. 

Bringing the strechy baby monster puzzle games directly to this nation's youth, one school at a time.

PullPals is a game we are working on, [find out more and see the teaser trailer here](http://twoscoopgames.com/pullpals/)

### Thanks so much for having us!
