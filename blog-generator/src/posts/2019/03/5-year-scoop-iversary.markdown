---
dateTime: 2019-03-25T00:00:00.000Z
year: 2019
month: 03
day: 25
time: 00.00.00
title: 5 Year Scoop-iversary
featured: false
coverPhoto: /images/2019/03/5year-aniversary.jpg
---


![](/images/2019/03/5year-aniversary.jpg)

Eric and I celebrated the 5-year anniversary of our studio, Two Scoop Games!
I wanted to take a few minutes to look back on what we have created in those 5 years, our goals, and what is next for Two Scoop Games.

#### 2014

![](http://twoscoopgames.com/img/scurry-ss-1.png)

I met Eric in 2014 at a local developer group meetup and saw he was working on a game to help his brother learn programming. At the time I was working on several attempts to start making games on my own, and coming from an art background, I offered to make some art for his JS Roof Runner game. From that point on, we met each week and worked on the game which became [Scurry](http://twoscoopgames.com/scurry/), this was also the start of Eric's JavaScript/HTML5 game engine "Splat JS". In Scurry you play as a cockroach who is running through a grocery store, avoiding obstacles, getting sweet power-ups, and tring not to fall off the shelves. We would later release Scurry on the web, Chrome Web Store, and iOS App Store in June 2014.

![](/images/2019/07/twoscoopgames-makerfaire-2014.jpg)

After Eric and I met we looked for other game developers in our city, I found out about game jams – events where people get together, are provided a theme, form teams and work to create a new game based on that theme in a set timeframe. We ended up working with several other individuals to host the first Global Game Jam in Louisville, Kentucky in January 2014. At this jam, Eric and I worked with 2 other folks to create [Apartment 213 ](http://twoscoopgames.com/apartment213/) a short story game where you change characters and perspectives each time you enter an apartment. We had a blast and many of the people we met at this game jam ended up being good friends after, and we started a game development community by merging the game development group based out of the local hacker space ([LVL 1](http://www.lvl1.org/)) and the participants of the Global Game Jam to form what is now [Louisville Makes Games](https://louisvillemakesgames.org/)).

![](http://twoscoopgames.com/img/apartment213-hover.gif)


On March 25, 2014 we officially formed Two Scoop Games as an LLC.
We both had (and still do) day jobs, and the goal is to make a living off of our own game creations eventually.
The "Two Scoop Games" name was Eric's idea during a brainstorming session, and I loved it from the first time I heard it. We both love ice cream and that's basically all there is to it!


The next month we released our first game [Kick Bot (Classic)](http://twoscoopgames.com/kickbotclassic/), created for the Flappy Jam, a game jam where you try to make a hard, almost unplayable game (in the spirit of then popular Flappy Bird). This jam was a great break from Scurry, which we had become burnt out on. I remember working at Eric's apartment and finishing the game in a single day (it ranked #23 in coolness in over 600 submissions). Shortly after we polished things up a bit and released [Kick Bot (Classic)](http://twoscoopgames.com/kickbotclassic/) on April 30th, 2014 on the App Store and Chrome Web Store. This surprisingly became our most successful game in terms of number of players, peaking at over 70,000 on Chrome Web Store this year) but with it being a free game, and it not containing ads we never were able to make any money off of this project.

![](http://twoscoopgames.com/img/kickbotclassic-ss-5.png)

After the release of [Kick Bot (Classic)](http://twoscoopgames.com/kickbotclassic/), the next game jam we did that year was our first Ludum Dare (another worldwide game jam event that happens multiple times per year) in April 2014, where we worked on a pretty big team including several friends from previous jams including [Rex Soriano](http://twitter.com/lolo_r), [Anthony Quisenberry](http://twitter.com/aqquisenberry), [Nathan Hutchens](http://twitter.com/TheLucidBard), and [Aaron Smith](http://twitter.com/aarondoodles) to create a triage game where you are the cat who manages internet traffic called [Mr. Fluffykins' Great Sorting Adventure ](http://twoscoopgames.com/fluffykins/)  

![](http://twoscoopgames.com/img/fluffykins-hover.gif)


With our next game, [SyRUSH](http://twoscoopgames.com/syrush/), the name comes from rushing to fill all the squares in a waffle with syrup and toppings evenly. We released SyRUSH on iOS in August 2014, and we learned a lot about how not to launch a game! I still receive messages from friends who play the "Official waffle simulator of breakfast" (as we called it) and I would love to do a remake sometime in Unity with what we know now.

![](http://twoscoopgames.com/img/syrush-ss-1.png)

Another Ludum Dare came up in August 2014, for this one we created [Stanley Squeaks and the Emerald Burrito ](http://twoscoopgames.com/stanleysqueaks/) which is our first platformer prototype, where you play as an adventuring hamster delving into ancient ruins to obtain the illustrious artifact known as The Emerald Burrito. I even spoke briefly about this game on local news ([check out the video here](https://www.wdrb.com/wdrb-in-the-morning/finger-derpy-mobile-game-launches-horse-racing-app-just-in/article_df182555-2c93-53fc-b1df-27dd67b44cb9.html)).

![](http://twoscoopgames.com/img/stanleysqueaks-ss-4.png)

Around this time Eric gave a great talk about making HTML5 games at a JavaScript conference in Nashville called Nodevember.([you can check out the video here](https://www.youtube.com/watch?v=5UuRj_1OG14))


In December, we participated in another Ludum Dare and created [Desserts Killed Your Daddy ](http://twoscoopgames.com/desserts/), based on the mechanic of swinging a sword in the SNES classic The Legend of Zelda - A Link to the Past, we created a game where you are a chef hat brandishing a rolling pin to fend off evil baked goods come to life!

![](http://twoscoopgames.com/img/desserts-ss-1.png)

#### 2015

![](/images/2019/07/5year-shark.png)
To kick off 2015, we hosted another Global Game Jam in Louisville, this time Eric and I created a game called [The Day the World Changed ](http://twoscoopgames.com/thedaytheworldchanged/). The Day the World Changed started off as a joke Eric came up with in the brainstorming session that we could not stop thinking and laughing about, we knew we had to make it! The theme was "What do we do now?", and Eric said (to the best of my memory)  "What if there's a couple lying in bed and the the woman tells the man 'honey, I'm pregnant', the man says 'What do we do now?', the next scene they are at the hospital and the woman gives birth – and it's a @%#^$ shark!". We took this absurd game concept veiled by a faux-somber introduction and rolled with it. The concept evolved to be an infini-shark, growing in length like the game snake with everything the shark eats. This was also our first game with screenshake, which is a huge step toward becoming real game devs.

![](https://globalgamejam.org/sites/default/files/styles/game_sidebar__wide/public/games/team_pictures/img_20150125_190125.jpg?itok=pVsyXEeV&timestamp=1422230796)


In April 2015 we participated in another Ludum Dare, this time the theme was "unconventional weapon", which of course led us to creating a game where you play the role of a leaping carrot fighting back against bunnies on treadmills. This was [Uprooted ](http://twoscoopgames.com/uprooted/), which was our first game with particle effects, and our first game to be featured by a popular Korean YouTuber TaeKyung TV (the video has over 200,000 views!)

![](http://twoscoopgames.com/img/uprooted-ss-3.png)

When Ludum Dare in August 2015 rolled around we decided to create our first (for real this time) serious game. [Treatment and Control ](http://twoscoopgames.com/treatmentandcontrol/) is a game where you are a nameless worker administering an unknown medication to other workers in a dystopian military-controlled future. I am still impressed that we finished this, given that Eric was trying very hard to get our engine working with WebGL on the first day of the jam, when that was looking like it was not going to work out, he quickly switched gears and created the bulk of this game in a single day. I loved working on the sprites and dialogue for Treatment and Control, and it was really fun to see players try to figure it out.

![](http://twoscoopgames.com/img/treatmentandcontrol-ss-4.jpg)


[Cluster Junk](http://twoscoopgames.com/clusterjunk/) was our next project, created for Ludum Dare in December 2015. It was also the first game my partner [Cara](http://howdycara.com/), and friend [Ben Wiley](https://benwiley.org/) worked with us on. We would pitch the game to people saying "You are a piece of trash floating in the ocean, your goal is to collect other trash to become the largest trash island in the sea!" [Cluster Junk](http://twoscoopgames.com/clusterjunk/) was a game I really loved working on and we worked on it until September 2016 hoping to release on Mobile and PC. While the game was humorous and light-hearted, it was about the pollution in our ocean and hopefully would make people think a bit. We threw in names for each piece of garbage collected and we had a lot of fun with that in both innocuous and subtly political ways that people seemed to enjoy. Sadly this game was never completed, and it was what I consider a major turning point for our studio when we had to cancel it; our custom engine posed too many problems related to performance and platform support that we ended up switching to Unity for our newer games.

![](http://twoscoopgames.com/img/clusterjunk-ss-2.jpg)

#### 2016

![](http://twoscoopgames.com/img/morningritual.gif)
January 2016 rolled around and with it another Global Game Jam. This time we worked with [Cara](http://howdycara.com/) and [Chris Kincaid](http://chriskincaid.com/) to create [Morning Ritual ](http://twoscoopgames.com/morningritual/), a really fun project inspired by WarioWare microgames.




![](http://twoscoopgames.com/img/electropolis-ss-2.png)
Later that year we had the awesome opportunity to participate in Train Jam, a game jam event that takes place on a train traveling from Chicago to San Francisco for the Game Developer Conference (GDC). Eric, [Cara](http://howdycara.com/), Rex, and myself traveled together and teamed up with [Lindar](https://cybre.space/@lindar), a musician we met on the train who used a modified Game Boy to create all the awesome music in the game. Together we created [Electropolis ](http://twoscoopgames.com/electropolis/), a game where players have to play a match 3 puzzle to harness mother nature into energy for a city powered by lightning.


![](http://twoscoopgames.com/img/presskit/events/gdc-trainjam-showcase-2016.jpg)

GDC was an amazing experience but it taught us a lot about ourselves as developers, namely that burnout is real and that we had it. After GDC that year we took a much needed break for a few months.


#### 2017

The theme for Global Game Jam in January 2017 was "Waves" which led us to sine waves, which led us to drawing circles, which led us to stacking circles into cones, which led us to [Duolastic ](http://twoscoopgames.com/duolastic/) our first multiplayer game where cones play ping pong on a platform floating through space! Eric came up with a really cool networking method for this game using something called WebRTC, allowing players to connect without a central server.

![](http://twoscoopgames.com/img/duolastic-ss-1.png)

In June 2017 our local game development community [Louisville Makes Games](https://louisvillemakesgames.org/) started it's own game jam called [Kentucky Fried Pixels](https://kentuckyfriedpixels.com/), at this event we started a project we are still actively working on today: A new and improved remake of [Kick Bot](http://twoscoopgames.com/kickbot/) This is our first project in Unity, and we started with a weekend game jam for [Kentucky Fried Pixels](https://kentuckyfriedpixels.com/) with remaking the original game. After that we decided to add more features and improvements and really liked where it was going. We have worked on Kick Bot off and on for 2 years since then, and hope to finish and release it soon!

![](http://twoscoopgames.com/img/presskit/two-scoop-games-maker-faire-2017.png)
Alex and Eric of Two Scoop Games showing Kick Bot at Louisville Maker Faire 2017


![](http://blog.twoscoopgames.com//images/logo.png)
Around August 2017 I asked my partner [Cara](http://howdycara.com/) to help create a new logo and branding for Two Scoop Games, she is a very talented and experienced graphic designer and she did a fantastic job.


Late in 2017 our studio began to take on contract work creating games an apps under the name [Two Scoop Interactive](http://twoscoopinteractive.com). In early 2018 I switched to this contract work through [Two Scoop Interactive](http://twoscoopinteractive.com) as my full time day job and we continue to work on our own games on the side.

#### 2018

For Global Game Jam, January 2018 we took a break from Kick Bot to create [Fineage ](http://twoscoopgames.com/fineage/). The theme was "Transmission" and we based our game on the transmission of genetic information. We worked again on a team of Eric, myself, and my partner [Cara](http://howdycara.com). The game was inspired by a game on the Super Nintendo I loved as a kid called Evo: Search for Eden.

![](http://twoscoopgames.com/img/fineage-ss-3.png)

In 2018 I had the opportunity to speak at [Vector](http://vectorconf.com/) game developer's conference about our studio's experience in doing over 20 game jams, and I encouraged other game creators to do game jams as well. I believe that we have learned and grown the most in our game development skills by prototyping and the creativity and pressure that comes along with game jams (especially in-person events).

![](http://twoscoopgames.com/img/presskit/two-scoop-games-alex-bezuska-vector-2018-talk.png)


We started working on another new game, [PullPals](http://twoscoopgames.com/pullpals) in December 2017 for the Ludum Dare game jam, and it is one of my favorite projects we have worked on so far, it's also our first foray into 3D with the Unity game engine, and the first time I did actual 3D modeling for a game using Blender. [PullPals](http://twoscoopgames.com/pullpals) is a game where you pull, stretch, and fling cute monster babies to solve puzzles. The plan is to continue on PullPals after Kick Bot is complete!

![](http://twoscoopgames.com/img/pullpals-ss-1.png)

Later in 2018 PullPals took home the blue ribbon in the Kentucky State Fair under a brand new category "Interactive Art" (sponsored by a local company called [Design Web](https://designweblouisville.com)).

![](/images/2018/08/pullpals-twoscoopgames-ky-state-fair-blue-ribbon.jpg)

####  2019
January 2019 for Global Game Jam we had planned to work on [Kick Bot](http://twoscoopgames.com/kickbot/) some more, but were too tempted by the theme to not create something. We teamed up with Barry from Roaring Cat Games and created our first ever physical card game, [Moving In ](http://twoscoopgames.com/movingin), which was a really fun challenge in terms of game design. In Moving In you and another player play the role of a couple moving in with each other for the first time and the goal is for both people to be content with the furniture brought into the home - each player has styles and colors they like but they aren't allowed to directly tell the opposite player. This game really makes any two people start to sound like a couple in a long-term relationship, and that can get pretty hillarious!

![](http://twoscoopgames.com/img/movingin-ss-0.png)



#### Where do we go from here?

I have really enjoyed the first 5 years of Two Scoop Games, and I am looking forward to the next 5 even more. We know more about making games than 5 years ago, we know more about business and releasing games than 5 years ago, and we know more about each other and how we can work better as a team than 5 years ago. The plan from here is to continue doing contract work games and apps for other companies and studios as [Two Scoop Interactive](http://twoscoopinteractive.com) while working on our own projects like [Kick Bot](http://twoscoopgames.com/kickbot/) and [PullPals](http://twoscoopgames.com/pullpals).

![](http://twoscoopgames.com/img/presskit/two-scoop-games-presskit-1.png)

I attribute a lot of what we have been able to do so far with being part of the local game dev community ([Louisville Makes Games](https://louisvillemakesgames.org/)), going to meetups, getting to know people as friends and not as tools to get ahead, and being on a team where we have the understanding that we don't know everything and we aren't good at everything, but we can keep trying to improve with each game we make.

We are hopeful that the release of [Kick Bot](http://twoscoopgames.com/kickbot/) will bring us a step closer to our goal of being a studio that can support itself off of its own games. I hope to bring more fun into the world, and to continue following the dream I have had since I was a kid.

- Alex Bezuska,
Two Scoop Games
