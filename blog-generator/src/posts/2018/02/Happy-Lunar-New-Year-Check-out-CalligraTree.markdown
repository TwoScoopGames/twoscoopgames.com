---
dateTime: 2018-02-16T19:54:03.000Z
year: 2018
month: 02
day: 16
time: 14.54.03
title: Happy Lunar New Year! Check out CalligraTree
blurb: CalligraTree
tags:
  - friends
featured: false
coverPhoto: /images/2018/02/tumblr_p49dy3KfRR1td7tmqo1_640.jpg
coverPhotoAlt: Happy LunarNewYear Go check out this great game where you create and share beautiful trees that our friends at RoaringCatGames just released called CalligraTree

---
![](/images/2018/02/tumblr_p49dy3KfRR1td7tmqo1_640.jpg)

### Happy Lunar New Year!
Go check out this great game where you create and share beautiful trees that our friends at [Roaring Cat Games](http://roaringcatgames.com/) just released!


*CalligraTree is a game about guiding a “growing” Sakura tree painting into it’s most full and beautiful form. As the trunk grows, you are able to guid the branches in a general direction, and their branches, and those branches branches, and so forth. Your goal is to see the tree to it’s largest form without crossing any branches.*

[Download CalligraTree for PC on itch.io](https://roaringcatgames.itch.io/calligratree)

[Donload Caligratree for iOS on the App Store](https://itunes.apple.com/us/app/calligratree/id1333362973?uo=8&at=1l3vuur)

[Donload Caligratree for android on the Google Play Store](https://play.google.com/store/apps/details?id=com.roaringcatgames.calligratree)
