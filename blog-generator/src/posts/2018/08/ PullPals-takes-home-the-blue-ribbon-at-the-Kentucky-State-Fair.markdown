---
dateTime: 2018-08-18T17:13:32.000Z
year: 2018
month: 08
day: 18
time: 13.13.32
title: PullPals takes home the blue ribbon at the Kentucky State Fair
tags:
  - events
  - awards
  - pullpals
featured: false
coverPhoto: /images/2018/08/pullpals-twoscoopgames-ky-state-fair-blue-ribbon.jpg
coverPhotoAlt: PullPals Takes home the blue ribbon at the Kentucky State Fair
---


![](/images/2018/08/pullpals-twoscoopgames-ky-state-fair-blue-ribbon.jpg)

So our game PullPals just won the blue ribbon for interactive art at the Kentucky State Fair which is both an honor and hilarious.



We presented PullPals along with other local games and interactive art pieces at the Kentucky State Fair for the first time ever, as in Interactive art and games were not even a category before this year.

![](/images/2018/08/two-scoop-games-ky-state-fair-pullpals.png)

I want to give a shout-out to the other games that recieved ribbons including:

- [Cententable](http://buttonsare.cool/)
- [Kentucky Dash](https://store.steampowered.com/app/730780/Kentucky_Dash/)

And a huge thanks to [Design Web](designweblouisville.com/) which is a local Louisville web development studio that graciously sponsored the Interactive Art category, including the prize money.
