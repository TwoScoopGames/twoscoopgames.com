---
dateTime: 2018-05-26T18:16:10.000Z
year: 2018
month: 05
day: 26
time: 14.16.10
title: Kentucky Route Zero Live stream to benefit Louisville Makes Games
blurb: Featuring KRZ co-creator Jake Elliot, and Alex from Two Scoop Games
tags:
  - livestream
  - fundraiser
featured: false
coverPhoto: /images/2018/05/tumblr_p9cleyCaO31td7tmqo1_640.jpg
coverPhotoAlt: Today 2-8pm EST KY game developers playing  streaming Kentucky Route Zero to benefit non profit louisvillemakesgames with special guest, KRZ co-creator Jake Elliott of cardboardcompy Sat May 26, 2-8pm ESTtwitch.tvambocclusion...gamedev stream livestream indiegames kentucky kentuckyroutezero indiegamestream twitch gamedevky  view on Instagram httpsift.tt2J8vqCU

---

![](/images/2018/05/tumblr_p9cleyCaO31td7tmqo1_640.jpg)

Today 2-8pm EST KY game developers playing & streaming Kentucky Route Zero to benefit non profit Louisville Makes Games with special guest, KRZ co-creator: Jake Elliott of [Cardboard Computer](http://cardboardcomputer.com/)
Sat May 26, 2-8pm EST    
[http://twitch.tv/ambocclusion](http://twitch.tv/ambocclusion)
