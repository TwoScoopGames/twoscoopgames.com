---
dateTime: 2018-03-09T17:25:58.000Z
year: 2018
month: 03
day: 09
time: 12.25.58
title: PullPals at Lexington Comic and Toy Con
tags:
  - events
  - pullpals
featured: false
coverPhoto: /images/2018/03/tumblr_p5c33amMQd1td7tmqo1_640.jpg
coverPhotoAlt: Come play PullPals at lex_con Saturday only in the gaming room Pull, move, and fling cute stretchy monster babies to solve puzzles...LCTC2018 indiegamesgamedev mobilegame comics lexington lexingtonkentucky ipadgames tabletgames androidgames iosgames  view on Instagram httpift.tt2oWYJgs

---
![](/images/2018/03/tumblr_p5c33amMQd1td7tmqo1_640.jpg)

Come play PullPals at [Lexington Comic & Toy Convention](http://lexingtoncomiccon.com/)

Saturday only in the gaming room!

Pull, move, and fling cute stretchy monster babies to solve puzzles!

[More info on PullPals here](http://twoscoopgames.com/pullpals)
