---
dateTime: 2018-03-26T17:22:46.000Z
year: 2018
month: 03
day: 26
time: 13.22.46
title: PullPals at Field Elementary School Comic Con 2018
blurb: We had the kids try out PullPals alongside other games made in Louisville
tags:
  - events
  - pullpals
featured: false
coverPhoto: /images/2018/03/tumblr_p67k9ytlJO1td7tmqo1_640.jpg
coverPhotoAlt: We had the great opportunity to show our game at an elementary school. Young kids give the most pure and honest feedback on games....playtesting gamedev unity  view on Instagram httpsift.tt2pJVR6b

---

![](/images/2018/03/tumblr_p67k9ytlJO1td7tmqo1_640.jpg)

Field Elementary School was a greacious host, they gave us a full classroom for us and some fellow Louisville Makes Games developers to show off our creations. The kids were excited to try out games and I think they really liked the Pulpalls demo we had for them.

Young kids give the most pure and honest feedback on games.


We had a great time and hope to make this a yearly event.
