---
dateTime: 2018-03-03T20:46:48.000Z
year: 2018
month: 03
day: 03
time: 15.46.48
title: Come play PullPals at Louisville Arcade Expo 2018
tags:
  - events
  - pullpals
featured: false
coverPhoto: /images/2018/03/tumblr_p518e0cIA71td7tmqo1_640.jpg
coverPhotoAlt: Come visit us today at Louisville Arcade Expo and play PullPals and a dozen other locally-made video games arcadeRx louisville  WeekendFun  view on Instagram httpift.tt2F9eZVD

---
![](/images/2018/03/tumblr_p518e0cIA71td7tmqo1_640.jpg)

Come visit us today at [Louisville Arcade Expo](http://www.louisvillearcade.com/) and play [PullPals](http://twoscoopgames.com/pullpals) and a dozen other locally-made video games!
