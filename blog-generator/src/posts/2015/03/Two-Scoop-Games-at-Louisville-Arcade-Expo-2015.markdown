---
dateTime: 2015-03-08T19:24:09.000Z
year: 2015
month: 03
day: 08
time: 15.24.09
title: Two Scoop Games at Louisville Arcade Expo 2015
tags:
  - indiegames
  - louisville
  - gamedev
  - gamedevky
featured: false
coverPhoto: /images/2015/03/tumblr_nkwr8973vJ1td7tmqo1_640.jpg
coverPhotoAlt: The gamedevlouky booth at Louisville Arcade Expo 2015 arcaderx gamedev gamedevky indiegames

---

![](/images/2015/03/tumblr_nkwr8973vJ1td7tmqo1_640.jpg)
<p>The @gamedevlouky booth at #Louisville Arcade Expo 2015 @arcaderx #gamedev #gamedevky #indiegames</p>

![](/images/2015/03/tumblr_nkwrp3TsRA1td7tmqo1_640.jpg)
<p>Everyone loves games made here in #Louisville  at the Louisville Arcade Expo 2015 @arcaderx #indiegames #gamedev #gamedevky</p>
