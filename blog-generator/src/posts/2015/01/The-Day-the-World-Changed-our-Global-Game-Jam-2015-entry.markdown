---
dateTime: 2015-01-28T05:18:06.000Z
year: 2015
month: 01
day: 28
time: 00.18.06
title: The Day the World Changed - our Global Game Jam 2015 entry
tags:
  - game jam
  - Global Game Jam
featured: false
coverPhoto: /images/2015/01/tumblr_nivg26C1UG1td7tmqo1_1280.png
coverPhotoAlt: We created a new game for the Global Game Jam 2015!  
blurb: We created a new game for the Global Game Jam 2015!  

---

![](/images/2015/01/tumblr_nivg26C1UG1td7tmqo1_1280.png)

We created a new game for the Global Game Jam 2015!  

Global Game Jam is a world-wide experience where developers, artists, and musicians of all types create games in 48 hours!

This year the theme was “What do we do now?” and our game is called “The Day the World Changed”

_The Day the World Changed is a somber reflection on the troubles a new baby can bring to a family and how it can affect the whole community._

Our team this time was just Two Scoop Games (Alex Bezuska & Eric Lathrop)

We jammed at LVL1 Hackerspace in Louisville Kentucky home of Game Dev Lou, our local indie game development community.

Check out our game here: [http://twoscoopgames.com/thedaytheworldchanged/](http://twoscoopgames.com/thedaytheworldchanged/) and be sure to let is know what you think!