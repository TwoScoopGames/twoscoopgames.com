---
dateTime: 2015-04-13T18:55:27.000Z
year: 2015
month: 04
day: 13
time: 14.55.27
title: New Stanley Squeaks playable demo
tags:
  - demo
  - indie games
  - indie game
  - game dev
  - game design
  - twoscoops
  - louisville
  - kentucky
  - hamster
  - game
  - web
featured: false
coverPhoto: http://twoscoopgames.com/img/stanleysqueaks.png
---

New Stanley Squeaks playable demo!
Check out the new Stanley Squeaks demo: 
<a href="http://twoscoopgames.com/stanleysqueaks/">http://twoscoopgames.com/stanleysqueaks/</a>
