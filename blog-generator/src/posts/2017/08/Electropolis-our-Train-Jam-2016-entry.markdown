---
dateTime: 2016-03-14T17:29:22.000Z
year: 2016
month: 03
day: 14
time: 13.29.22
title: Electropolis - our Train Jam 2016 entry
tags:

  - YouTube
featured: false
coverPhoto: http://twoscoopgames.com/img/electropolis.png
youtube:
    - CSkmM16lr_8
---

[youtube-0]

Power a city by lightning by matching symbols in the clouds. 
Game Jam Created for Train Jam 2016 Theme: Maximum Catpacity 
View the Jam Page: http://localhost:8000/electropolis/ 
Eric Lathrop - Game Design and programming 
Alex Bezuska - Game Design, UI, animation, and programming 
Cara Smith - Game Design and art 
Rex Soriano - Game Design and programming 
Lindar K. Greenwood - Music and sound
