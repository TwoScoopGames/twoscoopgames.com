---
dateTime: 2017-08-16T16:59:35.000Z
year: 2017
month: 08
day: 16
time: 12.59.35
title: The all-new Two Scoop Games Logo
tags:


featured: false
coverPhoto: /images/2017/08/tumblr_ousf7bUWDW1td7tmqo1_1280.jpg
coverPhotoAlt: Here it is The brand new Two Scoop Games logo

---
![](/images/2017/08/tumblr_ousf7bUWDW1td7tmqo1_1280.jpg)

Here it is! The brand new Two Scoop Games logo!  Thanks so much [Cara](http://howdycara.com)!!
