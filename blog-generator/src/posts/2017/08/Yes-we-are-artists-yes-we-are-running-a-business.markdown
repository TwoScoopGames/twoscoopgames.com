---
dateTime: 2017-08-15T17:00:54.000Z
year: 2017
month: 08
day: 15
time: 13.00.54
title: Yes we are artists, yes we are running a business
tags:
  - game development
  - buiness
  - art
featured: false
coverPhoto: /images/2017/08/wraith-games.png
blurb: We recently had the pleasure of hearing from Jay Kidd of Wraith Games based out of Hamilton, Ohio
---


<p>Written by Alex Bezuska, Two Scoop Games</p><p><br/></p><p>We recently had the pleasure of hearing from Jay Kidd of <a href="http://www.wraithgames.com/">Wraith Games</a> based out of Hamilton, Ohio. We setup a remote video call with Jay and displayed it on our projector at <a href="http://louisvillemakesgames.org/warpzone/">Warp Zone Louisville game dev space</a>. I came away with a lot of great information.</p><p>First I would like to say that Jay is an enthusiastic speaker and you can instantly tell he is passionate about both making games and healing others in the community. At Jay’s studio which he founded in high school he handles a lot of jobs including marketing, promotion, business plans all on top of actually making the games. His talk was broken down into two main parts: business and marketing/promotion with questions and thoughtful answers scattered throughout.</p><p>The business portion was a refreshing contrast to what I continually hear about funding your game in that Jay suggests actually creating a traditional business plan and approaching a bank. it seems like crowdfunding through platforms such as kickstarter or Patreon, signing with a publisher at the cost of reduced creative control, or going out of your own pocket are the “indie” routes I hear about most often. Jay recommends coming up with market research, and presenting your case to the bank or credit union based on a 3-year plan.</p><p>In the second half of Jay’s talk he shared his experience with what works when it comes to promotion and marketing. Promotion defined as: showing up and having a booth at conventions and expos complete with banners, branded table cloth, signs and giving away cards, flyers, stickers and other SWAG. The marketing portion dove deep into social media – what to post, who to post to, and most importantly how often. Jay discussed Twitter, Facebook, and Instagram as the main platforms they focus their efforts on.</p><p>I feel like my studio, <a href="http://twoscoopgames.com/">Two Scoop Games</a>, can always benefit from thinking more and putting more focused effort into the business plan and marketing behind the games we are making. I have tended to stray from a lot of planning or even thinking of what we do from a business perspective because as indies we tend to want to cast aside the notion that what we are doing is a business or work, we usually want to ‘keep it fun’. I do believe you can think of your work as art while simultaneously making a living from what you do, and that balance is worth the hard work of achieving. I will leave you with a quote from Scott Benson (Night in the Woods):<br/>“Make art.<br/>Make rent.<br/>Help others do the same.”</p><p><br/></p><p>Alex Bezuska is a game artist and developer for <a href="http://twoscoopgames.com/">Two Scoop Games</a> and Director at <a href="http://louisvillemakesgames.org/">Louisville Makes Games!</a>. Alex is obsessively passionate about making games and building the local game development community. He is also an avid lover of ice cream.</p><p><br/></p><figure class="tmblr-embed tmblr-full" data-provider="youtube" data-orig-width="540" data-orig-height="304" data-url="https%3A%2F%2Fyoutu.be%2FM1d5PDRHJ9g"><iframe width="540" height="304" id="youtube_iframe" src="https://www.youtube.com/embed/M1d5PDRHJ9g?feature=oembed&amp;enablejsapi=1&amp;origin=https://safe.txmblr.com&amp;wmode=opaque" frameborder="0" allowfullscreen=""></iframe></figure>
