---
dateTime: 2017-12-30T01:21:07.000Z
year: 2017
month: 12
day: 29
time: 20.21.07
title: Alex of Two Scoop Games intiviewed on local news
blurb: Promoting the Kentucky Fried Pixels game jam / bundle
tags:
  - press
  - friends
featured: false

youtube:
  - TfG2J3FYyJ0
coverPhoto: /images/2017/12/whas11-interview.png
---

[youtube-0]

[Kentucky Fried Pixels](http://louisvillemakesgames.org/kentucky-fried-pixels/) (KFP) is a game jam and bundle that our local gamedev community [Louisville Makes Games](http://louisvillemakesgames.org/) hosts each year. This year we got some media attention when WHAS11 reporter Derrik Rose reached out about coming to film our KFP launch party event. When Derrick was looking for someone to speak to to find out what the event, the bundle, and the jam were all about I stepped up. Our friends Matt [Independant games developer](https://onlyslightlygames.tumblr.com/about) based out of Lexingtoh, KY and Jay from studio [Wraith Games](https://www.wraithgames.com/) based out of Hamilton Ohio were also interviewed for the news story. I feel like between what Matt, Jay and I said we got the point that the KFP bundle is trying to get across; People make the games you play, and quess what? maybe some people in your own home town are making games too!


[Check out the page on WHAS11's website here](https://www.whas11.com/video/news/kentucky-fried-pixels-game-bundle-created-in-louisville/417-2850202)

Games shown (in order of apperance):

- [Privateers by Monster Tree Games](https://store.steampowered.com/app/903180/Privateers/)
- [Kick Bot by Two Scoop Games](http://twoscoopgames.com/kickbot)
- [SpheraKill](https://www.spacetronaut.co/spherakill)
- [Cententable](http://buttonsare.cool/)
- [Rope-A-Dope](https://dawdawdo.itch.io/ropeadope)
- [Collapsus](https://www.wraithgames.com/collapsus)
- [SyRUSH](http://twoscoopgames.com/syrush)
- [Space Jeff](https://onlyslightlygames.tumblr.com/mygames)
- [GalaxSeed](https://roaringcatgames.itch.io/galaxseed)
