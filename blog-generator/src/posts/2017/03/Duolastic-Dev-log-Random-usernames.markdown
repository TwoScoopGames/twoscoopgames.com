---
dateTime: 2017-03-31T19:35:57.000Z
year: 2017
month: 03
day: 31
time: 15.35.57
title: Duolastic progress - Random usernames
tags:
  - gamedev
  - video games
  - online game
  - browser game
  - indie game
  - indie games
  - game development
  - game
  - gaming
  - random
  - procedural
  - duolasticgame
  - louisville
featured: false
coverPhoto: /images/2017/03/tumblr_inline_onp2fr2lne1sh27ku_540.gif
---

<p>Over the last few nights I have been working on a new feature we have wanted in Duolatic for a while, randomly generated usernames.</p><p>When I say random, I don’t necessarily mean completely random there is a bit of a method to it and a lot of attention has gone into creating the word lists.</p><figure data-orig-width="640" data-orig-height="242" class="tmblr-full"><img src="/images/2017/03/tumblr_inline_onp2fr2lne1sh27ku_540.gif" alt="image" data-orig-width="640" data-orig-height="242"/></figure><p>The idea is each user is given a universally unique id or UUID that is stored in their browser’s local storage and the username is based off of that UUID as its seed. A seedable random number generator is used so that the same name is always the result of suing the same number. This means when a user returns to the game they will always keep their username.</p><figure data-orig-width="640" data-orig-height="245" class="tmblr-full"><img src="/images/2017/03/tumblr_inline_onp22d36vJ1sh27ku_540.gif" alt="image" data-orig-width="640" data-orig-height="245"/></figure><p><br/></p><p>The second part of this is generating location names, which will be the name of the “room” where you can play with another player online. When a player wants to play with a friend they can click a button and receive a link, when two players click the link and enter the game the location name will be displayed and they can come back and use the same location again if they like it.</p><figure data-orig-width="640" data-orig-height="245" class="tmblr-full"><img src="/images/2017/03/tumblr_inline_onp22sc84j1sh27ku_540.gif" alt="image" data-orig-width="640" data-orig-height="245"/></figure><p><br/><br/>For fun I created a small website where you can try out the name generator yourself outside of the game and see what interesting combinations it comes up with. (You can also hit the twitter links and share your findings with others!)<br/><a href="http://duolastic-namegen.twoscoopgames.com">http://duolastic-namegen.twoscoopgames.com</a></p><figure data-orig-width="800" data-orig-height="561" class="tmblr-full"><img src="/images/2017/03/tumblr_inline_onp2334inf1sh27ku_540.png" alt="image" data-orig-width="800" data-orig-height="561"/></figure><p>-Alex, Two Scoop Games<br/><br/>About Duolastic:<br/>Challenge your friends anytime, anywhere!<br/>Duolastic is a multiplayer online game coming soon to your browser!<br/>Follow us on twitter for updates: <a href="http://twitter.com/twoscoopgames">@TwoScoopGames</a>!</p>
