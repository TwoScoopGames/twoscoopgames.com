---
dateTime: 2017-09-29T04:33:54.000Z
year: 2017
month: 09
day: 29
time: 00.33.54
title: Kick Bot is coming to GDEX
tags:


featured: false
coverPhoto: /images/2017/09/tumblr_ox0y0i59tQ1td7tmqo1_1280.jpg
coverPhotoAlt: Excited about GDEX in Columbus Ohio this weekend We will be showing off lots of new updates to Kick Bot DX Come play our he beta for a chance to win a steam controller...gamedev gdex2017 kickbotgame indiegamedev indiedev  view on Instagram httpift.tt2fDmXe8

---

**Warning the game described on this post was an unused prototype or features and visuals have changed so drastically that you would appreciate not using them any longer. This post is only for historical purposes. Please look at the [official Kick Bot Presskit for videos, gifs, and screenshots of the current game](http://kickbotgame.com)**

![](/images/2017/09/tumblr_ox0y0i59tQ1td7tmqo1_1280.jpg)
Excited to announce that Kick Bot is coming to GDEX in Columbus Ohio this weekend! 
We will be showing off lots of new updates to Kick Bot! 

Come play our beta for a chance to win a steam controller!


![](/images/2017/09/tumblr_owzzrimNMt1td7tmqo1_1280.jpg)
Chibi Kick Bot!  Worked on some signage for Kick Bot in preparation for GDEX 2017
