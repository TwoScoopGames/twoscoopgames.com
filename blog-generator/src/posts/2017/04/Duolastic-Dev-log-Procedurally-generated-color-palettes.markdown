---
dateTime: 2017-04-05T03:07:50.000Z
year: 2017
month: 04
day: 04
time: 23.07.50
title: Duolastic progress - Procedurally generated color palettes
tags:
  - gamedev
  - game
  - game development
  - game design
  - indie games
  - indie game
  - duolasticgame
  - duolastic
  - louisville
  - louisville makes games
  - two scoop games
featured: false
coverPhoto: /images/2017/04/tumblr_inline_onx1y46iS61sh27ku_540.gif
---

<p>Tonight we worked on a new feature - adding color palettes that are generated for each player the first time they play the game. Now every time a player plays they will retain their color scheme.</p><p>We generate the colors by picking a random spot on the color wheel, using the player’s id as a seed(just like with the random usernames). From the random spot on the color wheel we use a color scheme where we select the opposite color and the two on either side of it to form something between and complementary and split complementary palette.</p><p>Here are some examples of the colors it has come up with:</p><p><br/></p><figure class="tmblr-full" data-orig-height="364" data-orig-width="640"><img src="/images/2017/04/tumblr_inline_onx1y0uNoD1sh27ku_540.gif" data-orig-height="364" data-orig-width="640"/></figure><figure class="tmblr-full" data-orig-height="364" data-orig-width="640"><img src="/images/2017/04/tumblr_inline_onx1y46iS61sh27ku_540.gif" data-orig-height="364" data-orig-width="640"/></figure><p>-Alex, Two Scoop Games<br/><br/>About Duolastic:<br/>Challenge your friends anytime, anywhere!<br/>Duolastic is a multiplayer online game coming soon to your browser!<br/>Follow us on twitter for updates: <a href="http://t.umblr.com/redirect?z=http%3A%2F%2Ftwitter.com%2Ftwoscoopgames&amp;t=ODcxZDZmMmFmMWY3ZTQxMmEyYTFjZjcxOTFjZDk5OTA3ZmRjMWI5ZCw0RnNXaVptTw%3D%3D&amp;b=t%3Augth9JiyY9YQHdzCbDKn_Q&amp;p=https%3A%2F%2Ftwoscoopgames.tumblr.com%2Fpost%2F159046670261%2Fduolastic-dev-log-random-usernames&amp;m=1">@TwoScoopGames</a>!<br/></p>
