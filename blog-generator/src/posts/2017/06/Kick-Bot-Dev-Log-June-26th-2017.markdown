---
dateTime: 2017-06-26T04:00:00.000Z
year: 2017
month: 06
day: 26
time: 00.00.00
title: Kick Bot progress - June 26th, 2017
blub: Odd issue with the Unity animation state machine
tags:
  - gamedev
  - indiedev
  - kickbot
  - louisvillemakesgames
  - twocoopgames
  - kentuckyfriedpixels
  - concept art
  - sketch
  - drawing
  - Game Art
  - game development
  - game
featured: false
coverPhoto: /images/2017/06/tumblr_inline_osmmu3YTzb1sh27ku_540.png
---


**Warning: the game described on this post is a prototype - features and visuals have changed so drastically that we would appreciate if you don't use them if writing about the game. This post is only for historical purposes. Please look at the [official Kick Bot Presskit for videos, gifs, and screenshots of the current game](http://kickbotgame.com)**

When playing around with new artwork we noticed that the jump animation was only actually playing for one frame, it turned out to be a tricky issue to solve but Eric was able to get it working.  Our animation for the player uses the Unity animator state machine and for some reason, making the jump state the default state fixes it. No idea why</p><figure class="tmblr-full" data-orig-height="584" data-orig-width="1474"><img src="/images/2017/06/tumblr_inline_osmmu3YTzb1sh27ku_540.png" data-orig-height="584" data-orig-width="1474"/></figure><p><i>Unity animator state machine for Kick Bot DX</i></p><p><br/>Eric also worked on some tweaks to the high score screen working like it used to, including setting the “NEW BESTt” to purple, and hiding the level score when game over screen shows.<br/><br/>I worked on some sketches but I was generally dried up creatively on the character designs, I am only really happy with the astronaut character:<br/></p><figure class="tmblr-full" data-orig-height="2048" data-orig-width="2048"><img src="/images/2017/06/tumblr_inline_osmmtww4R81sh27ku_540.png" data-orig-height="2048" data-orig-width="2048"/></figure><p>&mdash;</p><p>Kick Bot DX is being created for the Kentucky Fried Pixels (KFP) game jam which ends July 9th, we will be releasing a pay-what-you-want bundle of games on August 17th!</p><p><a href="http://bit.ly/kfpmail">Sign up here to get an email when the KFP bundle is released</a></p><p>Who is Alex?</p><p>Alex Bezuska is the cofounder/ artist for <a href="http://t.umblr.com/redirect?z=http%3A%2F%2Ftwoscoopgames.com&amp;t=YmVmMjViMmEwMWZjZDAzNjhhMGQ5YjkxNTNlMzJmYTgzODQyOTM4Nix2TDIwcE5BNw%3D%3D">Two Scoop Games</a> and Director at <a href="http://t.umblr.com/redirect?z=http%3A%2F%2Flouisvillemakesgames.org&amp;t=YTgwM2ZmZmM4NTI5MWUyZDdkOWY1YmQ5ZjcwNjkwYmE4ZjJjOGJkYSxXTkxYN0p6Ug%3D%3D&amp;b=t%3Augth9JiyY9YQHdzCbDKn_Q&amp;p=https%3A%2F%2Ftwoscoopgames.tumblr.com%2Fpost%2F150318354866%2Fwhat-you-should-ask-yourself-after-showing-your-game-at&amp;m=1">Louisville Makes Games!</a> Alex is obsessively passionate about making games and building the local game development community. He is also an avid lover of ice cream.</p><p>Keep up to date with all the latest news and updates from Two Scoop Games by <a href="http://eepurl.com/1j1_b">joining our mailing list!</a></p>
