---
dateTime: 2017-06-13T15:55:53.000Z
year: 2017
month: 06
day: 13
time: 11.55.53
title: Kick Bot progress - June 13th, 2017
tags:
  - gamdev
  - video games
  - game
  - game development
  - GameJam
  - kentucky fried pixels
  - kentucky
  - louisville
  - two scoop games
  - kickbot
  - kick bot
  - kick bot dx
  - indie game dev
  - indie games
  - indie developer
featured: false
coverPhoto: /images/2017/06/tumblr_inline_orhtgcdSlG1sh27ku_540.gif
---


**Warning: the game described on this post is a prototype - features and visuals have changed so drastically that we would appreciate if you don't use them if writing about the game. This post is only for historical purposes. Please look at the [official Kick Bot Presskit for videos, gifs, and screenshots of the current game](http://kickbotgame.com)**

<p>For the second year in a row my game studio, <a href="http://twoscoopgames.com">Two Scoop Games</a>, is proudly participating in the Kentucky Fried Pixels (KFP) game jam! For those unfamiliar, a game jam is an event for people who make games or want to make games to stretch their skills and creative muscles to produce a game based on a theme within a given timeframe. KFP fits that description, but has a couple of fun and interesting quirks. It is based out of our game dev home, Louisville, KY and it is actually two jams in one; a 48 hour in-person event to kick the game jam off, followed by 30 days to polish and prepare your project for inclusion in the KFP bundle. Bundles of games have been around for a while, one you might have heard of is the Humble Indie Bundle, the concept is multiple game creators selling their games together with a pay-what-you-want price tag on a digital distribution channel, in our case Itch.io. This is a great opportunity for a team or individual to earn their first dollar for doing something they love — creating games!</p><p>The jam started Friday, June 9 and we already had a plan of what we were going to work on — so we jumped right in after an awesome keynote full of jam tips by <a href="http://kpulv.com/">Kyle Pulver</a>.<br/>Our project for the weekend was rebuilding one of the first games Eric and I created together back in 2013; Kick Bot! Kick Bot is our most successful game from a sheer number of weekly active users standpoint, it fluctuates between 25k and 40k. That number is partially due to being featured on the Chrome Web Store homepage along with our other game, Scurry. There have also been quite a few YouTube videos made of people struggling through the game! So this new version will be released as an update and gift for all the Chromebook users we currently have.</p><figure data-orig-width="518" data-orig-height="521" class="tmblr-full"><img src="/images/2017/06/tumblr_inline_orhtff32i51sh27ku_540.png" alt="image" data-orig-width="518" data-orig-height="521"/></figure><p><i>Kick Bot, a game by Two Scoop Games on display at CodePaLOUsa, Louisville Kentucky June 2017</i></p><p>In Kick Bot, you play as a robot painstakingly engineered for one thing and one thing only – wall jumping! In this brutal endless climber you must test your reflexes and jump from wall to wall avoiding hazards and proving your skill.</p><p>We set out with a goal to rebuild the game in Unity so that cross-platform release will be possible for desktop, Chrome Web Store, iOS, and Android. Throughout the course of the weekend things went surprisingly smoothly, because we had an amazing resource available — the Louisville Makes Games community! Many teams in our community have Unity experience and expertise, and we took full advantage of our close proximity to ask lots of questions and learn as much as we could. The community aspect of game jams has always been just as important as the learning for me, and with a community that is humble, helpful, and hoping you succeed, the positive impact can’t be denied.</p><p>The process of ‘porting’ our game to Unity went very well. Eric focused on the core gameplay and mechanics and translated all of the math to make the game move and feel just like the original. I focused on converting and cleaning up artwork, creating a new logo, and learning the feature-packed Unity particle system.</p><figure data-orig-width="320" data-orig-height="623" class="tmblr-full"><img src="/images/2017/06/tumblr_inline_orhtgcdSlG1sh27ku_540.gif" alt="image" data-orig-width="320" data-orig-height="623"/></figure><p>As with all of our game jams, we end Sunday night at 7pm and do presentations of each team and their game. I was proud to show what we were able to complete, it looked like the original game plus more, so we named it “Kick Bot DX”.</p><figure data-orig-width="516" data-orig-height="443" class="tmblr-full"><img src="/images/2017/06/tumblr_inline_orhth6Id581sh27ku_540.png" alt="image" data-orig-width="516" data-orig-height="443"/></figure><p><i>Eric Lathrop and Alex Bezuska of Two Scoop Games showing their creation Kick Bot DX at the Kentucky Fried Pixels game Jam, Louisville Kentucky June 11th</i></p><p>The plan from here is to spend more time on it leading up to the July 9th end of Kentucky Fried Pixels and get it to a state where it is complete enough to launch on iOS and Android, and sold as part of the KFP bundle on itch.io of course!</p><figure data-orig-width="523" data-orig-height="347" class="tmblr-full"><img src="/images/2017/06/tumblr_inline_orhthxPyVC1sh27ku_540.png" alt="image" data-orig-width="523" data-orig-height="347"/></figure><p><i>Kick Bot DX by Two Scoop Games</i></p><p>The next big hurdle is a marketing plan. We want to give this new DX version to the players and fans on Chrome Web Store as a gift, then point them in the direction of the mobile apps, and start hinting at a sequel with even more features.</p><p>You can <a href="http://twoscoopgames.com/kickbot">play the current version of Kick Bot here</a> and buy the Kentucky Fried Pixels Bundle soon! <a href="https://goo.gl/37LZsO">Sign up for the Kentucky Fried Pixels email list</a> to get updates when the bundle is available for purchase!</p>
