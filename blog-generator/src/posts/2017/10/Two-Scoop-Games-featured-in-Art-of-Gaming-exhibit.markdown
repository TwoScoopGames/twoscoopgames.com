---
dateTime: 2017-10-07T04:53:15.000Z
year: 2017
month: 10
day: 07
time: 00.53.15
title: Two Scoop Games featured in Art of Gaming exhibit
tags:
    - Kick Bot
    - events
    - art

featured: false
coverPhoto: /images/2017/10/tumblr_oxfs8rBUgi1td7tmqo1_1280.jpg
coverPhotoAlt: We were part of a local art gallery doing an event called The Art of Gaming
blurb: We were part of a local art gallery doing an event called The Art of Gaming
---

![](/images/2017/10/tumblr_oxfs8rBUgi1td7tmqo1_1280.jpg)

We were part of a local art gallery doing an event called The Art of Gaming run by the Normany Gallery! The piece by Alex Bezuska is inspired by Kick Bot!   
