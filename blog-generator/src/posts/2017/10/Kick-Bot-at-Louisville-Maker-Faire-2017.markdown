---
dateTime: 2017-10-22T03:04:27.000Z
year: 2017
month: 10
day: 21
time: 23.04.27
title: Kick Bot at Louisville Maker Faire 2017
tags:
    - Kick Bot
    - events
    - Louisville
featured: false
coverPhoto: /images/2017/10/tumblr_oy7f7fORw31td7tmqo1_1280.jpg
coverPhotoAlt: We had a great day showing off Kick Bot DX at Louisville Mini Maker Faire  Photo by justvcreative of riseshinegames ...makerlou makerfaire minimakerfaire fourthstreetlive louisville kentucky gamedev gamedevky louisvillemakesgames kickbotgame twoscoopgames indiedev indiegames  view on Instagram httpift.tt2zs3At2

---

**Warning: the game described on this post is a prototype - features and visuals have changed so drastically that we would appreciate if you don't use them if writing about the game. This post is only for historical purposes. Please look at the [official Kick Bot Presskit for videos, gifs, and screenshots of the current game](http://kickbotgame.com)**

![](/images/2017/10/tumblr_oy7f7fORw31td7tmqo1_1280.jpg)
We had a great day showing off Kick Bot DX at Louisville Mini Maker Faire! (Photo by our friend [Veronica Rivera](http://justvcreative.com/))  


![](/images/2017/10/tumblr_oy7f9qTVph1td7tmqo1_1280.jpg)
Lots of kids took home Kick Bot DX papercraft at Louisville Maker Faire today!  


![](/images/2017/10/tumblr_oy7fkqe9NW1td7tmqo1_1280.jpg)
The Two Scoop team, Alex and Eric! We make games for you! 😝  
