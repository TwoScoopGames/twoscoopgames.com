---
dateTime: 2017-10-04T13:22:44.000Z
year: 2017
month: 10
day: 04
time: 09.22.44
title: Our studio pen
tags:
  - Instagram
  - IFTTT
featured: false
coverPhoto: /images/2017/10/tumblr_oxavtwy52R1td7tmqo1_1280.jpg
coverPhotoAlt: Can you believe this pen we found at artistcraftsman_lou   view on Instagram httpift.tt2yoCpma
imported-from: Tumblr
import-date: 2019-07-02T01:27:32.046Z
tumblr-id: 166040236706
tumblr-url: https://twoscoopgames.tumblr.com/post/166040236706/can-you-believe-this-pen-we-found-at
tumblr-type: photo
---

<p>Can you believe this pen we found at @artistcraftsman_lou ? — view on Instagram <a href="http://ift.tt/2yoCpma">http://ift.tt/2yoCpma</a></p>
