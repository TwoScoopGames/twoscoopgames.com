---
dateTime: 2017-10-22T03:04:27.000Z
year: 2017
month: 10
day: 21
time: 23.04.27
title: Kick Bot at Louisville Maker Faire 2017
tags:
  - Instagram
  - IFTTT
featured: false
coverPhoto: /images/2017/10/tumblr_oy7f7fORw31td7tmqo1_1280.jpg
coverPhotoAlt: We had a great day showing off Kick Bot DX at Louisville Mini Maker Faire  Photo by justvcreative of riseshinegames ...makerlou makerfaire minimakerfaire fourthstreetlive louisville kentucky gamedev gamedevky louisvillemakesgames kickbotgame twoscoopgames indiedev indiegames  view on Instagram httpift.tt2zs3At2
imported-from: Tumblr
import-date: 2019-07-02T01:27:32.046Z
tumblr-id: 166659124276
tumblr-url: https://twoscoopgames.tumblr.com/post/166659124276/we-had-a-great-day-showing-off-kick-bot-dx-at
tumblr-type: photo
---


Photo by @justvcreative

<p>We had a great day showing off Kick Bot DX at Louisville Mini Maker Faire!  Photo by @justvcreative<br/>
.<br/>
.#makerlou #makerfaire #minimakerfaire #fourthstreetlive #louisville #kentucky #gamedev #gamedevky #louisvillemakesgames #kickbotgame #twoscoopgames #indiedev #indiegames — view on Instagram <a href="http://ift.tt/2zs3At2">http://ift.tt/2zs3At2</a></p>
