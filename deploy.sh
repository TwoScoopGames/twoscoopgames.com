#!/bin/bash

# Load environment variables from .env file
export $(grep -v '^#' .env | xargs)

# Build the project
echo "Building the project..."
npm run build

# Check if build was successful
if [ $? -eq 0 ]; then
  echo "Build successful."

  # Additional task: Copy directories from playable-games/ to out/ and rename them
  for dir in out/*/; do
    dir_name=$(basename "$dir")
    if [ -d "playable-games/$dir_name" ]; then
      echo "Found matching directory: $dir_name"
      cp -r "playable-games/$dir_name" "$dir/game"
      echo "Copied and renamed directory: $dir_name to $dir/game"
      
      # Fix permissions locally
      echo "Fixing permissions for $dir/game"
      chmod -R 755 "$dir/game"
      chown -R $(whoami):$(whoami) "$dir/game"
    fi
  done

  echo "Deploying to $USERNAME@$IP:$DIRECTORY"

  # Deploy the website using rsync to sync files to the remote server
  rsync -avz out/ $USERNAME@$IP:$DIRECTORY

  # Check if deployment was successful
  if [ $? -eq 0 ]; then
    echo "Deployment successful!"
  else
    echo "Deployment failed."
    exit 1
  fi
else
  echo "Build failed. Aborting deployment."
  exit 1
fi
