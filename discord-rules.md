---========================================---
1)  Don't Be a Jerk. This means different things to different individuals and cultures. It doesn't matter if something isn't offensive to you if it's offensive to someone else. Be kind, remember that others see the world differently from you, and assume you're in the wrong if someone calls you out. If you need more clarity on this, read our Code of Conduct (http://twoscoopgames.com/conduct/)
---
2) Use @ mentions sparingly. Only @ a moderator when something has happened that needs fixing ASAP. Only @ a developer if there is a true emergency (e.g. our websites are down or we just published a horribly broken game update). Moderators are here to moderate, not to provide game help.
---
3) Use the appropriate channel. We have a lot of channels to help keep things organized. Choose the most appropriate one for your conversations. Don't cross-post.
---
4) This isn't the place for bug reports. Submit bug reports at http://twoscoopgames.com/support/
---
5) Don't tell the devs your game ideas/suggestions/improvements. We have more ideas than we can handle and are already working on them more than full time.
---
6) Be careful with profanity. We don't have anything against it, but in practice it's easy to break Rule 1 when using profanity. Don't use words that target groups of people, and don't point your profanity guns at other people. Hover around PG-13.
---
7) Don't try to ride the line. Even if you don't break the rules, if it seems like you want to we'll assume you're a problem waiting to happen.
---
8) Getting called out. If someone calls you out for breaking the rules, your gut reaction will be to defend yourself. Don't! Instead, thank them for pointing it out and then take a little time to quietly reflect.
---
9) No advertising. This includes trying to sell your own games, recruiting people into other Discord servers, and trying to get people to view your Twitch stream. If you truly believe your proposal adds value to the community here, tell us what you want to promote and why, and how it relates to Two Scoop Games and makes this community better.
---
10) No spam. This includes rapidly filling a channel with back-to-back messages, or nearly all memes.
---
11) No bummers. We all have bad moments and bad days. You'll make your day better (and everyone else's!) by focusing on the good and engaging in positive social interactions. That bums everyone else out.
---
12) Let people like what they like. We all have opinions about which music, phones, and games are best. No matter how strongly you hold your beliefs, don't hate on the preferences of others.
---========================================---
Help Keep The Community Clean!

1) If you see someone break a rule, politely point it out to them. It's as easy as, "Hey, that (probably) breaks Rule 1." If they argue, don't argue back. Just point them to Rule 8.
---
2) If something horrible is happening that you can't stop, or don't feel comfortable trying to stop, mention one of the online mods (you can see them in the sidebar).
---========================================---
Consequences

1) Mods and devs are allowed to take any action they deem appropriate, with or without warning, and at any time. Blatant disregard for our rules will lead to an immediate ban.
2) Consequences may extend beyond Discord into Two Scoop Games games.
