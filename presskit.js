var fs = require("fs");
var handlebars = require("handlebars");

var presskitData = require("./presskit.json");
var presskitTemplate = "./presskit.hbs";
var presskitDestination = "./out/press/index.html";

createPresskit(presskitData, presskitTemplate, presskitDestination);

function createPresskit(data, template, destination) {
  fs.writeFileSync(destination, renderFromExternalTemplate(template, data));
}

function renderFromExternalTemplate(templateFile, data){
  var file = fs.readFileSync(templateFile, "utf8");
  var template = handlebars.compile(file);
  return template(data);
}
