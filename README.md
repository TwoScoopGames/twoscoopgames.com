
# twoscoopgames.com

## Building

This project uses the [unfold static site generator](https://github.com/ericlathrop/unfold).

### To build:
1. Install [node.js](http://nodejs.org/)
2. Run `npm install`
3. Run `npm run build-and-serve`

## Deployment

### Setting up `.env` for Deployment

We use a `.env` file to manage environment variables for deployment. This allows us to easily update configuration like the server IP, username, and deployment directory without modifying the deployment script directly.

### Setup:

1. **Create the `.env` file**  
In the root of the project, create a `.env` file with the following content:

```bash
IP=your_server_ip
USERNAME=your_ssh_username
DIRECTORY=/your/remote/directory/
```

Example:

```bash
IP=your_server_ip
USERNAME=your_username
DIRECTORY=/path/to/your/remote/directory/
```

2. **Install `dotenv-cli`**  
Run the following command to install `dotenv-cli` to automatically load environment variables from your `.env` file:

```bash
npm install dotenv-cli --save-dev
```

3. **Deployment Script**  
We have a deploy script located at `deploy.sh` that uses the environment variables to deploy the website to the specified server. Here's the basic structure of the script:

```bash
#!/bin/bash

# Load environment variables from .env file
export $(grep -v '^#' .env | xargs)

# Build the project
echo "Building the project..."
npm run build

if [ $? -eq 0 ]; then
  echo "Build successful. Deploying to $USERNAME@$IP:$DIRECTORY"

  rsync -avz --delete out/ $USERNAME@$IP:$DIRECTORY

  if [ $? -eq 0 ]; then
    echo "Deployment successful!"
  else
    echo "Deployment failed."
    exit 1
  fi
else
  echo "Build failed. Aborting deployment."
  exit 1
fi
```

4. **Run Deployment**  
To deploy your project, run the following npm command:

```bash
npm run deploy
```

This will:
- Load the environment variables from `.env`
- Build the project
- Sync the built files from the `out/` directory to the remote server using `rsync`

5. **Ensure Correct Permissions**  
Make sure your `deploy.sh` script is executable. You can set the correct permissions using the following command:

```bash
chmod +x deploy.sh
```

This allows for quick and easy deployment without hardcoding sensitive details like server IP and usernames directly into your scripts.
