require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");

var Splat = require("splatjs");

var animations = require("./animations");
var input = new Splat.Input(require("./inputs"));

var images = new Splat.ImageLoader();
images.loadFromManifest(require("./images"));

var sounds = new Splat.SoundLoader();
sounds.loadFromManifest(require("./sounds"));

var scenes = {};
var systems = require("./systems");

function installSystems(systems, ecs, data) {
	systems.forEach(function(system) {
		if (system.indexOf("splatjs:") === 0) {
			var names = system.substr(8).split(".");

			var func = names.reduce(function(obj, name) {
				return obj[name];
			}, Splat.systems);
			func(ecs, data);
		} else {
			require(system)(ecs, data);
		}
	});
}

var main = makeScene("main");
main.entities.load(require("./entities"));

function makeScene(name) {
	var scene = new Splat.Scene();
	var data = {
		animations: animations,
		canvas: canvas,
		context: context,
		entities: scene.entities,
		images: images,
		input: input,
		scenes: scenes,
		sounds: sounds
	};
	scenes[name] = scene;
	installSystems(systems.simulation, scene.simulation, data);
	installSystems(systems.renderer, scene.renderer, data);
	return scene;
}

function fullScreenImage(scene, name) {
	var img = scene.entities.add();
	img.position = {
		x: 0,
		y: 0
	};
	img.image = {
		name: name,
		sourceX: 0,
		sourceY: 0,
		sourceWidth: 1136,
		sourceHeight: 640,
		destinationX: 0,
		destinationY: 0,
		destinationWidth: 1136,
		destinationHeight: 640
	};
	return img;
}

function arrows(scene) {
	var img = scene.entities.add();
	img.position = {
		x: 929,
		y: 498
	};
	img.image = {
		name: name,
		sourceX: 0,
		sourceY: 0,
		sourceWidth: 186,
		sourceHeight: 126,
		destinationX: 0,
		destinationY: 0,
		destinationWidth: 186,
		destinationHeight: 126
	};
	img.animation = {
		"time": 0,
		"frame": 0,
		"loop": true,
		"speed": 0.3,
		"name": "arrows-right-f2"
	};
	return img;
}

var title = makeScene("title");
title.renderer.add(function(entities, context) { // jshint ignore:line
	if (input.button("left") || input.button("right")) {
		scenes.title.stop();
		sounds.play("crickets", true);
		scenes.intro1.start(context);
	}
});
fullScreenImage(title, "titlescreen");
arrows(title);

var intro1 = makeScene("intro1");
fullScreenImage(intro1, "intro-1");
arrows(intro1);

var words = intro1.entities.add();
words.position = {
	x: 500,
	y: 200
};
words.timers = {
	showText: {
		running: true,
		time: 0,
		max: 1000,
		script: "./lib/add-text-1"
	}
};
words.seq = 0;

intro1.renderer.add(function(entities, context) { // jshint ignore:line
	var isPressed  = input.button("left") || input.button("right");
	if (words.lastPressed === false && isPressed && words.text !== undefined) {
		words.text.a = 0;
		if (words.seq === 0) {
			words.text.text = "I'm pregnant.";
		} else if (words.seq === 1) {
			words.text.text = "What do we do now?";
			words.position.x = 200;
			words.position.y = 410;
		} else if (words.seq === 2) {
			scenes.intro1.stop();
			sounds.stop("crickets");
			sounds.play("hospital-sounds", true);
			scenes.sixMonths.start(context);
		}
		words.seq++;
	}
	words.lastPressed = isPressed;
});

var sixMonths = makeScene("sixMonths");
fullScreenImage(sixMonths, "black-screen");
arrows(sixMonths);
var words2 = sixMonths.entities.add();
words2.position = {
	x: 300,
	y: 300
};
words2.timers = {
	showText: {
		running: true,
		time: 0,
		max: 1000,
		script: "./lib/add-text-2"
	}
};
sixMonths.renderer.add(function(entities, context) { // jshint ignore:line
	var isPressed  = input.button("left") || input.button("right");
	if (words2.lastPressed === false && isPressed && words2.text !== undefined) {
		scenes.sixMonths.stop();
		sounds.play("shark-birth", true);
		scenes.intro2.start(context);
	}
	words2.lastPressed = isPressed;
});


var intro2 = makeScene("intro2");
fullScreenImage(intro2, "intro-2");
arrows(intro2);

var legs = intro2.entities.add();
legs.position = {
	x: 98,
	y: 214
};
legs.image = {
	name: "legs",
	sourceX: 0,
	sourceY: 0,
	sourceWidth: 940,
	sourceHeight: 426,
	destinationX: 0,
	destinationY: 0,
	destinationWidth: 940,
	destinationHeight: 426
};

var doctor = intro2.entities.add();
doctor.position = {
	x: 357,
	y: 0
};
doctor.velocity = {
	x: 0,
	y: 0.05
};
doctor.image = {
	name: "doctor-intro1",
	sourceX: 0,
	sourceY: 0,
	sourceWidth: 446,
	sourceHeight: 640,
	destinationX: 0,
	destinationY: 0,
	destinationWidth: 446,
	destinationHeight: 640
};
doctor.timers = {
	doctor: {
		running: true,
		time: 0,
		max: 3000,
		script: "./lib/doctor-freak-out"
	},
	stop: {
		running: true,
		time: 0,
		max: 6000,
		script: "./lib/doctor-stop"
	}
};

intro2.renderer.add(function(entities, context) { // jshint ignore:line
	var isPressed  = input.button("left") || input.button("right");
	if (intro2.lastPressed === false && isPressed) {
		scenes.intro2.stop();
		sounds.stop("shark-birth");
		sounds.stop("hospital-sounds");
		sounds.play("go");
		scenes.go.start(context);
	}
	intro2.lastPressed = isPressed;
});

var go = makeScene("go");
fullScreenImage(go, "black-screen");
arrows(go);

var goAnim = go.entities.add();
goAnim.position = {
	x: 338,
	y: 208
};
goAnim.image = {
	sourceX: 0,
	sourceY: 0,
	sourceWidth: 382,
	sourceHeight: 204,
	destinationX: 0,
	destinationY: 0,
	destinationWidth: 382,
	destinationHeight: 204
};
goAnim.animation = {
	"time": 0,
	"frame": 0,
	"loop": true,
	"speed": 0.3,
	"name": "go"
};

go.renderer.add(function(entities, context) { // jshint ignore:line
	var isPressed  = input.button("left") || input.button("right");
	if (go.lastPressed === false && isPressed) {
		scenes.go.stop();
		sounds.play("Wake_-_67_-_Duckbag", true);
		scenes.main.start(context);
	}
	go.lastPressed = isPressed;
});

var end = makeScene("end");
fullScreenImage(end, "endscreen");

function percentLoaded() {
	return (images.loadedImages + sounds.loadedSounds) / (images.totalImages + sounds.totalSounds);
}
var loading = Splat.loadingScene(canvas, percentLoaded, title);
loading.start(context);

},{"./animations":2,"./entities":3,"./images":4,"./inputs":5,"./sounds":60,"./systems":61,"splatjs":32}],"./lib/add-collisions":[function(require,module,exports){
"use strict";

module.exports = function addCollisions(entity) {
	entity.collisions = [];
};

},{}],"./lib/add-text-1":[function(require,module,exports){
"use strict";

module.exports = function(entity) {
	entity.text = {
		r: 255,
		g: 255,
		b: 255,
		a: 0,
		font: "30px sans-serif",
		text: "Honey, I have to tell you something..."
	};
};

},{}],"./lib/add-text-2":[function(require,module,exports){
"use strict";

module.exports = function(entity) {
	entity.text = {
		r: 255,
		g: 255,
		b: 255,
		a: 0,
		font: "40px sans-serif",
		text: "6 months later..."
	};
};

},{}],"./lib/advance-timers":[function(require,module,exports){
"use strict";

module.exports = function(ecs) {
	ecs.addEach(function(entity, elapsed) {
		var names = Object.keys(entity.timers);

		names.forEach(function(name) {
			var timer = entity.timers[name];
			if (!timer.running) {
				return;
			}

			timer.time += elapsed;

			if (timer.time > timer.max) {
				timer.running = false;
				timer.time = 0;

				if (timer.script !== undefined) {
					var script = require(timer.script);
					script(entity);
				}
			}
		});
	}, ["timers"]);
};

},{}],"./lib/apply-angular-velocity":[function(require,module,exports){
"use strict";

module.exports = function(ecs, data) { // jshint ignore:line
	ecs.addEach(function(entity, elapsed) {
		entity.rotation.angle += entity.rotation.velocity * elapsed;
		if (entity.rotation.friction !== undefined) {
			entity.rotation.velocity *= entity.rotation.friction;
		}
	}, ["rotation"]);
};

},{}],"./lib/control-player":[function(require,module,exports){
"use strict";

var addSegment = require("./add-segment");
var copyEntityComponents = require("./copy-entity-components");

var createdRooms = false;

var prefabs = require("../prefabs");

function createPerson(entities, x, y) {
	var names = [ "person-a", "person-b", "person-c", "person-d" ];
	var index = Math.floor(Math.random() * names.length);

	var person = copyEntityComponents(prefabs[names[index]], entities.add());
	person.position.x = x;
	person.position.y = y;
}

var roomWidth = 20;
var roomHeight = 12;

var wallWidth = 74;
var wallHeight = 71;

function createRoom(entities, rx, ry) {
	var doorWidth = 4;
	var doorStart = (roomWidth - doorWidth) / 2;

	for (var x = 0; x < roomWidth; x++) {
		createWall(entities, rx + x * wallWidth, ry + 0 * wallHeight);
		if (x < doorStart || x > doorStart + doorWidth) {
			createWall(entities, rx + x * wallWidth, ry + (roomHeight - 1) * wallHeight);
		}
	}

	for (var y = 1; y < roomHeight - 1; y++) {
		createWall(entities, rx + 0 * wallWidth, ry + y * wallHeight);
		createWall(entities, rx + (roomWidth - 1) * wallWidth, ry + y * wallHeight);
	}

	for (var i = 0; i < 8; i++) {
		var px = Math.floor(Math.random() * (roomWidth - 2)) + 1;
		var py = Math.floor(Math.random() * (roomHeight - 2)) + 1;
		createPerson(entities, rx + (px * wallWidth), ry + (py * wallHeight));
	}

	[ "bed", "breathing machine", "ekg", "iv", "potted plant" ].forEach(function(prefab) {
		var prop = copyEntityComponents(prefabs[prefab], entities.add());
		var x = Math.floor(Math.random() * (roomWidth - 2)) + 1;
		var y = Math.floor(Math.random() * (roomHeight - 2)) + 1;
		prop.position.x = rx + x * wallWidth;
		prop.position.y = ry + y * wallWidth;
	});
}

function createWall(entities, x, y) {
	var prop = copyEntityComponents(prefabs.wall, entities.add());
	prop.position.x = x;
	prop.position.y = y;
}

module.exports = function(ecs, data) {
	ecs.addEach(function(entity, elapsed) { // jshint ignore:line
		var forwardSpeed = 0.5;
		var angle = entity.rotation.angle + (Math.PI / 2);
		entity.velocity.x = forwardSpeed * Math.cos(angle);
		entity.velocity.y = forwardSpeed * Math.sin(angle);

		if (!createdRooms) {
			for (var y = 0; y < 1; y++) {
				for (var x = -1; x < 2; x++) {
					var rx = x * roomWidth * wallWidth;
					var ry = y * roomHeight * wallHeight;
					console.log(rx, ry);
					createRoom(data.entities, rx, ry);
				}
			}
			// createRoom(data.entities, 0, 0);
			createdRooms = true;
		}

		var angularSpeed = 0.005;
		entity.rotation.velocity = 0.0;
		if (data.input.button(entity.playerController2d.left)) {
			entity.rotation.velocity = -angularSpeed;
		}
		if (data.input.button(entity.playerController2d.right)) {
			entity.rotation.velocity = angularSpeed;
		}

		if (data.input.button(entity.playerController2d.down)) {
			addSegment(data.entities, entity);
		}
	}, ["playerController2d", "rotation", "velocity"]);
};

},{"../prefabs":59,"./add-segment":6,"./copy-entity-components":7}],"./lib/delete-entities":[function(require,module,exports){
"use strict";

module.exports = function(ecs, data) {
	ecs.add(function(entities, elapsed) { // jshint ignore:line
		var keys = Object.keys(entities);
		for (var i = 0; i < keys.length; i++) {
			var entity = entities[keys[i]];
			if (entity.dead) {
				delete data.entities.entities[entity.id];
			}
		}
	});
};

},{}],"./lib/delete-old-entities":[function(require,module,exports){
"use strict";

module.exports = function(entity) {
	entity.dead = true;
};

},{}],"./lib/delete-shake":[function(require,module,exports){
"use strict";

module.exports = function(entity) {
	delete entity.shake;
};

},{}],"./lib/doctor-freak-out":[function(require,module,exports){
"use strict";

module.exports = function(entity) {
	entity.velocity.y = -0.05;
	entity.animation = {
		"time": 0,
		"frame": 0,
		"loop": true,
		"speed": 3,
		"name": "doctor-intro2-f2"
	};
	entity.position.x -= 26;
	entity.image.destinationWidth = 645;
};

},{}],"./lib/doctor-stop":[function(require,module,exports){
"use strict";

module.exports = function(entity) {
	delete entity.velocity;
};

},{}],"./lib/draw-rotated-image":[function(require,module,exports){
"use strict";

function drawEntity(context, images, entity) {
	if (entity.rotation !== undefined) {
		context.save();

		var x = entity.position.x + entity.rotation.x;
		var y = entity.position.y + entity.rotation.y;
		context.translate(x, y);
		context.rotate(entity.rotation.angle);
		context.translate(-x, -y);
	}

	var image = images.get(entity.image.name);
	if (!image) {
		return;
	}

	context.drawImage(
		image,
		entity.image.sourceX,
		entity.image.sourceY,
		entity.image.sourceWidth,
		entity.image.sourceHeight,
		entity.image.destinationX + entity.position.x,
		entity.image.destinationY + entity.position.y,
		entity.image.destinationWidth,
		entity.image.destinationHeight
	);

	if (entity.rotation !== undefined) {
		context.restore();
	}
}

module.exports = function(ecs, data) {
	ecs.add(function(entities, context) {
		var toDraw = Object.keys(entities).reduce(function(list, id) {
			var entity = entities[id];
			if (entity.position !== undefined && entity.image !== undefined) { // requirements
				list.push(entity);
			}
			return list;
		}, []);

		toDraw.sort(function(a, b) {
			return a.position.y - b.position.y;
		});

		toDraw.forEach(drawEntity.bind(undefined, context, data.images));
	});
};

},{}],"./lib/draw-text":[function(require,module,exports){
"use strict";

module.exports = function(ecs, data) { // jshint ignore:line
	ecs.addEach(function(entity, context) { // jshint ignore:line
		context.fillStyle = "rgba(" + entity.text.r + ", " + entity.text.g + ", " + entity.text.b + ", " + entity.text.a + ")";
		context.font = entity.text.font;
		context.fillText(entity.text.text, entity.position.x, entity.position.y);
	}, [ "text", "position" ]);
};

},{}],"./lib/eat-food":[function(require,module,exports){
"use strict";

var addSegment = require("./add-segment");
var getCamera = require("./get-camera");

function angleToVectors(angle, magnitude) {
	var x = magnitude * Math.cos(angle);
	var y = magnitude * Math.sin(angle);
	return { x: x, y: y };
}

function addGib(entities, entity, images) {
	var gibs = entity.gibs;
	var name = gibs[Math.floor(Math.random() * gibs.length)];
	var image = images.get(name);

	var gib = entities.add();
	gib.gibs = [ "people-gib1", "people-gib2", "people-gib3", "people-gib4", "people-gib5", "people-gib6" ];
	gib.position = {
		x: entity.position.x,
		y: entity.position.y
	};
	gib.size = {
		width: image.width,
		height: image.height
	};
	gib.velocity = angleToVectors(Math.random() * Math.PI * 2, 1);
	gib.friction = {
		x: 0.97,
		y: 0.97
	};
	if (entity.regib !== undefined) {
		gib.timers = {
			regib: {
				running: true,
				time: 0,
				max: 1000,
				script: "./lib/add-collisions"
			}
		};
	} else {
		gib.timers = {
			fadeAway: {
				running: true,
				time: 0,
				max: 300 + Math.random() * 700,
				script: "./lib/delete-old-entities"
			}
		};
	}

	// var rotateSpeed = 0.01;
	// gib.rotation = {
	// 	x: image.width / 2,
	// 	y: image.height / 2,
	// 	angle: 0,
	// 	velocity: (Math.random() * rotateSpeed) - (rotateSpeed / 2),
	// 	friction: 0.99
	// };

	// gib.collisions = [];

	gib.image = {
		"sourceX": 0,
		"sourceY": 0,
		"sourceWidth": 0,
		"sourceHeight": 0,
		"destinationX": 0,
		"destinationY": 0,
		"destinationWidth": image.width,
		"destinationHeight": image.height
	};
	gib.animation = {
		"time": 0,
		"frame": 0,
		"loop": true,
		"speed": 1,
		"name": name
	};
}

function addGibs(entities, entity, images) {
	var n = 4 + (Math.random() * 8);
	for (var i = 0; i < n; i++) {
		addGib(entities, entity, images);
	}
}

function hasPeople(entities) {
	var keys = Object.keys(entities);
	for (var i = 0; i < keys.length; i++) {
		var entity = entities[keys[i]];
		if (entity.dead !== true && entity.animation !== undefined && entity.animation.name.indexOf("person") === 0) {
			return true;
		}
	}
	return false;
}

function isConnectedToShark(entities, shark, food) {
	while (food.follow !== undefined) {
		food = entities[food.follow.id];
		if (food === undefined) {
			return false;
		}
	}
	return food.id === shark.id;
}

module.exports = function(ecs, data) { // jshint ignore:line
	ecs.addEach(function(entity, elapsed) { // jshint ignore:line
		entity.collisions.forEach(function(collisionId) {
			var food = data.entities.entities[collisionId];
			food.dead = true;

			var sounds = [ "wall-eat1", "wall-eat2", "wall-eat3", "wall-eat4" ];
			if (food.sounds !== undefined) {
				sounds = food.sounds;
			}

			var camera = getCamera(data.entities.entities);
			var shake = 10;
			if (food.animation.name.indexOf("person") === 0) {
				shake = 20;
			}

			camera.shake = {
				x: shake,
				y: shake
			};
			camera.timers = {
				shake: {
					running: true,
					time: 0,
					max: 100,
					script: "./lib/delete-shake"
				}
			};

			if (isConnectedToShark(data.entities.entities, entity, food) && (entity.timers === undefined || entity.timers.hurt.running === false)) {
				data.sounds.play("shark-hurt");
				entity.timers = {
					hurt: {
						running: true,
						time: 0,
						max: 2000
					}
				};
			} else {
				data.sounds.play(sounds[Math.floor(Math.random() * sounds.length)]);
			}

			if (food.animation.name.indexOf("person") === 0) {
				addSegment(data.entities, entity);
			}
			addGibs(data.entities, food, data.images);

			if (!hasPeople(data.entities.entities)) {
				data.scenes.main.stop();
				data.scenes.end.start(data.context);
			}
		});
	}, ["playerController2d", "collisions"]);
};

},{"./add-segment":6,"./get-camera":8}],"./lib/fade-in-text":[function(require,module,exports){
"use strict";

module.exports = function(ecs, data) { // jshint ignore:line
	ecs.addEach(function(entity, elapsed) {
		var a = entity.text.a;
		a += 0.0007 * elapsed;
		entity.text.a = Math.min(a, 1.0);
	}, [ "text" ]);
};

},{}],"./lib/follow-parent":[function(require,module,exports){
"use strict";

function distanceSquared(x1, y1, x2, y2) {
	return ((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2));
}

module.exports = function(ecs, data) {
	ecs.addEach(function(entity, elapsed) { // jshint ignore:line
		var x1 = entity.position.x + (entity.size.width / 2);
		var y1 = entity.position.y + (entity.size.height / 2);

		var parent = data.entities.entities[entity.follow.id];
		if (parent === undefined) {
			return;
		}
		var x2 = parent.position.x + (parent.size.width / 2);
		var y2 = parent.position.y + (parent.size.height / 2);

		var angle = Math.atan2(y2 - y1, x2 - x1);
		if (entity.rotation !== undefined) {
			entity.rotation.angle = angle - (Math.PI / 2);
		}

		var distSquared = distanceSquared(x1, y1, x2, y2);
		if (distSquared < entity.follow.distance * entity.follow.distance) {
			return;
		}

		var toMove = Math.sqrt(distSquared) - entity.follow.distance;

		entity.position.x += toMove * Math.cos(angle);
		entity.position.y += toMove * Math.sin(angle);
	}, ["position", "follow"]);
};

},{}],"./lib/shake-screen":[function(require,module,exports){
"use strict";

module.exports = function(ecs, data) { // jshint ignore:line
	ecs.addEach(function(entity, elapsed) { // jshint ignore:line
		var shakeX = Math.floor(Math.random() * entity.shake.x * 2) - entity.shake.x;
		var shakeY = Math.floor(Math.random() * entity.shake.y * 2) - entity.shake.y;
		entity.position.x += shakeX;
		entity.position.y += shakeY;
	}, [ "shake" ]);
};

},{}],"./lib/tile-background":[function(require,module,exports){
"use strict";

var getCamera = require("./get-camera");

function getScreenTopLeft(camera) {
	if (camera === undefined) {
		return { x: 0, y: 0 };
	}
	var x = camera.position.x + camera.camera.x;
	var y = camera.position.y + camera.camera.y;
	return { x: x, y: y };
}

module.exports = function(ecs, data) { // jshint ignore:line
	ecs.add(function(entities, context) { // jshint ignore:line
		var f1 = data.images.get("floor-1");
		var f2 = data.images.get("floor-2");

		var screen = getScreenTopLeft(getCamera(data.entities.entities));
		var startX = Math.floor(screen.x / f1.width) * f1.width;
		var startY = Math.floor(screen.y / f1.height) * f1.height;

		for (var y = startY; y <= screen.y + data.canvas.height; y += f1.height) {
			for (var x = startX; x <= screen.x + data.canvas.width; x += f1.width) {
				var tile = (Math.floor(x / f1.width) + Math.floor(y / f1.height)) % 2;
				context.drawImage(tile === 0 ? f1 : f2, x, y, f1.width, f1.height);
			}
		}
	});
};

},{"./get-camera":8}],2:[function(require,module,exports){
module.exports={
 "floor-1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "floor-1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 170,
     "sourceHeight": 170
    }
   }
  }
 ],
 "floor-2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "floor-2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 170,
     "sourceHeight": 170
    }
   }
  }
 ],
 "iv": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "iv",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 129,
     "sourceHeight": 167
    }
   }
  }
 ],
 "shark-body1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  }
 ],
 "shark-body2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  }
 ],
 "shark-body3": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body3",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  }
 ],
 "shark-end": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-end",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  }
 ],
 "shark-tail": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  }
 ],
 "intro-1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "intro-1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 1136,
     "sourceHeight": 640
    }
   }
  }
 ],
 "person-a-gib1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-a-gib1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 62,
     "sourceHeight": 118
    }
   }
  }
 ],
 "person-a-gib2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-a-gib2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 53,
     "sourceHeight": 100
    }
   }
  }
 ],
 "person-a-gib3": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-a-gib3",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 35,
     "sourceHeight": 32
    }
   }
  }
 ],
 "person-a-gib4": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-a-gib4",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 38,
     "sourceHeight": 78
    }
   }
  }
 ],
 "person-a-gib5": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-a-gib5",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 54,
     "sourceHeight": 100
    }
   }
  }
 ],
 "person-a": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-a",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 115,
     "sourceHeight": 200
    }
   }
  }
 ],
 "wall": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "wall",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 74,
     "sourceHeight": 200
    }
   }
  }
 ],
 "ekg": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "ekg",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 159
    }
   }
  }
 ],
 "wall-gib1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "wall-gib1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 39,
     "sourceHeight": 39
    }
   }
  }
 ],
 "wall-gib2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "wall-gib2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 21,
     "sourceHeight": 20
    }
   }
  }
 ],
 "wall-gib3": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "wall-gib3",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 39,
     "sourceHeight": 29
    }
   }
  }
 ],
 "wall-gib4": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "wall-gib4",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 27,
     "sourceHeight": 21
    }
   }
  }
 ],
 "people-gib1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "people-gib1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 20,
     "sourceHeight": 22
    }
   }
  }
 ],
 "people-gib2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "people-gib2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 12,
     "sourceHeight": 20
    }
   }
  }
 ],
 "people-gib3": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "people-gib3",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 25,
     "sourceHeight": 17
    }
   }
  }
 ],
 "people-gib4": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "people-gib4",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 17,
     "sourceHeight": 13
    }
   }
  }
 ],
 "potted-plant": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "potted-plant",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 200,
     "sourceHeight": 189
    }
   }
  }
 ],
 "jellybean1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "jellybean1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 41,
     "sourceHeight": 27
    }
   }
  }
 ],
 "jellybean2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "jellybean2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 41,
     "sourceHeight": 27
    }
   }
  }
 ],
 "jellybean3": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "jellybean3",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 42,
     "sourceHeight": 27
    }
   }
  }
 ],
 "jellybean4": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "jellybean4",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 42,
     "sourceHeight": 27
    }
   }
  }
 ],
 "jellybean5": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "jellybean5",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 41,
     "sourceHeight": 27
    }
   }
  }
 ],
 "jellybean6": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "jellybean6",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 41,
     "sourceHeight": 27
    }
   }
  }
 ],
 "person-b-gib1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-b-gib1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 53,
     "sourceHeight": 100
    }
   }
  }
 ],
 "person-b-gib2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-b-gib2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 54,
     "sourceHeight": 100
    }
   }
  }
 ],
 "person-b-gib3": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-b-gib3",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 50,
     "sourceHeight": 45
    }
   }
  }
 ],
 "person-b-gib4": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-b-gib4",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 42,
     "sourceHeight": 89
    }
   }
  }
 ],
 "person-b-gib5": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-b-gib5",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 54,
     "sourceHeight": 100
    }
   }
  }
 ],
 "person-b": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-b",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 165,
     "sourceHeight": 247
    }
   }
  }
 ],
 "person-c-gib1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-c-gib1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 52,
     "sourceHeight": 98
    }
   }
  }
 ],
 "person-c-gib2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-c-gib2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 52,
     "sourceHeight": 97
    }
   }
  }
 ],
 "person-c-gib3": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-c-gib3",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 42,
     "sourceHeight": 38
    }
   }
  }
 ],
 "person-c-gib4": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-c-gib4",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 61,
     "sourceHeight": 114
    }
   }
  }
 ],
 "person-c-gib5": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-c-gib5",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 35,
     "sourceHeight": 74
    }
   }
  }
 ],
 "person-c": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-c",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 119,
     "sourceHeight": 200
    }
   }
  }
 ],
 "person-d-gib1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-d-gib1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 52,
     "sourceHeight": 98
    }
   }
  }
 ],
 "person-d-gib2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-d-gib2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 42,
     "sourceHeight": 38
    }
   }
  }
 ],
 "person-d-gib3": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-d-gib3",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 35,
     "sourceHeight": 74
    }
   }
  }
 ],
 "person-d-gib4": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-d-gib4",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 54,
     "sourceHeight": 100
    }
   }
  }
 ],
 "person-d-gib5": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-d-gib5",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 52,
     "sourceHeight": 97
    }
   }
  }
 ],
 "person-d": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "person-d",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 102,
     "sourceHeight": 190
    }
   }
  }
 ],
 "breathing-machine": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "breathing-machine",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 65,
     "sourceHeight": 188
    }
   }
  }
 ],
 "doctor-gib1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "doctor-gib1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 64,
     "sourceHeight": 119
    }
   }
  }
 ],
 "doctor-gib2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "doctor-gib2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 63,
     "sourceHeight": 117
    }
   }
  }
 ],
 "doctor-gib3": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "doctor-gib3",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 43,
     "sourceHeight": 90
    }
   }
  }
 ],
 "doctor-gib4": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "doctor-gib4",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 74,
     "sourceHeight": 137
    }
   }
  }
 ],
 "doctor-gib5": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "doctor-gib5",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 50,
     "sourceHeight": 46
    }
   }
  }
 ],
 "doctor": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "doctor",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 133,
     "sourceHeight": 200
    }
   }
  }
 ],
 "people-gib5": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "people-gib5",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 22,
     "sourceHeight": 30
    }
   }
  }
 ],
 "people-gib6": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "people-gib6",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 25,
     "sourceHeight": 26
    }
   }
  }
 ],
 "hospital-bed": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "hospital-bed",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 173,
     "sourceHeight": 300
    }
   }
  }
 ],
 "doctor-intro1": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "doctor-intro1",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 446,
     "sourceHeight": 640
    }
   }
  }
 ],
 "doctor-intro2-f2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "doctor-intro2-f2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 645,
     "sourceHeight": 640
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "doctor-intro2-f2",
     "sourceX": 645,
     "sourceY": 0,
     "sourceWidth": 645,
     "sourceHeight": 640
    }
   }
  }
 ],
 "endscreen": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "endscreen",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 1136,
     "sourceHeight": 640
    }
   }
  }
 ],
 "legs": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "legs",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 940,
     "sourceHeight": 426
    }
   }
  }
 ],
 "titlescreen": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "titlescreen",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 1136,
     "sourceHeight": 640
    }
   }
  }
 ],
 "shark-head-f10": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 95,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 190,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 285,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 380,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 475,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 570,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 665,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 760,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-head-f10",
     "sourceX": 855,
     "sourceY": 0,
     "sourceWidth": 95,
     "sourceHeight": 99
    }
   }
  }
 ],
 "shark-body2-f10": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 100,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 200,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 300,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 400,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 500,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 600,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 700,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 800,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-body2-f10",
     "sourceX": 900,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  }
 ],
 "shark-tail-f9": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail-f9",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail-f9",
     "sourceX": 100,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail-f9",
     "sourceX": 200,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail-f9",
     "sourceX": 300,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail-f9",
     "sourceX": 400,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail-f9",
     "sourceX": 500,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail-f9",
     "sourceX": 600,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail-f9",
     "sourceX": 700,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "shark-tail-f9",
     "sourceX": 800,
     "sourceY": 0,
     "sourceWidth": 100,
     "sourceHeight": 100
    }
   }
  }
 ],
 "arrows-f2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "arrows-f2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 188,
     "sourceHeight": 126
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "arrows-f2",
     "sourceX": 188,
     "sourceY": 0,
     "sourceWidth": 188,
     "sourceHeight": 126
    }
   }
  }
 ],
 "arrows-left-f2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "arrows-left-f2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 188,
     "sourceHeight": 126
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "arrows-left-f2",
     "sourceX": 188,
     "sourceY": 0,
     "sourceWidth": 188,
     "sourceHeight": 126
    }
   }
  }
 ],
 "arrows-right-f2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "arrows-right-f2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 188,
     "sourceHeight": 126
    }
   }
  },
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "arrows-right-f2",
     "sourceX": 188,
     "sourceY": 0,
     "sourceWidth": 188,
     "sourceHeight": 126
    }
   }
  }
 ],
 "intro-2": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "intro-2",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 1136,
     "sourceHeight": 640
    }
   }
  }
 ],
 "black-screen": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "black-screen",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 1136,
     "sourceHeight": 640
    }
   }
  }
 ],
 "go": [
  {
   "time": 100,
   "properties": {
    "image": {
     "name": "go",
     "sourceX": 0,
     "sourceY": 0,
     "sourceWidth": 382,
     "sourceHeight": 204
    }
   }
  }
 ]
}
},{}],3:[function(require,module,exports){
module.exports=[
 {
  "id": 0,
  "name": "player",
  "position": {
   "x": 100,
   "y": 100
  },
  "size": {
   "width": 54,
   "height": 56
  },
  "rotation": {
   "x": 27,
   "y": 28,
   "angle": 0,
   "velocity": 0.001
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "shark-head-f10"
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -23,
   "destinationY": -18,
   "destinationWidth": 100,
   "destinationHeight": 96
  },
  "playerController2d": {
   "up": "up",
   "down": "down",
   "left": "left",
   "right": "right"
  },
  "movement2d": {
   "up": false,
   "down": false,
   "left": false,
   "right": false,
   "upAccel": -0.1,
   "downAccel": 0.1,
   "leftAccel": -0.1,
   "rightAccel": 0.1,
   "upMax": -1,
   "downMax": 1,
   "leftMax": -1,
   "rightMax": 1
  },
  "velocity": {
   "x": 0,
   "y": 0
  },
  "collisions": []
 },
 {
  "id": 1,
  "name": "middle",
  "position": {
   "x": 200,
   "y": 100
  },
  "size": {
   "width": 54,
   "height": 54
  },
  "rotation": {
   "x": 27,
   "y": 27,
   "angle": 0,
   "velocity": 0
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "shark-end"
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -23,
   "destinationY": -23,
   "destinationWidth": 100,
   "destinationHeight": 100
  },
  "follow": {
   "id": 0,
   "distance": 50
  },
  "gibs": [
   "people-gib1",
   "people-gib2",
   "people-gib3",
   "people-gib4",
   "people-gib5",
   "people-gib6"
  ]
 },
 {
  "id": 2,
  "name": "tail",
  "position": {
   "x": 300,
   "y": 100
  },
  "size": {
   "width": 54,
   "height": 54
  },
  "rotation": {
   "x": 27,
   "y": 27,
   "angle": 0,
   "velocity": 0
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "shark-tail-f9"
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -23,
   "destinationY": -23,
   "destinationWidth": 100,
   "destinationHeight": 100
  },
  "collisions": [],
  "follow": {
   "id": 1,
   "distance": 50
  },
  "gibs": [
   "people-gib1",
   "people-gib2",
   "people-gib3",
   "people-gib4",
   "people-gib5",
   "people-gib6"
  ]
 },
 {
  "id": 4,
  "name": "camera",
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 600,
   "height": 600
  },
  "camera": {
   "x": 0,
   "y": 0
  },
  "follow": {
   "id": 0,
   "distance": 100
  }
 }
]
},{}],4:[function(require,module,exports){
module.exports={
 "arrows-f2": "images/arrows-f2.png",
 "arrows-left-f2": "images/arrows-left-f2.png",
 "arrows-right-f2": "images/arrows-right-f2.png",
 "black-screen": "images/black-screen.png",
 "breathing-machine": "images/breathing-machine.png",
 "doctor-gib1": "images/doctor-gib1.png",
 "doctor-gib2": "images/doctor-gib2.png",
 "doctor-gib3": "images/doctor-gib3.png",
 "doctor-gib4": "images/doctor-gib4.png",
 "doctor-gib5": "images/doctor-gib5.png",
 "doctor-intro1": "images/doctor-intro1.png",
 "doctor-intro2-f2": "images/doctor-intro2-f2.png",
 "doctor": "images/doctor.png",
 "ekg": "images/ekg.png",
 "endscreen": "images/endscreen.png",
 "floor-1": "images/floor-1.png",
 "floor-2": "images/floor-2.png",
 "go": "images/go.png",
 "hospital-bed": "images/hospital-bed.png",
 "intro-1": "images/intro-1.png",
 "intro-2": "images/intro-2.png",
 "iv": "images/iv.png",
 "legs": "images/legs.png",
 "people-gib1": "images/people-gib1.png",
 "people-gib2": "images/people-gib2.png",
 "people-gib3": "images/people-gib3.png",
 "people-gib4": "images/people-gib4.png",
 "people-gib5": "images/people-gib5.png",
 "people-gib6": "images/people-gib6.png",
 "person-a-gib1": "images/person-a-gib1.png",
 "person-a-gib2": "images/person-a-gib2.png",
 "person-a-gib3": "images/person-a-gib3.png",
 "person-a-gib4": "images/person-a-gib4.png",
 "person-a-gib5": "images/person-a-gib5.png",
 "person-a": "images/person-a.png",
 "person-b-gib1": "images/person-b-gib1.png",
 "person-b-gib2": "images/person-b-gib2.png",
 "person-b-gib3": "images/person-b-gib3.png",
 "person-b-gib4": "images/person-b-gib4.png",
 "person-b-gib5": "images/person-b-gib5.png",
 "person-b": "images/person-b.png",
 "person-c-gib1": "images/person-c-gib1.png",
 "person-c-gib2": "images/person-c-gib2.png",
 "person-c-gib3": "images/person-c-gib3.png",
 "person-c-gib4": "images/person-c-gib4.png",
 "person-c-gib5": "images/person-c-gib5.png",
 "person-c": "images/person-c.png",
 "person-d-gib1": "images/person-d-gib1.png",
 "person-d-gib2": "images/person-d-gib2.png",
 "person-d-gib3": "images/person-d-gib3.png",
 "person-d-gib4": "images/person-d-gib4.png",
 "person-d-gib5": "images/person-d-gib5.png",
 "person-d": "images/person-d.png",
 "potted-plant": "images/potted-plant.png",
 "shark-body1": "images/shark-body1.png",
 "shark-body2-f10": "images/shark-body2-f10.png",
 "shark-body2": "images/shark-body2.png",
 "shark-body3": "images/shark-body3.png",
 "shark-end": "images/shark-end.png",
 "shark-head-f10": "images/shark-head-f10.png",
 "shark-tail-f9": "images/shark-tail-f9.png",
 "shark-tail": "images/shark-tail.png",
 "titlescreen": "images/titlescreen.png",
 "wall-gib1": "images/wall-gib1.png",
 "wall-gib2": "images/wall-gib2.png",
 "wall-gib3": "images/wall-gib3.png",
 "wall-gib4": "images/wall-gib4.png",
 "wall": "images/wall.png"
}
},{}],5:[function(require,module,exports){
module.exports={
 "up": {
  "type": "button",
  "inputs": [
   [
    "keyboard",
    "w"
   ],
   [
    "keyboard",
    "up"
   ]
  ]
 },
 "down": {
  "type": "button",
  "inputs": [
   [
    "keyboard",
    "s"
   ],
   [
    "keyboard",
    "down"
   ]
  ]
 },
 "left": {
  "type": "button",
  "inputs": [
   [
    "keyboard",
    "a"
   ],
   [
    "keyboard",
    "left"
   ]
  ]
 },
 "right": {
  "type": "button",
  "inputs": [
   [
    "keyboard",
    "d"
   ],
   [
    "keyboard",
    "right"
   ]
  ]
 }
}
},{}],6:[function(require,module,exports){
"use strict";

var copyEntityComponents = require("./copy-entity-components");

function findFollower(entities, parent) {
	var keys = Object.keys(entities);
	for (var i = 0; i < keys.length; i++) {
		var entity = entities[keys[i]];
		if (entity.follow !== undefined && entity.follow.id === parent.id && !entity.camera) {
			return entity;
		}
	}
	return undefined;
}

function getSegmentAnimation(current) {
	var anims = ["shark-body1", "shark-body2-f10", "shark-body3"];
	var index = (anims.indexOf(current) + 1) % anims.length;
	return anims[index];
}

function addSegment(entities, parent) {
	var follower = findFollower(entities.entities, parent);

	var newSegment = copyEntityComponents(follower, entities.add());
	newSegment.animation.name = getSegmentAnimation(newSegment.animation.name);
	newSegment.animation.frame = 0;
	follower.follow.id = newSegment.id;
	follower.timers = {
		regib: {
			running: true,
			time: 0,
			max: 1000,
			script: "./lib/add-collisions"
		}
	};
}

module.exports = addSegment;

},{"./copy-entity-components":7}],7:[function(require,module,exports){
"use strict";

module.exports = function(src, dest) {
	var clone = JSON.parse(JSON.stringify(src));
	Object.keys(clone).forEach(function(key) {
		if (key === "id") {
			return;
		}
		dest[key] = clone[key];
	});
	return dest;
};

},{}],8:[function(require,module,exports){
"use strict";

module.exports = function getCamera(entities) {
	return entities[4];
};

},{}],9:[function(require,module,exports){
"use strict";

// converts a changing absolute value into a value relative to the previous value
module.exports = function() {
	var last = -1;
	return function(current) {
		if (last === -1) {
			last = current;
		}
		var delta = current - last;
		last = current;
		return delta;
	};
};

},{}],10:[function(require,module,exports){
"use strict";
/**
 * @namespace Splat.ads
 */

var platform = require("./platform");

if (platform.isEjecta()) {
	var adBanner = new window.Ejecta.AdBanner();

	var isLandscape = window.innerWidth > window.innerHeight;

	var sizes = {
		"iPhone": {
			"portrait": {
				"width": 320,
				"height": 50
			},
			"landscape": {
				"width": 480,
				"height": 32
			}
		},
		"iPad": {
			"portrait": {
				"width": 768,
				"height": 66
			},
			"landscape": {
				"width": 1024,
				"height": 66
			}
		}
	};

	var device = window.navigator.userAgent.indexOf("iPad") >= 0 ? "iPad" : "iPhone";
	var size = sizes[device][isLandscape ? "landscape" : "portrait"];

	module.exports = {
		/**
		 * Show an advertisement.
		 * @alias Splat.ads.show
		 * @param {boolean} isAtBottom true if the ad should be shown at the bottom of the screen. false if it should be shown at the top.
		 */
		"show": function(isAtBottom) {
			adBanner.isAtBottom = isAtBottom;
			adBanner.show();
		},
		/**
		 * Hide the current advertisement.
		 * @alias Splat.ads.hide
		 */
		"hide": function() {
			adBanner.hide();
		},
		/**
		 * The width of the ad that will show.
		 * @alias Splat.ads#width
		 */
		"width": size.width,
		/**
		 * The height of the ad that will show.
		 * @alias Splat.ads#height
		 */
		"height": size.height
	};
} else {
	module.exports = {
		"show": function() {},
		"hide": function() {},
		"width": 0,
		"height": 0,
	};
}

},{"./platform":37}],11:[function(require,module,exports){
"use strict";

var BinaryHeap = require("./binary_heap");

/**
 * Implements the [A* pathfinding algorithm]{@link http://en.wikipedia.org/wiki/A*_search_algorithm} on a 2-dimensional grid. You can use this to find a path between a source and destination coordinate while avoiding obstacles.
 * @constructor
 * @alias Splat.AStar
 * @param {isWalkable} isWalkable A function to test if a coordinate is walkable by the entity you're performing the pathfinding for.
 */
function AStar(isWalkable) {
	this.destX = 0;
	this.destY = 0;
	this.scaleX = 1;
	this.scaleY = 1;
	this.openNodes = {};
	this.closedNodes = {};
	this.openHeap = new BinaryHeap(function(a, b) {
		return a.f - b.f;
	});
	this.isWalkable = isWalkable;
}
/**
 * The [A* heuristic]{@link http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html}, commonly referred to as h(x), that estimates how far a location is from the destination. This implementation is the [Manhattan method]{@link http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html#manhattan-distance}, which is good for situations when the entity can travel in four directions. Feel free to replace this with a different heuristic implementation.
 * @param {number} x The x coordinate to estimate the distance to the destination.
 * @param {number} y The y coordinate to estimate the distance to the destination.
 */
AStar.prototype.heuristic = function(x, y) {
	// manhattan method
	var dx = Math.abs(x - this.destX) / this.scaleX;
	var dy = Math.abs(y - this.destY) / this.scaleY;
	return dx + dy;
};
/**
 * Make a node to track a given coordinate
 * @param {number} x The x coordinate of the node
 * @param {number} y The y coordinate of the node
 * @param {object} parent The parent node for the current node. This chain of parents eventually points back at the starting node.
 * @param {number} g The g(x) travel cost from the parent node to this node.
 * @private
 */
AStar.prototype.makeNode = function(x, y, parent, g) {
	g += parent.g;
	var h = this.heuristic(x, y);

	return {
		x: x,
		y: y,
		parent: parent,
		f: g + h,
		g: parent.g + g,
		h: h
	};
};
/**
 * Update the g(x) travel cost to a node if a new lower-cost path is found.
 * @param {string} key The key of the node on the open list.
 * @param {object} parent A parent node that may have a shorter path for the node specified in key.
 * @param {number} g The g(x) travel cost from parent to the node specified in key.
 * @private
 */
AStar.prototype.updateOpenNode = function(key, parent, g) {
	var node = this.openNodes[key];
	if (!node) {
		return false;
	}

	var newG = parent.g + g;

	if (newG >= node.g) {
		return true;
	}

	node.parent = parent;
	node.g = newG;
	node.f = node.g + node.h;

	var pos = this.openHeap.indexOf(node);
	this.openHeap.bubbleUp(pos);

	return true;
};
/**
 * Create a neighbor node to a parent node, and add it to the open list for consideration.
 * @param {string} key The key of the new neighbor node.
 * @param {number} x The x coordinate of the new neighbor node.
 * @param {number} y The y coordinate of the new neighbor node.
 * @param {object} parent The parent node of the new neighbor node.
 * @param {number} g The travel cost from the parent to the new parent node.
 * @private
 */
AStar.prototype.insertNeighbor = function(key, x, y, parent, g) {
	var node = this.makeNode(x, y, parent, g);
	this.openNodes[key] = node;
	this.openHeap.insert(node);
};
AStar.prototype.tryNeighbor = function(x, y, parent, g) {
	var key = makeKey(x, y);
	if (this.closedNodes[key]) {
		return;
	}
	if (!this.isWalkable(x, y)) {
		return;
	}
	if (!this.updateOpenNode(key, parent, g)) {
		this.insertNeighbor(key, x, y, parent, g);
	}
};
AStar.prototype.getNeighbors = function getNeighbors(parent) {
	var diagonalCost = 1.4;
	var straightCost = 1;
	this.tryNeighbor(parent.x - this.scaleX, parent.y - this.scaleY, parent, diagonalCost);
	this.tryNeighbor(parent.x, parent.y - this.scaleY, parent, straightCost);
	this.tryNeighbor(parent.x + this.scaleX, parent.y - this.scaleY, parent, diagonalCost);

	this.tryNeighbor(parent.x - this.scaleX, parent.y, parent, straightCost);
	this.tryNeighbor(parent.x + this.scaleX, parent.y, parent, straightCost);

	this.tryNeighbor(parent.x - this.scaleX, parent.y + this.scaleY, parent, diagonalCost);
	this.tryNeighbor(parent.x, parent.y + this.scaleY, parent, straightCost);
	this.tryNeighbor(parent.x + this.scaleX, parent.y + this.scaleY, parent, diagonalCost);
};

function generatePath(node) {
	var path = [];
	while (node.parent) {
		var ix = node.x;
		var iy = node.y;
		while (ix !== node.parent.x || iy !== node.parent.y) {
			path.unshift({x: ix, y: iy});

			var dx = node.parent.x - ix;
			if (dx > 0) {
				ix++;
			} else if (dx < 0) {
				ix--;
			}
			var dy = node.parent.y - iy;
			if (dy > 0) {
				iy++;
			} else if (dy < 0) {
				iy--;
			}
		}
		node = node.parent;
	}
	return path;
}

function makeKey(x, y) {
	return x + "," + y;
}

/**
 * Search for an optimal path between srcX, srcY and destX, destY, while avoiding obstacles.
 * @param {number} srcX The starting x coordinate
 * @param {number} srcY The starting y coordinate
 * @param {number} destX The destination x coordinate
 * @param {number} destY The destination y coordinate
 * @returns {Array} The optimal path, in the form of an array of objects that each have an x and y property.
 */
AStar.prototype.search = function aStar(srcX, srcY, destX, destY) {
	function scale(c, s) {
		var downscaled = (c / s) |0;
		return downscaled * s;
	}
	srcX = scale(srcX, this.scaleX);
	srcY = scale(srcY, this.scaleY);
	this.destX = scale(destX, this.scaleX);
	this.destY = scale(destY, this.scaleY);

	if (!this.isWalkable(this.destX, this.destY)) {
		return [];
	}

	var srcKey = makeKey(srcX, srcY);
	var srcNode = {
		x: srcX,
		y: srcY,
		g: 0,
		h: this.heuristic(srcX, srcY)
	};
	srcNode.f = srcNode.h;
	this.openNodes = {};
	this.openNodes[srcKey]  = srcNode;
	this.openHeap = new BinaryHeap(function(a, b) {
		return a.f - b.f;
	});
	this.openHeap.insert(srcNode);
	this.closedNodes = {};

	var node = this.openHeap.deleteRoot();
	while (node) {
		var key = makeKey(node.x, node.y);
		delete this.openNodes[key];
		this.closedNodes[key] = node;
		if (node.x === this.destX && node.y === this.destY) {
			return generatePath(node);
		}
		this.getNeighbors(node);
		node = this.openHeap.deleteRoot();
	}
	return [];
};

module.exports = AStar;

},{"./binary_heap":12}],12:[function(require,module,exports){
"use strict";

/**
 * An implementation of the [Binary Heap]{@link https://en.wikipedia.org/wiki/Binary_heap} data structure suitable for priority queues.
 * @constructor
 * @alias Splat.BinaryHeap
 * @param {compareFunction} cmp A comparison function that determines how the heap is sorted.
 */
function BinaryHeap(cmp) {
	/**
	 * The comparison function for sorting the heap.
	 * @member {compareFunction}
	 * @private
	 */
	this.cmp = cmp;
	/**
	 * The list of elements in the heap.
	 * @member {Array}
	 * @private
	 */
	this.array = [];
	/**
	 * The number of elements in the heap.
	 * @member {number}
	 * @readonly
	 */
	this.length = 0;
}
/**
 * Calculate the index of a node's parent.
 * @param {number} i The index of the child node
 * @returns {number}
 * @private
 */
BinaryHeap.prototype.parentIndex = function(i) {
	return ((i - 1) / 2) |0;
};
/**
 * Calculate the index of a parent's first child node.
 * @param {number} i The index of the parent node
 * @returns {number}
 * @private
 */
BinaryHeap.prototype.firstChildIndex = function(i) {
	return (2 * i) + 1;
};
/**
 * Bubble a node up the heap, stopping when it's value should not be sorted before its parent's value.
 * @param {number} pos The index of the node to bubble up.
 * @private
 */
BinaryHeap.prototype.bubbleUp = function(pos) {
	if (pos === 0) {
		return;
	}

	var data = this.array[pos];
	var parentIndex = this.parentIndex(pos);
	var parent = this.array[parentIndex];
	if (this.cmp(data, parent) < 0) {
		this.array[parentIndex] = data;
		this.array[pos] = parent;
		this.bubbleUp(parentIndex);
	}
};
/**
 * Store a new node in the heap.
 * @param {object} data The data to store
 */
BinaryHeap.prototype.insert = function(data) {
	this.array.push(data);
	this.length = this.array.length;
	var pos = this.array.length - 1;
	this.bubbleUp(pos);
};
/**
 * Bubble a node down the heap, stopping when it's value should not be sorted after its parent's value.
 * @param {number} pos The index of the node to bubble down.
 * @private
 */
BinaryHeap.prototype.bubbleDown = function(pos) {
	var left = this.firstChildIndex(pos);
	var right = left + 1;
	var largest = pos;
	if (left < this.array.length && this.cmp(this.array[left], this.array[largest]) < 0) {
		largest = left;
	}
	if (right < this.array.length && this.cmp(this.array[right], this.array[largest]) < 0) {
		largest = right;
	}
	if (largest !== pos) {
		var tmp = this.array[pos];
		this.array[pos] = this.array[largest];
		this.array[largest] = tmp;
		this.bubbleDown(largest);
	}
};
/**
 * Remove the heap's root node, and return it. The root node is whatever comes first as determined by the {@link compareFunction}.
 * @returns {data} The root node's data.
 */
BinaryHeap.prototype.deleteRoot = function() {
	var root = this.array[0];
	if (this.array.length <= 1) {
		this.array = [];
		this.length = 0;
		return root;
	}
	this.array[0] = this.array.pop();
	this.length = this.array.length;
	this.bubbleDown(0);
	return root;
};
/**
 * Search for a node in the heap.
 * @param {object} data The data to search for.
 * @returns {number} The index of the data in the heap, or -1 if it is not found.
 */
BinaryHeap.prototype.indexOf = function(data) {
	for (var i = 0; i < this.array.length; i++) {
		if (this.array[i] === data) {
			return i;
		}
	}
	return -1;
};

module.exports = BinaryHeap;

},{}],13:[function(require,module,exports){
"use strict";
/** @module buffer */

var platform = require("./platform");

/**
 * Make an invisible {@link canvas}.
 * @param {number} width The width of the canvas
 * @param {number} height The height of the canvas
 * @returns {external:canvas} A canvas DOM element
 * @private
 */
function makeCanvas(width, height) {
	var c = document.createElement("canvas");
	c.width = width;
	c.height = height;
	// when retina support is enabled, context.getImageData() reads from the wrong pixel causing NinePatch to break
	if (platform.isEjecta()) {
		c.retinaResolutionEnabled = false;
	}
	return c;
}

/**
 * Make an invisible canvas buffer, and draw on it.
 * @param {number} width The width of the buffer
 * @param {number} height The height of the buffer
 * @param {drawCallback} drawFun The callback that draws on the buffer
 * @returns {external:canvas} The drawn buffer
 */
function makeBuffer(width, height, drawFun) {
	var canvas = makeCanvas(width, height);
	var ctx = canvas.getContext("2d");
	// when image smoothing is enabled, the image gets blurred and the pixel data isn't correct even when the image shouldn't be scaled which breaks NinePatch
	if (platform.isEjecta()) {
		ctx.imageSmoothingEnabled = false;
	}
	drawFun(ctx);
	return canvas;
}

/**
 * Make a horizonally-flipped copy of a buffer or image.
 * @param {external:canvas|external:image} buffer The original image
 * @return {external:canvas} The flipped buffer
 */
function flipBufferHorizontally(buffer) {
	return makeBuffer(buffer.width, buffer.height, function(context) {
		context.scale(-1, 1);
		context.drawImage(buffer, -buffer.width, 0);
	});
}

/**
 * Make a vertically-flipped copy of a buffer or image.
 * @param {external:canvas|external:image} buffer The original image
 * @return {external:canvas} The flipped buffer
 */
function flipBufferVertically(buffer) {
	return makeBuffer(buffer.width, buffer.height, function(context) {
		context.scale(1, -1);
		context.drawImage(buffer, 0, -buffer.height);
	});
}
/**
 * Make a copy of a buffer that is rotated 90 degrees clockwise.
 * @param {external:canvas|external:image} buffer The original image
 * @return {external:canvas} The rotated buffer
 */
function rotateClockwise(buffer) {
	var w = buffer.height;
	var h = buffer.width;
	var w2 = Math.floor(w / 2);
	var h2 = Math.floor(h / 2);
	return makeBuffer(w, h, function(context) {
		context.translate(w2, h2);
		context.rotate(Math.PI / 2);
		context.drawImage(buffer, -h2, -w2);
	});
}
/**
 * Make a copy of a buffer that is rotated 90 degrees counterclockwise.
 * @param {external:canvas|external:image} buffer The original image
 * @return {external:canvas} The rotated buffer
 */
function rotateCounterclockwise(buffer) {
	var w = buffer.height;
	var h = buffer.width;
	var w2 = Math.floor(w / 2);
	var h2 = Math.floor(h / 2);
	return makeBuffer(w, h, function(context) {
		context.translate(w2, h2);
		context.rotate(-Math.PI / 2);
		context.drawImage(buffer, -h2, -w2);
	});
}

module.exports = {
	makeBuffer: makeBuffer,
	flipBufferHorizontally: flipBufferHorizontally,
	flipBufferVertically: flipBufferVertically,
	rotateClockwise: rotateClockwise,
	rotateCounterclockwise: rotateCounterclockwise
};

},{"./platform":37}],14:[function(require,module,exports){
"use strict";

module.exports = function animation(name, loop) {
	return {
		name: name,
		time: 0,
		frame: 0,
		loop: loop,
		speed: 1
	};
};

},{}],15:[function(require,module,exports){
"use strict";

module.exports = function position(x, y) {
	return { x: x, y: y };
};

},{}],16:[function(require,module,exports){
"use strict";

module.exports = function friction(x, y) {
	return { x: x, y: y };
};

},{}],17:[function(require,module,exports){
"use strict";

module.exports = function image(name, sourceX, sourceY, sourceWidth, sourceHeight, destinationX, destinationY, destinationWidth, destinationHeight) {
	return {
		name: name,
		sourceX: sourceX,
		sourceY: sourceY,
		sourceWidth: sourceWidth,
		sourceHeight: sourceHeight,
		destinationX: destinationX,
		destinationY: destinationY,
		destinationWidth: destinationWidth,
		destinationHeight: destinationHeight
	};
};

},{}],18:[function(require,module,exports){
"use strict";

module.exports = function movement2d(accel, max) {
	return {
		up: false,
		down: false,
		left: false,
		right: false,
		upAccel: -accel,
		downAccel: accel,
		leftAccel: -accel,
		rightAccel: accel,
		upMax: -max,
		downMax: max,
		leftMax: -max,
		rightMax: max
	};
};

},{}],19:[function(require,module,exports){
"use strict";

module.exports = function playableArea(x, y, width, height) {
	return { x: x, y: y, width: width, height: height };
};

},{}],20:[function(require,module,exports){
"use strict";

module.exports = function playerController2d(up, down, left, right) {
	return { up: up, down: down, left: left, right: right };
};

},{}],21:[function(require,module,exports){
module.exports=require(15)
},{"/home/eric/src/ggj15/node_modules/splatjs/lib/components/camera.js":15}],22:[function(require,module,exports){
"use strict";

module.exports = function size(width, height) {
	return { width: width, height: height };
};

},{}],23:[function(require,module,exports){
"use strict";

module.exports = function timers() {
	return {};
};

},{}],24:[function(require,module,exports){
"use strict";

module.exports = function velocity(x, y) {
	return { x: x, y: y };
};

},{}],25:[function(require,module,exports){
"use strict";

function EntityPool() {
	this.nextId = 0;
	this.entities = {};
}
EntityPool.prototype.add = function() {
	var id = this.nextId;
	this.nextId++;
	var entity = { id: id };
	this.entities[id] = entity;
	return entity;
};
EntityPool.prototype.save = function() {
	return objectValues(this.entities);
};
EntityPool.prototype.load = function(data) {
	this.entities = data.reduce(function(entities, entity) {
		entities[entity.id] = entity;
		if (this.nextId <= entity.id) {
			this.nextId = entity.id + 1;
		}
		return entities;
	}.bind(this), this.entities);
};

function objectValues(obj) {
	return Object.keys(obj).map(function(key) {
		return obj[key];
	});
}

module.exports = EntityPool;

},{}],26:[function(require,module,exports){
"use strict";

var timeAccumulator = require("time-accumulator");

module.exports = function(entities, simulation, simulationStepTime, renderer, context) {
	var run = timeAccumulator(simulationStepTime);
	var timeDelta = require("./absolute-to-relative")();
	var running = true;

	function render(time) {
		if (!running) {
			return;
		}

		var elapsed = timeDelta(time);
		run(elapsed, function(elapsed) {
			simulation.run(entities.entities, elapsed);
		});

		context.save();
		renderer.run(entities.entities, context, elapsed);
		context.restore();

		if (running) {
			window.requestAnimationFrame(render);
		}
	}
	window.requestAnimationFrame(render);

	return function() {
		running = false;
	};
};

},{"./absolute-to-relative":9,"time-accumulator":58}],27:[function(require,module,exports){
"use strict";

var platform = require("./platform");

if (platform.isEjecta()) {
	var iap = new window.Ejecta.IAPManager();

	module.exports = {
		"get": function(sku, callback) {
			iap.getProducts([sku], function(err, products) {
				if (err) {
					callback(err);
					return;
				}
				callback(undefined, products[0]);
			});
		},
		"buy": function(product, quantity, callback) {
			product.purchase(quantity, callback);
		},
		"restore": function(callback) {
			iap.restoreTransactions(function(err, transactions) {
				if (err) {
					callback(err);
					return;
				}
				callback(undefined, transactions.map(function(transaction) {
					return transaction.productId;
				}));
			});
		}
	};
} else if (platform.isChromeApp()) {
	// FIXME: needs google's buy.js included
	// https://developer.chrome.com/webstore/payments-iap
	module.exports = {
		"get": function(sku, callback) {
			window.google.payments.inapp.getSkuDetails({
				"parameters": {
					"env": "prod"
				},
				"sku": sku,
				"success": function(response) {
					callback(undefined, response.response.details.inAppProducts[0]);
				},
				"failure": function(response) {
					callback(response);
				}
			});
		},
		"buy": function(product, quantity, callback) { // jshint ignore:line
			window.google.payments.inapp.buy({
				"parameters": {
					"env": "prod"
				},
				"sku": product.sku,
				"success": function(response) {
					callback(undefined, response);
				},
				"failure": function(response) {
					callback(response);
				}
			});
		},
		"restore": function(callback) {
			window.google.payments.inapp.getPurchases({
				"success": function(response) {
					callback(undefined, response.response.details.map(function(detail) {
						return detail.sku;
					}));
				},
				"failure": function(response) {
					callback(response);
				}
			});
		}
	};
} else {
	module.exports = {
		"get": function(sku, callback) { // jshint ignore:line
			callback(undefined, undefined);
		},
		"buy": function(product, quantity, callback) { // jshint ignore:line
			callback(undefined);
		},
		"restore": function(callback) {
			callback(undefined, []);
		}
	};
}

},{"./platform":37}],28:[function(require,module,exports){
"use strict";

/**
 * Loads {@link external:image}s and lets you know when they're all available. An instance of ImageLoader is available as {@link Splat.Game#images}.
 * @constructor
 */
function ImageLoader(onLoad) {
	/**
	 * The key-value object that stores named {@link external:image}s
	 * @member {object}
	 * @private
	 */
	this.images = {};
	/**
	 * The total number of images to be loaded.
	 * @member {number}
	 * @private
	 */
	this.totalImages = 0;
	/**
	 * The number of images that have loaded completely.
	 * @member {number}
	 * @private
	 */
	this.loadedImages = 0;
	/**
	 * The names of all the images that were requested to be loaded.
	 * @member {Array}
	 * @private
	 */
	this.names = [];
	/**
	 * A callback to be called once all images are loaded.
	 * @member {Array}
	 * @private
	 */
	this.onLoad = onLoad;
}
/**
 * Load an {@link external:image}.
 * @param {string} name The name you want to use when you {@link ImageLoader#get} the {@link external:image}
 * @param {string} path The path of the {@link external:image}.
 */
ImageLoader.prototype.load = function(name, path) {
	// only load an image once
	if (this.names.indexOf(name) > -1) {
		return;
	}
	this.names.push(name);

	this.totalImages++;

	var img = new Image();
	var self = this;
	img.addEventListener("load", function() {
		self.loadedImages++;
		self.images[name] = img;
		if (self.allLoaded() && self.onLoad) {
			self.onLoad();
		}
	});
	img.addEventListener("error", function() {
		console.error("Error loading image " + path);
	});
	img.src = path;
};
ImageLoader.prototype.loadFromManifest = function(manifest) {
	var keys = Object.keys(manifest);
	var self = this;
	keys.forEach(function(key) {
		self.load(key, manifest[key]);
	});
};

/**
 * Test if all {@link external:image}s have loaded.
 * @returns {boolean}
 */
ImageLoader.prototype.allLoaded = function() {
	return this.totalImages === this.loadedImages;
};
/**
 * Retrieve a loaded {@link external:image}.
 * @param {string} name The name given to the image during {@link ImageLoader#load}.
 * @returns {external:image}
 */
ImageLoader.prototype.get = function(name) {
	return this.images[name];
};

module.exports = ImageLoader;

},{}],29:[function(require,module,exports){
"use strict";

var Keyboard = require("game-keyboard");
var keyMap = require("game-keyboard/key_map").US;
var keyboard = new Keyboard(keyMap);

function Input(config) {
	this.config = config;
}
Input.prototype.button = function(name) {
	var input = this.config[name];
	if (input === undefined) {
		console.error("No such button: " + name);
		return false;
	}
	if (input.type !== "button") {
		console.error("\"" + name + "\" is not a button");
		return false;
	}
	for (var i = 0; i < input.inputs.length; i++) {
		var physicalInput = input.inputs[i];
		var source = physicalInput[0];
		var button = physicalInput[1];
		if (source === "keyboard") {
			if (keyboard.isPressed(button)) {
				return true;
			}
		}
	}
	return false;
};

module.exports = Input;

},{"game-keyboard":55,"game-keyboard/key_map":56}],30:[function(require,module,exports){
"use strict";
/**
 * @namespace Splat.leaderboards
 */

var platform = require("./platform");

if (platform.isEjecta()) {
	var gameCenter = new window.Ejecta.GameCenter();
	gameCenter.softAuthenticate();

	var authFirst = function(action) {
		if (gameCenter.authed) {
			action();
		} else {
			gameCenter.authenticate(function(err) {
				if (err) {
					return;
				}
				action();
			});
		}
	};

	module.exports = {
		/**
		 * Report that an achievement was achieved.
		 * @alias Splat.leaderboards.reportAchievement
		 * @param {string} id The name of the achievement.
		 * @param {int} percent The percentage of the achievement that is completed in the range of 0-100.
		 */
		"reportAchievement": function(id, percent) {
			authFirst(function() {
				gameCenter.reportAchievement(id, percent);
			});
		},
		/**
		 * Report that a score was achieved on a leaderboard.
		 * @alias Splat.leaderboards.reportScore
		 * @param {string} leaderboard The name of the leaderboard the score is on.
		 * @param {int} score The score that was achieved.
		 */
		"reportScore": function(leaderboard, score) {
			authFirst(function() {
				gameCenter.reportScore(leaderboard, score);
			});
		},
		/**
		 * Show the achievements screen.
		 * @alias Splat.leaderboards.showAchievements
		 */
		"showAchievements": function() {
			authFirst(function() {
				gameCenter.showAchievements();
			});
		},
		/**
		 * Show a leaderboard screen.
		 * @alias Splat.leaderboards.showLeaderboard
		 * @param {string} name The name of the leaderboard to show.
		 */
		"showLeaderboard": function(name) {
			authFirst(function() {
				gameCenter.showLeaderboard(name);
			});
		}
	};
} else {
	module.exports = {
		"reportAchievement": function() {},
		"reportScore": function() {},
		"showAchievements": function() {},
		"showLeaderboard": function() {}
	};
}


},{"./platform":37}],31:[function(require,module,exports){
"use strict";

var Scene = require("./scene");

module.exports = function(canvas, percentLoaded, nextScene) {
	var scene = new Scene();
	scene.renderer.add(function(entities, context) { // jshint ignore:line
		context.fillStyle = "#000000";
		context.fillRect(0, 0, canvas.width, canvas.height);

		var quarterWidth = Math.floor(canvas.width / 4);
		var halfWidth = Math.floor(canvas.width / 2);
		var halfHeight = Math.floor(canvas.height / 2);

		context.fillStyle = "#ffffff";
		context.fillRect(quarterWidth, halfHeight - 15, halfWidth, 30);

		context.fillStyle = "#000000";
		context.fillRect(quarterWidth + 3, halfHeight - 12, halfWidth - 6, 24);

		context.fillStyle = "#ffffff";
		var barWidth = (halfWidth - 6) * percentLoaded();
		context.fillRect(quarterWidth + 3, halfHeight - 12, barWidth, 24);

		if (percentLoaded() === 1) {
			scene.stop();
			nextScene.start(context);
		}
	});
	return scene;
};

},{"./scene":39}],32:[function(require,module,exports){
"use strict";

var buffer = require("./buffer");

/**
 * @namespace Splat
 */
module.exports = {
	makeBuffer: buffer.makeBuffer,
	flipBufferHorizontally: buffer.flipBufferHorizontally,
	flipBufferVertically: buffer.flipBufferVertically,

	ads: require("./ads"),
	AStar: require("./astar"),
	BinaryHeap: require("./binary_heap"),
	EntityPool: require("./entity-pool"),
	iap: require("./iap"),
	ImageLoader: require("./image_loader"),
	Input: require("./input"),
	leaderboards: require("./leaderboards"),
	loadingScene: require("./loading-scene"),
	math: require("./math"),
	openUrl: require("./openUrl"),
	NinePatch: require("./ninepatch"),
	Particles: require("./particles"),
	saveData: require("./save_data"),
	Scene: require("./scene"),
	SoundLoader: require("./sound_loader"),

	components: {
		animation: require("./components/animation"),
		camera: require("./components/camera"),
		friction: require("./components/friction"),
		image: require("./components/image"),
		movement2d: require("./components/movement-2d"),
		playableArea: require("./components/playable-area"),
		playerController2d: require("./components/player-controller-2d"),
		position: require("./components/position"),
		size: require("./components/size"),
		timers: require("./components/timers"),
		velocity: require("./components/velocity"),
	},
	systems: {
		advanceAnimations: require("./systems/advance-animations"),
		advanceTimers: require("./systems/advance-timers"),
		applyFriction: require("./systems/apply-friction"),
		applyMovement2d: require("./systems/apply-movement-2d"),
		applyVelocity: require("./systems/apply-velocity"),
		boxCollider: require("./systems/box-collider"),
		clearScreen: require("./systems/clear-screen"),
		constrainToPlayableArea: require("./systems/constrain-to-playable-area"),
		controlPlayer: require("./systems/control-player"),
		drawFrameRate: require("./systems/draw-frame-rate"),
		drawImage: require("./systems/draw-image"),
		drawRectangles: require("./systems/draw-rectangles"),
		viewport: require("./systems/viewport"),
	}
};

},{"./ads":10,"./astar":11,"./binary_heap":12,"./buffer":13,"./components/animation":14,"./components/camera":15,"./components/friction":16,"./components/image":17,"./components/movement-2d":18,"./components/playable-area":19,"./components/player-controller-2d":20,"./components/position":21,"./components/size":22,"./components/timers":23,"./components/velocity":24,"./entity-pool":25,"./iap":27,"./image_loader":28,"./input":29,"./leaderboards":30,"./loading-scene":31,"./math":33,"./ninepatch":34,"./openUrl":35,"./particles":36,"./save_data":38,"./scene":39,"./sound_loader":40,"./systems/advance-animations":41,"./systems/advance-timers":42,"./systems/apply-friction":43,"./systems/apply-movement-2d":44,"./systems/apply-velocity":45,"./systems/box-collider":46,"./systems/clear-screen":47,"./systems/constrain-to-playable-area":48,"./systems/control-player":49,"./systems/draw-frame-rate":50,"./systems/draw-image":51,"./systems/draw-rectangles":52,"./systems/viewport":53}],33:[function(require,module,exports){
"use strict";

/**
 * Oscillate between -1 and 1 given a value and a period. This is basically a simplification on using Math.sin().
 * @alias Splat.math.oscillate
 * @param {number} current The current value of the number you want to oscillate.
 * @param {number} period The period, or how often the number oscillates. The return value will oscillate between -1 and 1, depending on how close current is to a multiple of period.
 * @returns {number} A number between -1 and 1.
 * @example
Splat.math.oscillate(0, 100); // returns 0
Splat.math.oscillate(100, 100); // returns 0-ish
Splat.math.oscillate(50, 100); // returns 1
Splat.math.oscillate(150, 100); // returns -1
Splat.math.oscillate(200, 100); // returns 0-ish
 */
function oscillate(current, period) {
	return Math.sin(current / period * Math.PI);
}

/**
 * @namespace Splat.math
 */
module.exports = {
	oscillate: oscillate,
	/**
	 * A seedable pseudo-random number generator. Currently a Mersenne Twister PRNG.
	 * @constructor
	 * @alias Splat.math.Random
	 * @param {number} [seed] The seed for the PRNG.
	 * @see [mersenne-twister package at github]{@link https://github.com/boo1ean/mersenne-twister}
	 * @example
var rand = new Splat.math.Random(123);
var val = rand.random();
	 */
	Random: require("mersenne-twister")
};

},{"mersenne-twister":57}],34:[function(require,module,exports){
"use strict";

var buffer = require("./buffer");

function getContextForImage(image) {
	var ctx;
	buffer.makeBuffer(image.width, image.height, function(context) {
		context.drawImage(image, 0, 0, image.width, image.height);
		ctx = context;
	});
	return ctx;
}

/**
 * A stretchable image that has borders.
 * Similar to the [Android NinePatch]{@link https://developer.android.com/guide/topics/graphics/2d-graphics.html#nine-patch}, but it only has the lines on the bottom and right edges to denote the stretchable area.
 * A NinePatch is a normal picture, but has an extra 1-pixel wide column on the right edge and bottom edge. The extra column contains a black line that denotes the tileable center portion of the image. The lines are used to divide the image into nine tiles that can be automatically repeated to stretch the picture to any size without distortion.
 * @constructor
 * @alias Splat.NinePatch
 * @param {external:image} image The source image to make stretchable.
 */
function NinePatch(image) {
	this.img = image;
	var imgw = image.width - 1;
	var imgh = image.height - 1;

	var context = getContextForImage(image);
	var firstDiv = imgw;
	var secondDiv = imgw;
	var pixel;
	var alpha;
	for (var x = 0; x < imgw; x++) {
		pixel = context.getImageData(x, imgh, 1, 1).data;
		alpha = pixel[3];
		if (firstDiv === imgw && alpha > 0) {
			firstDiv = x;
		}
		if (firstDiv < imgw && alpha === 0) {
			secondDiv = x;
			break;
		}
	}
	this.w1 = firstDiv;
	this.w2 = secondDiv - firstDiv;
	this.w3 = imgw - secondDiv;

	firstDiv = secondDiv = imgh;
	for (var y = 0; y < imgh; y++) {
		pixel = context.getImageData(imgw, y, 1, 1).data;
		alpha = pixel[3];
		if (firstDiv === imgh && alpha > 0) {
			firstDiv = y;
		}
		if (firstDiv < imgh && alpha === 0) {
			secondDiv = y;
			break;
		}
	}
	this.h1 = firstDiv;
	this.h2 = secondDiv - firstDiv;
	this.h3 = imgh - secondDiv;
}
/**
 * Draw the image stretched to a given rectangle.
 * @param {external:CanvasRenderingContext2D} context The drawing context.
 * @param {number} x The left side of the rectangle.
 * @param {number} y The top of the rectangle.
 * @param {number} width The width of the rectangle.
 * @param {number} height The height of the rectangle.
 */
NinePatch.prototype.draw = function(context, x, y, width, height) {
	x = x|0;
	y = y|0;
	width = width |0;
	height = height |0;
	var cx, cy, w, h;

	for (cy = y + this.h1; cy < y + height - this.h3; cy += this.h2) {
		for (cx = x + this.w1; cx < x + width - this.w3; cx += this.w2) {
			w = Math.min(this.w2, x + width - this.w3 - cx);
			h = Math.min(this.h2, y + height - this.h3 - cy);
			context.drawImage(this.img, this.w1, this.h1, w, h, cx, cy, w, h);
		}
	}
	for (cy = y + this.h1; cy < y + height - this.h3; cy += this.h2) {
		h = Math.min(this.h2, y + height - this.h3 - cy);
		if (this.w1 > 0) {
			context.drawImage(this.img, 0,                 this.h1, this.w1, h, x,                   cy, this.w1, h);
		}
		if (this.w3 > 0) {
			context.drawImage(this.img, this.w1 + this.w2, this.h1, this.w3, h, x + width - this.w3, cy, this.w3, h);
		}
	}
	for (cx = x + this.w1; cx < x + width - this.w3; cx += this.w2) {
		w = Math.min(this.w2, x + width - this.w3 - cx);
		if (this.h1 > 0) {
			context.drawImage(this.img, this.w1, 0,                 w, this.h1, cx, y,                    w, this.h1);
		}
		if (this.h3 > 0) {
			context.drawImage(this.img, this.w1, this.w1 + this.w2, w, this.h3, cx, y + height - this.h3, w, this.h3);
		}
	}
	if (this.w1 > 0 && this.h1 > 0) {
		context.drawImage(this.img, 0, 0, this.w1, this.h1, x, y, this.w1, this.h1);
	}
	if (this.w3 > 0 && this.h1 > 0) {
		context.drawImage(this.img, this.w1 + this.w2, 0, this.w3, this.h1, x + width - this.w3, y, this.w3, this.h1);
	}
	if (this.w1 > 0 && this.h3 > 0) {
		context.drawImage(this.img, 0, this.h1 + this.h2, this.w1, this.h3, x, y + height - this.h3, this.w1, this.h3);
	}
	if (this.w3 > 0 && this.h3 > 0) {
		context.drawImage(this.img, this.w1 + this.w2, this.h1 + this.h2, this.w3, this.h3, x + width - this.w3, y + height - this.h3, this.w3, this.h3);
	}
};

module.exports = NinePatch;

},{"./buffer":13}],35:[function(require,module,exports){
"use strict";

var platform = require("./platform");

/**
 * Open a url in a new window.
 * @alias Splat.openUrl
 * @param {string} url The url to open in a new window.
 */
module.exports = function(url) {
	window.open(url);
};

if (platform.isEjecta()) {
	module.exports = function(url) {
		window.ejecta.openURL(url);
	};
}

},{"./platform":37}],36:[function(require,module,exports){
"use strict";

function Particles(max, setupParticle, drawParticle) {
	this.particles = [];
	this.setupParticle = setupParticle;
	this.drawParticle = drawParticle;
	for (var i = 0; i < max; i++) {
		var particle = {
			x: 0,
			y: 0,
			vx: 0,
			vy: 0,
			enabled: false,
			age: 0
		};
		this.setupParticle(particle);
		this.particles.push(particle);
	}
	this.gravity = 0.1;
	this.maxAge = 1000;
}
Particles.prototype.move = function(elapsedMillis) {
	for (var i = 0; i < this.particles.length; i++) {
		var particle = this.particles[i];
		if (!particle.enabled) {
			continue;
		}
		particle.age += elapsedMillis;
		if (particle.age > this.maxAge) {
			particle.enabled = false;
			continue;
		}
		particle.x += particle.vx * elapsedMillis;
		particle.y += particle.vy * elapsedMillis;
		particle.vy += this.gravity;
	}
};
Particles.prototype.draw = function(context) {
	for (var i = 0; i < this.particles.length; i++) {
		var particle = this.particles[i];
		if (!particle.enabled) {
			continue;
		}
		this.drawParticle(context, particle);
	}
};
Particles.prototype.add = function(quantity, x, y, velocity, config) {
	var self = this;
	function setupParticle(particle) {
		particle.enabled = true;
		particle.age = 0;
		particle.x = x;
		particle.y = y;
		particle.vx = (Math.random() - 0.5) * velocity;
		particle.vy = (Math.random() - 0.5) * velocity;
		self.setupParticle(particle, config);
	}

	var particle;
	for (var i = 0; i < this.particles.length; i++) {
		particle = this.particles[i];
		if (particle.enabled) {
			continue;
		}
		if (quantity < 1) {
			return;
		}
		quantity--;
		setupParticle(particle);
	}

	// sort oldest first
	this.particles.sort(function(a, b) {
		return b.age - a.age;
	});

	for (i = 0; i < quantity; i++) {
		particle = this.particles[i];
		setupParticle(particle);
	}
};
Particles.prototype.reset = function() {
	for (var i = 0; i < this.particles.length; i++) {
		this.particles[i].enabled = false;
	}
};

module.exports = Particles;

},{}],37:[function(require,module,exports){
"use strict";

module.exports = {
	isChromeApp: function() {
		return window.chrome && window.chrome.app && window.chrome.app.runtime;
	},
	isEjecta: function() {
		return window.ejecta;
	}
};

},{}],38:[function(require,module,exports){
"use strict";
/**
 * @namespace Splat.saveData
 */

var platform = require("./platform");

function cookieGet(name) {
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	if (parts.length === 2) {
		return parts.pop().split(";").shift();
	} else {
		throw "cookie " + name + " was not found";
	}
}

function cookieSet(name, value) {
	var expire = new Date();
	expire.setTime(expire.getTime() + 1000 * 60 * 60 * 24 * 365);
	var cookie = name + "=" + value + "; expires=" + expire.toUTCString() + ";";
	document.cookie = cookie;
}

function getMultiple(getSingleFunc, keys, callback) {
	if (typeof keys === "string") {
		keys = [keys];
	}

	try
	{
		var data = keys.map(function(key) {
			return [key, getSingleFunc(key)];
		}).reduce(function(accum, pair) {
			accum[pair[0]] = pair[1];
			return accum;
		}, {});

		callback(undefined, data);
	}
	catch (e) {
		callback(e);
	}
}

function setMultiple(setSingleFunc, data, callback) {
	try {
		for (var key in data) {
			if (data.hasOwnProperty(key)) {
				setSingleFunc(key, data[key]);
			}
		}
		callback();
	}
	catch (e) {
		callback(e);
	}
}

var cookieSaveData = {
	"get": getMultiple.bind(undefined, cookieGet),
	"set": setMultiple.bind(undefined, cookieSet)
};

function localStorageGet(name) {
	return window.localStorage.getItem(name);
}

function localStorageSet(name, value) {
	window.localStorage.setItem(name, value.toString());
}

var localStorageSaveData = {
	"get": getMultiple.bind(undefined, localStorageGet),
	"set": setMultiple.bind(undefined, localStorageSet)
};

/**
 * A function that is called when save data has finished being retrieved.
 * @callback saveDataGetFinished
 * @param {error} err If defined, err is the error that occurred when retrieving the data.
 * @param {object} data The key-value pairs of data that were previously saved.
 */
/**
 * Retrieve data previously stored with {@link Splat.saveData.set}.
 * @alias Splat.saveData.get
 * @param {string | Array} keys A single key or array of key names of data items to retrieve.
 * @param {saveDataGetFinished} callback A callback that is called with the data when it has been retrieved.
 */
function chromeStorageGet(keys, callback) {
	window.chrome.storage.sync.get(keys, function(data) {
		if (window.chrome.runtime.lastError) {
			callback(window.chrome.runtime.lastError);
		} else {
			callback(undefined, data);
		}
	});
}

/**
 * A function that is called when save data has finished being stored.
 * @callback saveDataSetFinished
 * @param {error} err If defined, err is the error that occurred when saving the data.
 */
/**
 * Store data for later.
 * @alias Splat.saveData.set
 * @param {object} data An object containing key-value pairs of data to save.
 * @param {saveDataSetFinished} callback A callback that is called when the data has finished saving.
 */
function chromeStorageSet(data, callback) {
	window.chrome.storage.sync.set(data, function() {
		callback(window.chrome.runtime.lastError);
	});
}

var chromeStorageSaveData = {
	"get": chromeStorageGet,
	"set": chromeStorageSet,
};

if (platform.isChromeApp()) {
	module.exports = chromeStorageSaveData;
} else if (window.localStorage) {
	module.exports = localStorageSaveData;
} else {
	module.exports = cookieSaveData;
}

},{"./platform":37}],39:[function(require,module,exports){
"use strict";

var ECS = require("entity-component-system");
var EntityPool = require("./entity-pool");
var gameLoop = require("./game-loop");

function Scene() {
	this.simulation = new ECS();
	this.renderer = new ECS();
	this.entities = new EntityPool();
	this.simulationStepTime = 5;
}
Scene.prototype.start = function(context) {
	if (this._stop) {
		return;
	}
	this._stop = gameLoop(this.entities, this.simulation, this.simulationStepTime, this.renderer, context);
};
Scene.prototype.stop = function() {
	if (this._stop) {
		this._stop();
		delete this._stop;
	}
};

module.exports = Scene;

},{"./entity-pool":25,"./game-loop":26,"entity-component-system":54}],40:[function(require,module,exports){
"use strict";

window.AudioContext = window.AudioContext || window.webkitAudioContext;

/**
 * Loads sound files and lets you know when they're all available. An instance of SoundLoader is available as {@link Splat.Game#sounds}.
 * This implementation uses the Web Audio API, and if that is not available it automatically falls back to the HTML5 &lt;audio&gt; tag.
 * @constructor
 */
function SoundLoader(onLoad) {
	/**
	 * The key-value object that stores named sounds.
	 * @member {object}
	 * @private
	 */
	this.sounds = {};
	/**
	 * The total number of sounds to be loaded.
	 * @member {number}
	 * @private
	 */
	this.totalSounds = 0;
	/**
	 * The number of sounds that have loaded completely.
	 * @member {number}
	 * @private
	 */
	this.loadedSounds = 0;
	/**
	 * A flag signifying if sounds have been muted through {@link SoundLoader#mute}.
	 * @member {boolean}
	 * @private
	 */
	this.muted = false;
	/**
	 * A key-value object that stores named looping sounds.
	 * @member {object}
	 * @private
	 */
	this.looping = {};

	/**
	 * The Web Audio API AudioContext
	 * @member {external:AudioContext}
	 * @private
	 */
	this.context = new window.AudioContext();

	this.gainNode = this.context.createGain();
	this.gainNode.connect(this.context.destination);
	this.volume = this.gainNode.gain.value;
	this.onLoad = onLoad;
}
/**
 * Load an audio file.
 * @param {string} name The name you want to use when you {@link SoundLoader#play} the sound.
 * @param {string} path The path of the sound file.
 */
SoundLoader.prototype.load = function(name, path) {
	var self = this;

	if (this.totalSounds === 0) {
		// safari on iOS mutes sounds until they're played in response to user input
		// play a dummy sound on first touch
		var firstTouchHandler = function() {
			window.removeEventListener("click", firstTouchHandler);
			window.removeEventListener("keydown", firstTouchHandler);
			window.removeEventListener("touchstart", firstTouchHandler);

			var source = self.context.createOscillator();
			source.connect(self.gainNode);
			source.start(0);
			source.stop(0);

			if (self.firstPlay) {
				self.play(self.firstPlay, self.firstPlayLoop);
			} else {
				self.firstPlay = "workaround";
			}
		};
		window.addEventListener("click", firstTouchHandler);
		window.addEventListener("keydown", firstTouchHandler);
		window.addEventListener("touchstart", firstTouchHandler);
	}

	this.totalSounds++;

	var request = new XMLHttpRequest();
	request.open("GET", path, true);
	request.responseType = "arraybuffer";
	request.addEventListener("readystatechange", function() {
		if (request.readyState !== 4) {
			return;
		}
		if (request.status !== 200 && request.status !== 0) {
			console.error("Error loading sound " + path);
			return;
		}
		self.context.decodeAudioData(request.response, function(buffer) {
			self.sounds[name] = buffer;
			self.loadedSounds++;
			if (self.allLoaded() && self.onLoad) {
				self.onLoad();
			}
		}, function(err) {
			console.error("Error decoding audio data for " + path + ": " + err);
		});
	});
	request.addEventListener("error", function() {
		console.error("Error loading sound " + path);
	});
	try {
		request.send();
	} catch (e) {
		console.error("Error loading sound", path, e);
	}
};
SoundLoader.prototype.loadFromManifest = function(manifest) {
	var keys = Object.keys(manifest);
	var self = this;
	keys.forEach(function(key) {
		self.load(key, manifest[key]);
	});
};
/**
 * Test if all sounds have loaded.
 * @returns {boolean}
 */
SoundLoader.prototype.allLoaded = function() {
	return this.totalSounds === this.loadedSounds;
};
/**
 * Play a sound.
 * @param {string} name The name given to the sound during {@link SoundLoader#load}
 * @param {boolean} [loop=false] A flag denoting whether the sound should be looped. To stop a looped sound use {@link SoundLoader#stop}.
 */
SoundLoader.prototype.play = function(name, loop) {
	if (loop && this.looping[name]) {
		return;
	}
	if (!this.firstPlay) {
		// let the iOS user input workaround handle it
		this.firstPlay = name;
		this.firstPlayLoop = loop;
		return;
	}
	var snd = this.sounds[name];
	if (snd === undefined) {
		console.error("Unknown sound: " + name);
	}
	var source = this.context.createBufferSource();
	source.buffer = snd;
	source.connect(this.gainNode);
	if (loop) {
		source.loop = true;
		this.looping[name] = source;
	}
	source.start(0);
};
/**
 * Stop playing a sound. This currently only stops playing a sound that was looped earlier, and doesn't stop a sound mid-play. Patches welcome.
 * @param {string} name The name given to the sound during {@link SoundLoader#load}
 */
SoundLoader.prototype.stop = function(name) {
	if (!this.looping[name]) {
		return;
	}
	this.looping[name].stop();
	delete this.looping[name];
};
/**
 * Silence all sounds. Sounds keep playing, but at zero volume. Call {@link SoundLoader#unmute} to restore the previous volume level.
 */
SoundLoader.prototype.mute = function() {
	this.gainNode.gain.value = 0;
	this.muted = true;
};
/**
 * Restore volume to whatever value it was before {@link SoundLoader#mute} was called.
 */
SoundLoader.prototype.unmute = function() {
	this.gainNode.gain.value = this.volume;
	this.muted = false;
};
/**
 * Set the volume of all sounds.
 * @param {number} gain The desired volume level. A number between 0.0 and 1.0, with 0.0 being silent, and 1.0 being maximum volume.
 */
SoundLoader.prototype.setVolume = function(gain) {
	this.volume = gain;
	this.gainNode.gain  = gain;
	this.muted = false;
};
/**
 * Test if the volume is currently muted.
 * @return {boolean} True if the volume is currently muted.
 */
SoundLoader.prototype.isMuted = function() {
	return this.muted;
};

function AudioTagSoundLoader(onLoad) {
	this.sounds = {};
	this.totalSounds = 0;
	this.loadedSounds = 0;
	this.muted = false;
	this.looping = {};
	this.volume = new Audio().volume;
	this.onLoad = onLoad;
}
AudioTagSoundLoader.prototype.load = function(name, path) {
	this.totalSounds++;

	var audio = new Audio();
	var self = this;
	audio.addEventListener("error", function() {
		console.error("Error loading sound " + path);
	});
	audio.addEventListener("canplaythrough", function() {
		self.sounds[name] = audio;
		self.loadedSounds++;
		if (self.allLoaded() && self.onLoad) {
			self.onLoad();
		}
	});
	audio.volume = this.volume;
	audio.src = path;
	audio.load();
};
AudioTagSoundLoader.prototype.loadFromManifest = function(manifest) {
	var keys = Object.keys(manifest);
	var self = this;
	keys.forEach(function(key) {
		self.load(key, manifest[key]);
	});
};
AudioTagSoundLoader.prototype.allLoaded = function() {
	return this.totalSounds === this.loadedSounds;
};
AudioTagSoundLoader.prototype.play = function(name, loop) {
	if (loop && this.looping[name]) {
		return;
	}
	var snd = this.sounds[name];
	if (snd === undefined) {
		console.error("Unknown sound: " + name);
	}
	if (loop) {
		snd.loop = true;
		this.looping[name] = snd;
	}
	snd.play();
};
AudioTagSoundLoader.prototype.stop = function(name) {
	var snd = this.looping[name];
	if (!snd) {
		return;
	}
	snd.loop = false;
	snd.pause();
	snd.currentTime = 0;
	delete this.looping[name];
};
function setAudioTagVolume(sounds, gain) {
	for (var name in sounds) {
		if (sounds.hasOwnProperty(name)) {
			sounds[name].volume = gain;
		}
	}
}
AudioTagSoundLoader.prototype.mute = function() {
	setAudioTagVolume(this.sounds, 0);
	this.muted = true;
};
AudioTagSoundLoader.prototype.unmute = function() {
	setAudioTagVolume(this.sounds, this.volume);
	this.muted = false;
};
AudioTagSoundLoader.prototype.setVolume = function(gain) {
	this.volume = gain;
	setAudioTagVolume(this.sounds, gain);
	this.muted = false;
};
AudioTagSoundLoader.prototype.isMuted = function() {
	return this.muted;
};


function FakeSoundLoader(onLoad) {
	this.onLoad = onLoad;
}
FakeSoundLoader.prototype.load = function() {
	if (this.onLoad) {
		this.onLoad();
	}
};
FakeSoundLoader.prototype.loadFromManifest = function() {};
FakeSoundLoader.prototype.allLoaded = function() { return true; };
FakeSoundLoader.prototype.play = function() {};
FakeSoundLoader.prototype.stop = function() {};
FakeSoundLoader.prototype.mute = function() {};
FakeSoundLoader.prototype.unmute = function() {};
FakeSoundLoader.prototype.setVolume = function() {};
FakeSoundLoader.prototype.isMuted = function() {
	return true;
};

if (window.AudioContext) {
	module.exports = SoundLoader;
} else if (window.Audio) {
	module.exports = AudioTagSoundLoader;
} else {
	console.log("This browser doesn't support the Web Audio API or the HTML5 audio tag.");
	module.exports = FakeSoundLoader;
}

},{}],41:[function(require,module,exports){
"use strict";

function setOwnPropertiesDeep(src, dest) {
	var props = Object.keys(src);
	for (var i = 0; i < props.length; i++) {
		var prop = props[i];
		var val = src[prop];
		if (typeof val === "object") {
			if (!dest[prop]) {
				dest[prop] = {};
			}
			setOwnPropertiesDeep(val, dest[prop]);
		} else {
			dest[prop] = val;
		}
	}
}

module.exports = function advanceAnimations(ecs, data) {
	ecs.addEach(function(entity, elapsed) {
		var animation = data.animations[entity.animation.name];
		if (animation === undefined) {
			return;
		}

		entity.animation.time += elapsed * entity.animation.speed;
		while (entity.animation.time > animation[entity.animation.frame].time) {
			entity.animation.time -= animation[entity.animation.frame].time;
			entity.animation.frame++;
			if (entity.animation.frame >= animation.length) {
				if (entity.animation.loop) {
					entity.animation.frame = 0;
				} else {
					entity.animation.frame--;
				}
			}
		}
		setOwnPropertiesDeep(animation[entity.animation.frame].properties, entity);
	}, ["animation"]);
};

},{}],42:[function(require,module,exports){
module.exports=require("./lib/advance-timers")
},{"./lib/advance-timers":"./lib/advance-timers"}],43:[function(require,module,exports){
"use strict";

module.exports = function(ecs) {
	ecs.addEach(function(entity, elapsed) { // jshint ignore:line
		entity.velocity.x *= entity.friction.x;
		entity.velocity.y *= entity.friction.y;
	}, ["velocity", "friction"]);
};

},{}],44:[function(require,module,exports){
"use strict";

module.exports = function(ecs) {
	ecs.addEach(function(entity, elapsed) { // jshint ignore:line
		if (entity.movement2d.up && entity.velocity.y > entity.movement2d.upMax) {
			entity.velocity.y += entity.movement2d.upAccel;
		}
		if (entity.movement2d.down && entity.velocity.y < entity.movement2d.downMax) {
			entity.velocity.y += entity.movement2d.downAccel;
		}
		if (entity.movement2d.left && entity.velocity.x > entity.movement2d.leftMax) {
			entity.velocity.x += entity.movement2d.leftAccel;
		}
		if (entity.movement2d.right && entity.velocity.x < entity.movement2d.rightMax) {
			entity.velocity.x += entity.movement2d.rightAccel;
		}
	}, ["velocity", "movement2d"]);
};

},{}],45:[function(require,module,exports){
"use strict";

module.exports = function(ecs) {
	ecs.addEach(function(entity, elapsed) {
		entity.position.x += entity.velocity.x * elapsed;
		entity.position.y += entity.velocity.y * elapsed;
	}, ["position", "velocity"]);
};

},{}],46:[function(require,module,exports){
"use strict";

var spatialHash = {};

var gridSize = 100;

function toGrid(i) {
	return Math.floor(i / gridSize);
}
function keys(entity) {
	var x1 = toGrid(entity.position.x);
	var x2 = toGrid(entity.position.x + entity.size.width);

	var y1 = toGrid(entity.position.y);
	var y2 = toGrid(entity.position.y + entity.size.height);

	var k = [];
	for (var x = x1; x <= x2; x++) {
		for (var y = y1; y <= y2; y++) {
			k.push(x + "," + y);
		}
	}
	return k;
}

function add(entity, key) {
	if (!spatialHash[key]) {
		spatialHash[key] = [];
	}
	spatialHash[key].forEach(function(peer) {
		if (entity.collisions.indexOf(peer.id) !== -1) {
			return;
		}
		if (collides(entity, peer)) {
			entity.collisions.push(peer.id);
			peer.collisions.push(entity.id);
		}
	});
	spatialHash[key].push(entity);
}

function collides(a, b) {
	return a.position.x + a.size.width > b.position.x &&
		a.position.x < b.position.x + b.size.width &&
		a.position.y + a.size.height > b.position.y &&
		a.position.y < b.position.y + b.size.height;
}

module.exports = function(ecs) {
	ecs.add(function(entities, elapsed) { // jshint ignore:line
		spatialHash = {};
	});
	ecs.addEach(function(entity, elapsed) { // jshint ignore:line
		entity.collisions = []; // Maybe we should just empty the array instead of allocating a new one?
		keys(entity).forEach(add.bind(undefined, entity));
	}, ["position", "size", "collisions"]);
};

},{}],47:[function(require,module,exports){
"use strict";

module.exports = function(ecs, data) {
	ecs.add(function(entities, context) { // jshint ignore:line
		context.clearRect(0, 0, data.canvas.width, data.canvas.height);
	});
};

},{}],48:[function(require,module,exports){
"use strict";

module.exports = function(ecs) {
	ecs.addEach(function(entity, elapsed) { // jshint ignore:line
		if (entity.position.x < entity.playableArea.x) {
			entity.position.x = entity.playableArea.x;
		}
		if (entity.position.x + entity.size.width > entity.playableArea.x + entity.playableArea.width) {
			entity.position.x = entity.playableArea.x + entity.playableArea.width - entity.size.width;
		}
		if (entity.position.y < entity.playableArea.y) {
			entity.position.y = entity.playableArea.y;
		}
		if (entity.position.y + entity.size.height > entity.playableArea.y + entity.playableArea.height) {
			entity.position.y = entity.playableArea.y + entity.playableArea.height - entity.size.height;
		}
	}, ["position", "size", "playableArea"]);
};

},{}],49:[function(require,module,exports){
"use strict";

module.exports = function(ecs, data) {
	ecs.addEach(function(entity, elapsed) { // jshint ignore:line
		entity.movement2d.up = data.input.button(entity.playerController2d.up);
		entity.movement2d.down = data.input.button(entity.playerController2d.down);
		entity.movement2d.left = data.input.button(entity.playerController2d.left);
		entity.movement2d.right = data.input.button(entity.playerController2d.right);
	}, ["movement2d", "playerController2d"]);
};

},{}],50:[function(require,module,exports){
"use strict";

module.exports = function(ecs, data) {
	ecs.add(function(entities, context, elapsed) { // jshint ignore:line
		var fps = Math.floor(1000 / elapsed);

		context.font = "24px mono";
		if (fps < 30) {
			context.fillStyle = "red";
		} else if (fps < 50) {
			context.fillStyle = "yellow";
		} else {
			context.fillStyle = "green";
		}

		var msg = fps + " FPS";
		var w = context.measureText(msg).width;
		context.fillText(msg, data.canvas.width - w - 50, 50);
	});
};

},{}],51:[function(require,module,exports){
"use strict";

module.exports = function(ecs, data) {
	ecs.addEach(function(entity, context) {
		var image = data.images.get(entity.image.name);
		if (!image) {
			return;
		}
		context.drawImage(
			image,
			entity.image.sourceX,
			entity.image.sourceY,
			entity.image.sourceWidth,
			entity.image.sourceHeight,
			entity.image.destinationX + entity.position.x,
			entity.image.destinationY + entity.position.y,
			entity.image.destinationWidth,
			entity.image.destinationHeight
		);
	}, ["image", "position"]);
};

},{}],52:[function(require,module,exports){
"use strict";

module.exports = function(ecs) {
	ecs.addEach(function(entity, context) {
		if (entity.strokeStyle) {
			context.strokeStyle = entity.strokeStyle;
		}
		context.strokeRect(Math.floor(entity.position.x), Math.floor(entity.position.y), entity.size.width, entity.size.height);
	}, ["position", "size"]);
};

},{}],53:[function(require,module,exports){
"use strict";

var x = 0;
var y = 0;

module.exports = {
	moveToCamera: function(ecs) {
		ecs.add(function(entities, context) { // jshint ignore:line
			x = 0;
			y = 0;
		});
		ecs.addEach(function(entity, context) {
			var dx = Math.floor(entity.position.x + entity.camera.x) - x;
			var dy = Math.floor(entity.position.y + entity.camera.y) - y;
			x += dx;
			y += dy;
			context.translate(-dx, -dy);
		}, ["camera", "position"]);
	},
	reset: function(ecs) {
		ecs.addEach(function(entity, context) { // jshint ignore:line
			context.translate(x, y);
			x = 0;
			y = 0;
		}, ["camera", "position"]);
	}
};

},{}],54:[function(require,module,exports){
"use strict";

function EntityComponentSystem() {
	this.systems = [];
}
EntityComponentSystem.prototype.add = function(code) {
	this.systems.push(code);
};
EntityComponentSystem.prototype.addEach = function(code, requirements) {
	this.systems.push(function(entities) {
		var args = arguments;
		var keys = Object.keys(entities);
		for (var i = 0; i < keys.length; i++) {
			var entity = entities[keys[i]];
			if (requirements && !entityHasComponents(requirements, entity)) {
				continue;
			}
			args[0] = entity;
			code.apply(undefined, args);
		}
	});
};
EntityComponentSystem.prototype.run = function() {
	var args = arguments;
	for (var i = 0; i < this.systems.length; i++) {
		this.systems[i].apply(undefined, args);
	}
};

function entityHasComponents(components, entity) {
	for (var i = 0; i < components.length; i++) {
		if (!entity.hasOwnProperty(components[i])) {
			return false;
		}
	}
	return true;
}

module.exports = EntityComponentSystem;

},{}],55:[function(require,module,exports){
"use strict";

/**
 * Keyboard input handling.
 * @constructor
 * @param {module:KeyMap} keymap A map of keycodes to descriptive key names.
 */
function Keyboard(keyMap) {
	/**
	 * The current key states.
	 * @member {object}
	 * @private
	 */
	this.keys = {};

	var self = this;
	for (var kc in keyMap) {
		if (keyMap.hasOwnProperty(kc)) {
			this.keys[keyMap[kc]] = 0;
		}
	}
	window.addEventListener("keydown", function(event) {
		if (keyMap.hasOwnProperty(event.keyCode)) {
			if (self.keys[keyMap[event.keyCode]] === 0) {
				self.keys[keyMap[event.keyCode]] = 2;
			}
			return false;
		}
	});
	window.addEventListener("keyup", function(event) {
		if (keyMap.hasOwnProperty(event.keyCode)) {
			self.keys[keyMap[event.keyCode]] = 0;
			return false;
		}
	});
}
/**
 * Test if a key is currently pressed.
 * @param {string} name The name of the key to test
 * @returns {boolean}
 */
Keyboard.prototype.isPressed = function(name) {
	return this.keys[name] >= 1;
};
/**
 * Test if a key is currently pressed, also making it look like the key was unpressed.
 * This makes is so multiple successive calls will not return true unless the key was repressed.
 * @param {string} name The name of the key to test
 * @returns {boolean}
 */
Keyboard.prototype.consumePressed = function(name) {
	var p = this.keys[name] === 2;
	if (p) {
		this.keys[name] = 1;
	}
	return p;
};

module.exports = Keyboard;

},{}],56:[function(require,module,exports){
/**
 * Keyboard code mappings that map keycodes to key names. A specific named map should be given to {@link Keyboard}.
 * @module KeyMap
 */
module.exports = {
	"US": {
		8: "backspace",
		9: "tab",
		13: "enter",
		16: "shift",
		17: "ctrl",
		18: "alt",
		19: "pause/break",
		20: "capslock",
		27: "escape",
		32: "space",
		33: "pageup",
		34: "pagedown",
		35: "end",
		36: "home",
		37: "left",
		38: "up",
		39: "right",
		40: "down",
		45: "insert",
		46: "delete",
		48: "0",
		49: "1",
		50: "2",
		51: "3",
		52: "4",
		53: "5",
		54: "6",
		55: "7",
		56: "8",
		57: "9",
		65: "a",
		66: "b",
		67: "c",
		68: "d",
		69: "e",
		70: "f",
		71: "g",
		72: "h",
		73: "i",
		74: "j",
		75: "k",
		76: "l",
		77: "m",
		78: "n",
		79: "o",
		80: "p",
		81: "q",
		82: "r",
		83: "s",
		84: "t",
		85: "u",
		86: "v",
		87: "w",
		88: "x",
		89: "y",
		90: "z",
		91: "leftwindow",
		92: "rightwindow",
		93: "select",
		96: "numpad-0",
		97: "numpad-1",
		98: "numpad-2",
		99: "numpad-3",
		100: "numpad-4",
		101: "numpad-5",
		102: "numpad-6",
		103: "numpad-7",
		104: "numpad-8",
		105: "numpad-9",
		106: "multiply",
		107: "add",
		109: "subtract",
		110: "decimalpoint",
		111: "divide",
		112: "f1",
		113: "f2",
		114: "f3",
		115: "f4",
		116: "f5",
		117: "f6",
		118: "f7",
		119: "f8",
		120: "f9",
		121: "f10",
		122: "f11",
		123: "f12",
		144: "numlock",
		145: "scrolllock",
		186: "semicolon",
		187: "equals",
		188: "comma",
		189: "dash",
		190: "period",
		191: "forwardslash",
		192: "graveaccent",
		219: "openbracket",
		220: "backslash",
		221: "closebraket",
		222: "singlequote"
	}
};

},{}],57:[function(require,module,exports){
/*
  https://github.com/banksean wrapped Makoto Matsumoto and Takuji Nishimura's code in a namespace
  so it's better encapsulated. Now you can have multiple random number generators
  and they won't stomp all over eachother's state.
  
  If you want to use this as a substitute for Math.random(), use the random()
  method like so:
  
  var m = new MersenneTwister();
  var randomNumber = m.random();
  
  You can also call the other genrand_{foo}() methods on the instance.
 
  If you want to use a specific seed in order to get a repeatable random
  sequence, pass an integer into the constructor:
 
  var m = new MersenneTwister(123);
 
  and that will always produce the same random sequence.
 
  Sean McCullough (banksean@gmail.com)
*/
 
/* 
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.
 
   Before using, initialize the state by using init_seed(seed)  
   or init_by_array(init_key, key_length).
 
   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.                          
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:
 
     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
 
     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
 
     3. The names of its contributors may not be used to endorse or promote 
        products derived from this software without specific prior written 
        permission.
 
   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 
   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/
 
var MersenneTwister = function(seed) {
	if (seed == undefined) {
		seed = new Date().getTime();
	} 

	/* Period parameters */  
	this.N = 624;
	this.M = 397;
	this.MATRIX_A = 0x9908b0df;   /* constant vector a */
	this.UPPER_MASK = 0x80000000; /* most significant w-r bits */
	this.LOWER_MASK = 0x7fffffff; /* least significant r bits */

	this.mt = new Array(this.N); /* the array for the state vector */
	this.mti=this.N+1; /* mti==N+1 means mt[N] is not initialized */

	this.init_seed(seed);
}  

/* initializes mt[N] with a seed */
/* origin name init_genrand */
MersenneTwister.prototype.init_seed = function(s) {
	this.mt[0] = s >>> 0;
	for (this.mti=1; this.mti<this.N; this.mti++) {
		var s = this.mt[this.mti-1] ^ (this.mt[this.mti-1] >>> 30);
		this.mt[this.mti] = (((((s & 0xffff0000) >>> 16) * 1812433253) << 16) + (s & 0x0000ffff) * 1812433253)
		+ this.mti;
		/* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
		/* In the previous versions, MSBs of the seed affect   */
		/* only MSBs of the array mt[].                        */
		/* 2002/01/09 modified by Makoto Matsumoto             */
		this.mt[this.mti] >>>= 0;
		/* for >32 bit machines */
	}
}

/* initialize by an array with array-length */
/* init_key is the array for initializing keys */
/* key_length is its length */
/* slight change for C++, 2004/2/26 */
MersenneTwister.prototype.init_by_array = function(init_key, key_length) {
	var i, j, k;
	this.init_seed(19650218);
	i=1; j=0;
	k = (this.N>key_length ? this.N : key_length);
	for (; k; k--) {
		var s = this.mt[i-1] ^ (this.mt[i-1] >>> 30)
		this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1664525) << 16) + ((s & 0x0000ffff) * 1664525)))
		+ init_key[j] + j; /* non linear */
		this.mt[i] >>>= 0; /* for WORDSIZE > 32 machines */
		i++; j++;
		if (i>=this.N) { this.mt[0] = this.mt[this.N-1]; i=1; }
		if (j>=key_length) j=0;
	}
	for (k=this.N-1; k; k--) {
		var s = this.mt[i-1] ^ (this.mt[i-1] >>> 30);
		this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1566083941) << 16) + (s & 0x0000ffff) * 1566083941))
		- i; /* non linear */
		this.mt[i] >>>= 0; /* for WORDSIZE > 32 machines */
		i++;
		if (i>=this.N) { this.mt[0] = this.mt[this.N-1]; i=1; }
	}

	this.mt[0] = 0x80000000; /* MSB is 1; assuring non-zero initial array */ 
}

/* generates a random number on [0,0xffffffff]-interval */
/* origin name genrand_int32 */
MersenneTwister.prototype.random_int = function() {
	var y;
	var mag01 = new Array(0x0, this.MATRIX_A);
	/* mag01[x] = x * MATRIX_A  for x=0,1 */

	if (this.mti >= this.N) { /* generate N words at one time */
		var kk;

		if (this.mti == this.N+1)  /* if init_seed() has not been called, */
			this.init_seed(5489);  /* a default initial seed is used */

		for (kk=0;kk<this.N-this.M;kk++) {
			y = (this.mt[kk]&this.UPPER_MASK)|(this.mt[kk+1]&this.LOWER_MASK);
			this.mt[kk] = this.mt[kk+this.M] ^ (y >>> 1) ^ mag01[y & 0x1];
		}
		for (;kk<this.N-1;kk++) {
			y = (this.mt[kk]&this.UPPER_MASK)|(this.mt[kk+1]&this.LOWER_MASK);
			this.mt[kk] = this.mt[kk+(this.M-this.N)] ^ (y >>> 1) ^ mag01[y & 0x1];
		}
		y = (this.mt[this.N-1]&this.UPPER_MASK)|(this.mt[0]&this.LOWER_MASK);
		this.mt[this.N-1] = this.mt[this.M-1] ^ (y >>> 1) ^ mag01[y & 0x1];

		this.mti = 0;
	}

	y = this.mt[this.mti++];

	/* Tempering */
	y ^= (y >>> 11);
	y ^= (y << 7) & 0x9d2c5680;
	y ^= (y << 15) & 0xefc60000;
	y ^= (y >>> 18);

	return y >>> 0;
}

/* generates a random number on [0,0x7fffffff]-interval */
/* origin name genrand_int31 */
MersenneTwister.prototype.random_int31 = function() {
	return (this.random_int()>>>1);
}

/* generates a random number on [0,1]-real-interval */
/* origin name genrand_real1 */
MersenneTwister.prototype.random_incl = function() {
	return this.random_int()*(1.0/4294967295.0); 
	/* divided by 2^32-1 */ 
}

/* generates a random number on [0,1)-real-interval */
MersenneTwister.prototype.random = function() {
	return this.random_int()*(1.0/4294967296.0); 
	/* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
/* origin name genrand_real3 */
MersenneTwister.prototype.random_excl = function() {
	return (this.random_int() + 0.5)*(1.0/4294967296.0); 
	/* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
/* origin name genrand_res53 */
MersenneTwister.prototype.random_long = function() { 
	var a=this.random_int()>>>5, b=this.random_int()>>>6; 
	return(a*67108864.0+b)*(1.0/9007199254740992.0); 
} 

/* These real versions are due to Isaku Wada, 2002/01/09 added */

module.exports = MersenneTwister;

},{}],58:[function(require,module,exports){
module.exports = function(rate) {
	var accum = 0;
	return function(time, callback) {
		accum += time;
		while (accum >= rate) {
			accum -= rate;
			callback(rate);
		}
	};
};

},{}],59:[function(require,module,exports){
module.exports={
 "person-a": {
  "regib": true,
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 75,
   "height": 81
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "person-a",
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -21,
   "destinationY": -117,
   "destinationWidth": 115,
   "destinationHeight": 200
  },
  "collisions": [],
  "sounds": [
   "male-scream1", "male-scream2"
  ],
  "gibs": [
   "person-a-gib1", "person-a-gib2", "person-a-gib3", "person-a-gib4", "person-a-gib5"
  ]
 },
 "person-b": {
  "regib": true,
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 75,
   "height": 81
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "person-b",
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -21,
   "destinationY": -117,
   "destinationWidth": 115,
   "destinationHeight": 200
  },
  "collisions": [],
  "sounds": [
   "male-scream1", "male-scream2"
  ],
  "gibs": [
   "person-b-gib1", "person-b-gib2", "person-b-gib3", "person-b-gib4", "person-b-gib5"
  ]
 },
 "person-c": {
  "regib": true,
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 75,
   "height": 81
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "person-c",
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -21,
   "destinationY": -117,
   "destinationWidth": 115,
   "destinationHeight": 200
  },
  "collisions": [],
  "sounds": [
   "woman-scream1", "woman-scream2"
  ],
  "gibs": [
   "person-c-gib1", "person-c-gib2", "person-c-gib3", "person-c-gib4", "person-c-gib5"
  ]
 },
 "person-d": {
  "regib": true,
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 75,
   "height": 81
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "person-d",
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -21,
   "destinationY": -117,
   "destinationWidth": 115,
   "destinationHeight": 200
  },
  "collisions": [],
  "sounds": [
   "woman-scream1", "woman-scream2"
  ],
  "gibs": [
   "person-d-gib1", "person-d-gib2", "person-d-gib3", "person-d-gib4", "person-d-gib5"
  ]
 },
 "ekg": {
  "name": "ekg",
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 59,
   "height": 50
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "ekg"
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -20,
   "destinationY": -108,
   "destinationWidth": 100,
   "destinationHeight": 159
  },
  "collisions": [],
  "gibs": [ "wall-gib1",  "wall-gib2",  "wall-gib3", "wall-gib4" ],
  "sounds": [ "ekg-chomp" ]
 },
 "iv": {
  "name": "iv",
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 68,
   "height": 27
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "iv"
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -30,
   "destinationY": -139,
   "destinationWidth": 129,
   "destinationHeight": 167
  },
  "collisions": [],
  "gibs": [ "wall-gib1",  "wall-gib2",  "wall-gib3", "wall-gib4" ],
  "sounds": [ "iv-chomp" ]
 },
 "potted plant": {
  "name": "potted plant",
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 77,
   "height": 40
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "potted-plant"
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": -67,
   "destinationY": -149,
   "destinationWidth": 200,
   "destinationHeight": 189
  },
  "collisions": [],
  "gibs": [ "wall-gib1",  "wall-gib2",  "wall-gib3", "wall-gib4" ],
  "sounds": [ "wall-eat1", "wall-eat2", "wall-eat3", "wall-eat4" ]
 },
 "breathing machine": {
  "name": "breathing machine",
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 64,
   "height": 50
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "breathing-machine"
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": 0,
   "destinationY": -137,
   "destinationWidth": 65,
   "destinationHeight": 188
  },
  "collisions": [],
  "gibs": [ "wall-gib1",  "wall-gib2",  "wall-gib3", "wall-gib4" ],
  "sounds": [ "wall-eat1", "wall-eat2", "wall-eat3", "wall-eat4" ]
 },
 "bed": {
  "name": "bed",
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 174,
   "height": 270
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "hospital-bed"
  },
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": 0,
   "destinationY": -29,
   "destinationWidth": 173,
   "destinationHeight": 300
  },
  "collisions": [],
  "gibs": [ "wall-gib1",  "wall-gib2",  "wall-gib3", "wall-gib4" ],
  "sounds": [ "wall-eat1", "wall-eat2", "wall-eat3", "wall-eat4" ]
 },
 "wall": {
  "position": {
   "x": 0,
   "y": 0
  },
  "size": {
   "width": 74,
   "height": 71
  },
  "collisions": [],
  "image": {
   "sourceX": 0,
   "sourceY": 0,
   "sourceWidth": 0,
   "sourceHeight": 0,
   "destinationX": 0,
   "destinationY": -129,
   "destinationWidth": 74,
   "destinationHeight": 200
  },
  "animation": {
   "time": 0,
   "frame": 0,
   "loop": true,
   "speed": 1,
   "name": "wall"
  },
  "gibs": [ "wall-gib1",  "wall-gib2",  "wall-gib3", "wall-gib4" ]
 }
}

},{}],60:[function(require,module,exports){
module.exports={
 "Wake_-_67_-_Duckbag": "sounds/Wake_-_67_-_Duckbag.mp3",
 "chomp1": "sounds/chomp1.mp3",
 "chomp10": "sounds/chomp10.mp3",
 "chomp2": "sounds/chomp2.mp3",
 "chomp3": "sounds/chomp3.mp3",
 "chomp4": "sounds/chomp4.mp3",
 "chomp5": "sounds/chomp5.mp3",
 "chomp6": "sounds/chomp6.mp3",
 "chomp7": "sounds/chomp7.mp3",
 "chomp8": "sounds/chomp8.mp3",
 "chomp9": "sounds/chomp9.mp3",
 "crickets": "sounds/crickets.mp3",
 "ekg-chomp": "sounds/ekg-chomp.mp3",
 "go": "sounds/go.mp3",
 "hospital-sounds": "sounds/hospital-sounds.mp3",
 "iv-chomp": "sounds/iv-chomp.mp3",
 "male-scream1": "sounds/male-scream1.mp3",
 "male-scream2": "sounds/male-scream2.mp3",
 "shark-birth": "sounds/shark-birth.mp3",
 "shark-hurt": "sounds/shark-hurt.mp3",
 "wall-eat1": "sounds/wall-eat1.mp3",
 "wall-eat2": "sounds/wall-eat2.mp3",
 "wall-eat3": "sounds/wall-eat3.mp3",
 "wall-eat4": "sounds/wall-eat4.mp3",
 "woman-scream1": "sounds/woman-scream1.mp3",
 "woman-scream2": "sounds/woman-scream2.mp3"
}
},{}],61:[function(require,module,exports){
module.exports={
	"simulation": [
		"./lib/advance-timers",
		"./lib/fade-in-text",
		"splatjs:advanceAnimations",
		"./lib/control-player",
		"splatjs:applyMovement2d",
		"./lib/apply-angular-velocity",
		"splatjs:applyVelocity",
		"splatjs:applyFriction",
		"./lib/follow-parent",
		"./lib/shake-screen",
		"splatjs:constrainToPlayableArea",
		"splatjs:boxCollider",
		"./lib/eat-food",
		"./lib/delete-entities"
	],
	"renderer": [
		"splatjs:clearScreen",
		"splatjs:viewport.moveToCamera",
		"./lib/tile-background",
		"./lib/draw-rotated-image",
		"./lib/draw-text",
		"splatjs:viewport.reset"
	]
}

},{}]},{},[1]);
